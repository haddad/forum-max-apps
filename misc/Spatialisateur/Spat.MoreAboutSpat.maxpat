{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 5,
			"architecture" : "x86"
		}
,
		"rect" : [ 25.0, 69.0, 546.0, 217.0 ],
		"bglocked" : 1,
		"openinpresentation" : 0,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 8.0, 8.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 264.0, 123.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 72.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 48.0, 174.0, 16.0 ],
									"text" : "loadunique \"Ircam Spat Overview\""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 16.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 192.0, 184.0, 64.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Launcher"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bgovercolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoveroncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"hint" : "",
					"id" : "obj-4",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 192.0, 152.0, 160.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 256.0, 146.0, 69.0, 17.0 ],
					"prototypename" : "M4L.toggle",
					"rounded" : 24.0,
					"text" : "Ircam Spat Overview",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"texton" : "Ircam Spat Overview",
					"textoncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 18.0,
					"frgb" : 0.0,
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 128.0, 40.0, 281.0, 27.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 39.0, 194.0, 635.0, 27.0 ],
					"text" : "___________________________",
					"textcolor" : [ 1.0, 0.603922, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 36.0,
					"frgb" : 0.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 32.0, 89.0, 47.0 ],
					"text" : "Spat",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"data" : [ 4601, "", "IBkSG0fBZn....PCIgDQRA...bE...vLHX....f3sRIH....DLmPIQEBHf.B7g.YHB..Q.aRDEDU3wY6asGdTUcs+2ZeNyqjIYxjWlGjRnXPdX.DQ+x0G.ErRjGQDgV4ZMeVexiJAQJkV5EKEZoJ3sHTkGhOPaQt.U9fauJzZUhUgAgz.WDnoDHfESHOLyj2yLmYuW2+HmS5PHff0hga822W9x4r2qydu1+l0dcV60de.5lirxJqUlXhIFHojRJvnG8nusur0mKEn+ksBbQ.uDQdXlQiM1niurUlKEzsmbsa29wCEJzekY1viGOM7ks9bo.5KaE3h.5.vF.T.vv7+eE9Wcn8ksB7YggNzgVfKWtlRhIl3Hl3Dm3oKszRqebiabCQJkOZhIl3HxO+7a8PG5PMsgMrgY5zoyo+HOxiT5d1ydBrnEsndVYkU9X1saeZd858txHiL9Z0Vas+kEtvEFL51uvBKbfMzPCEESLwL03hKtwkc1YmRM0TyGA.okLCZPC5631s6IlTRIkWgEVXkABD36Z2t84jPBIbKSdxS9XkVZo96ae66zc3vwr83wy3u5q9popppp+R2d2Bomd5ud3vguGlYzm9zmB74y2+ce6aemds0V6ywLCa1r8mXliSJkCF.X7ie78owFaT68du26MUJUuHh.yLHhfll19G23F2c9RuzKUI.Pt4l6jprxJWG.7vLC..hHXylscVUUUcODQA..RM0T+.oTdSLyPSS6vJkZ...l2eLcc8OITnPiHZ8Nqrx5ADWF4oOWPWWuEqqc3vgA.fc61CYQXFFF2pTJGrI4z3YNyYrs6cu6kyL2KhZ21whfkR4Pem24c9O..d1m8YSoxJq7Wg+Nwprj2vvXz8pW8pHq9UHDMYQ9Rob.LycXUqTpbBEJzHHhPz8WkUV4r51StWLvtc66H2by8NF4HG4vyHiLDgCGdXlje04lat42qd0q6AluHrwFa7d..VyZVyXXluJlYnqqWxMdi23vSO8zeLKhqs1Za7vzsoEwB.DarwttANvAdyNc5baQQlmJ2bycDYjQFOL.BC.nTpr61GJFvYO3..BF7u61TSS6PUUUUSnpppJD.vC7.Ov0BSRwkKW+wcsqcsS.fryN67BFL30BflAf9UcUWUEgBEZgRoTdC2vMroMu4MWF.d+jSN4YyL2KkR4B.B.HsrJYlijWd48badya9.2zMcSqsrxJ6NM6me+t10tJF.EmRJoLGoTdMDQ7UDjqkEhEb5zYGS0iO93eqZqs1PcRdF.fYtCWJm7jm7wiVj28ce22C.umYctxO+76elYlY3ssss0LyLXlOmP9HhjokVZgA.hDIRDqxUJkkBZClVt.WArHhKDLsnZoykqTJq5Net8X.f69tu6qymOe+zfACdKe3G9gwPDoTJkMgPbV+fF8LmVZoEA.fKWtDQ8hRKgIy+.yLthvmqokz4qtyYLHDe1Cqeyu42jbwEW7uMXvfiC.I..6LyNDBw4DdpEQqTpyo7tRurbibEA4F8ahAZ2m64iri9YtPXoKco2NybuLamJxLyLm7fG7fyiH5jck7Lyc4OZctehVuth0sv4i7hx+Wm8aFCyrMymsglZpo9XQDolZpK8PG5Paw75FjR4YQRVS+6L5pxi1Z9JBx87M35JDSLwnrbUHkxdaUbpol5tSJojxVHDMCfdPDo.53GIKlLNkRkQmayK19NZ8E3JDxsqhV.3bCQC.34e9m+iSN4j8qTpTCGN7HSO8zeSoTFqTJGjo6kSY1lsY0t0UWcyte8qen4la9las0VSoq5yymUZmQzx0s2mKybGAxGIRDq2DGsd24wPSd734YsFfgCG9NhDIxvr9gHyLy7o..xHiL9SVOfRoxo5pqdUs1ZqeGlYqPoh1vyRGzsb6DktzgNB.HDBcyxz61StDQMyL2.QTCRoz..PoTAAPCDQMPD0Zmelie7i+Td85cADQmQoTQXlMDBwIxHiLdjCbfCrA.fhKtXeIjPBKjHpIlYEQTcImbxyQWW2GybCBgvuU6IDhFAPCBg3SqolZh..31saCK8hHpYKYYl8aIa29D2..WLy1AfULsQP6gM4xrrP.HXW8fuwa7FIsoMsoqtgFZP9Vu0aUF.Zpyxr7ku7u1N1wN9Ztb45jacqa8z.vMyrloOYK4ik41WvkIQJAfNybrlkEF.sYJabcU3geE9J7U3Bhpqt5Y7TO0S8VO+y+7ajY9l9b1LDyrSqoqcGgdZok11UJUb.f0zN6U9IkRXUVzWGc8BgfIhBwLaiY1NyrjHRq28t2O0t28teyN2gkWd4EsfErfkCzdnLybly71YluIhn+xkhhyLe0+ve3ObaiZTi5M.vO9RZTeYB5IjPBuqggQL1rYqK23OCCC..XylMXXXXkwIEZ2xQXylMTWc0MCkRkoWudWjMa1ZKRjH58nG83S5p1aSaZS2EQT3UspUcskTRIS5EewW7mefCbfIAfEeIp6FABDnee7G+woeI9bW1fdiM13sZZ4dwrqpLQjRJkIQDoDBQ.oT5zlMak0m9zmG5Tm5TEzZqs1Ol4P0TSMeH.NXma.kRQtc6tRhniwLuoCbfCjyfG7fOB..y7s3ymuuYbwEWaCX.CX8DQUwLOgRJojbu9q+5O792+9G3PG5PKA.6XKaYKOA.vwO9wGJy7MCfH9746NxKu71wYNyYtwzRKshAP0kTRIOfgggi7xKueGQz99hi59rgdO6YOeN61sayJGnQChHNXvfRkRIb4xkvvvPbricrerTJ6aFYjwbb618wc3vgMlY5nG8nKToT8zqWu+bWtb0RJojxI6pNTHDV4Z0AQzwAvC7POzCAl4ac5Se56ToTtHhPpol5DYlG4pW8pu6CdvC9cV6ZWKDBAV25VWjUu5U+MJojRFAyLpu956O.x4fG7fo+Juxq7ju7K+xymHReJSYJO4t10ttiyblyjG.vq9pu5rYlG4kSBV+Dm3DK..tAN6TpIDBnTJ6RoLSMMs1DBQ0Lyj4Vm7sJu7xmRUUU0ioooYDIRDO.vqllVcABD3gCDHflKWtB.fxtXUjst0sNQkR4Z0qd0Et90u9g6ymuGD.CL4jS1O.vbm6bm0d1ydx88e+2+AAPOVxRVxDl1zlV4CYHCYiDQu592+9mOQDRN4jO7hW7hmK.Reiabi4MrgMr0L5QO5+77m+7Wy1291+V.3xG4lbxIuNgP3PHDR.XnqqqYXXnC.Tas0NEhHWYmc1ySJkwXXXnGIRD4QNxQdVoTlY5om97EBQKFFFLQjD.BcccAyLkc1Yen8t281kcZWslb+98607xJxM2bS2mOe..NBGNr..n28t2+5d26dOpO3C9fGD.1gYP6555F.PYsDzEu3E+yHh98Ly2NyLFzfFT4IlXhGA.nlZpI0u3ntOanWe80OEoTFmRobyLOXlY+1rY6i.ftc61qnO8oOKtrxJaVJkJaMMs.JkxAQTy1saee0TSM2ELW2cmxbula2tqB.+0tpS6pDtvLGgHBSaZS6cLuWAfHZZZVxGC.bXdMC.My0v2wZ8MqKfYSJ..V4JW4RP6jO6vgiv3xHz83wyuG.tpu95uChnR+5e8u9B762eeLLLbPDwG9vGdSLyoDWbw8y000az74XD0VZzYnoookSN4721yd1SWUMAz9xVYl62u3W7KV47l27V2V1xVZE.XUqZUOB.pDsSleT3vgK7rd3NY0aylsH..RozpBq3EI.fwN1wthwMtwsc.jL.NYgEdVM2+TgdKszxPTJU7tb45OmQFY71m3Dm36IDBmBgnZ.X2tc6EqooExvvHGqoneFfHhDG6XGy24oRYyM2blLyiXm6bm29oN0oFUUUU0ajRJo7olh7oG4HGYfaXCanvEu3EWnc61MtP4Ss5pqNCl4X26d2ajNUEg1G.gAPL+jexOY9268duuF.J4hXL7EBzsa2dUgCGtAkRoUVYksM.HhM1XeIyjJCXZkdwruTVPoTTZok14rwg..EVXguwhVzh9FSaZS6cA.b61cEomd5aN8zSO2W+0ecLsoMssSDAmNcdZ.7wMzPCVGZCQvfAcvLiFZngX73wSstb45zkWd4i4zm9zOra2tslxay7+GzkKWexV25Vm2V25VmmRoPN4jyO3yAG84F5.PoooEA.H93ieIZZZJoT5Fm8T9K7FV0Iz4UxEMxLyL+ULyMt90u9g40q2FKnfBVCQTs.3cXlm3q8Zu1Xb4xUvIMoIsJhn5Yl2rOe9NF.B3zoy8c+2+8uPOd7rOhnfLySvmOeEzidzi81idzC99u+6eg.3n..DQeBy7X2xV1xC0Vas479tu6aqDQ+gKQ94qvWg+EDc6SVNy7sFLXvAnTJNlXhoU.7GHhNy+j5qgKkxbzzzdUyDfaU9vLLL5qMa11LQj+KTaDM51ltNK7BuvK7vkVZo2m0pGiO93KmYdrDQcYLz+ifUrhU7XG9vG9tWyZVyVPTGKo0t109nkVZo+6qZUqxG.9+OjqlllToTXEqXEOX4kWdhqbkqboae6a+QAvS7EceMyYNykAfeK.Nq8kylMagrNloWJs2UL60ic61+CCX.CXY.HTEUTQuA.Xl6Gy73Yl6u48ZlK68FXlGBy72zLg5ChY9NXlcaJ2MvLOJl49yLOFl4qxrapC.mjHxvL2z2Fy7PiDIhdmVAZNl86PLKhXlGNy7MXV+PYlGc2dK2nfN.fPHjNb3nMl4BmwLlwpTJULBgnMl4oCfMLiYLisHkRa.vI.PJojxetlZpYHBg.8qe862xLeOKaYK6mVd4kmuUh8SM0T+Pl4a9ke4W9Gs28t2uKyr8hKt34swMtwEZZwFzzGbqLy4WTQEsgPgB4kYlO3AO3OXPCZPOy2+6+8ekjRJoJ.vHe5m9oWxINwI9Fc6sbiJUn8iY9dUJULd85stEtvENU.f7yO+mA.pErfELK.DmPHHGNbz7jm7jWhllVa0VasCYLiYLOSLwDymbzidzwCfXrR64XG6XWgWudKqlZp4FAPOb5zoAZOldW6ae66lYlwXG6XelDRHg+FZeVtiktzkNqPgBE+ccW20RhM1Xq9EewWbtLydEBgzJOGlmsgy6QrraCDBghHBScpS8+YpScp+ZGNbT429a+s+u762eexJqr1yDlvDlSVYkkO+98mM.hQJk5YkUV6+1tsa6G41s6+lGOdJeBSXByo+8u+6vZ+1TJkfHx3Nuy6b14latuq4TdMzNw1geUhHiBJnf4LvANv+HZelSLABDnGwGe7Uje94+it1q8Z2QnPgRD.1L27.UT5rpaO4JkRhYFCe3CeMyblybtKe4K+V.v9BFLnWa1rIA.z00kQhDwA.DlV5BlY6DQcjbdccc.yUZZkUNlYaRoTOpLs0AXqBOaY.QDGUB+klmuAlH5bNhoc6IWzNggoLko7yFv.FvRIhp.sePQh9TLpPWDydzo1LprlAfNNVpQHhTWnDCQDEAQ8YSwLKrNDe1saWY4J..PSSyxxUBbkA4ZAOQccXmNc5uwFaLEl4DZpolRwkKW9APjnOBomOPDwRoTiYNiVZokXMKNZtf.fPoTBl4zBDHPxlVxJcc8PACFzCybR0UWcVQYPJkh862eZLydat4lShYV0sOZgPgBY8wT2wfmHJ3xV1x7Ud4kO1YO6Yej1Zqszulq4Z1F.ZfY1YnPgbB.DIRjXHSyRy7SqA.RSSKhPHDEUTQGJTnPdP6V9M0Vas4Ds660voSmMJDBshJpnCFJTnTLallyM2b+f29se6u2i+3O9G0VaskVJojx9APiNc5rkZpolqqnhJpLS4izsmbG0nFUIM0TSICfFipX0S7DOwb9k+xeorpppZf4jSNaeVyZVyE.AyImbdyq+5u9C..00ccW2NsbGLrgMrRZt4l8h1+9g0YlC2yd1y+T0UWceKnfB1.QT0G4HG4+s95qeGlYb6+bAKXAYKDhv8rm87242u+z.PySZRSZI0VasIUQEU7ukd5o+1yctyc1DQsxL+ydxm7ImuCGN9zTSM0JaokVh+xNY8EMXyCjmItnxUxS+zO8a+nO5iFhYNFlYmWf11FNOtN6T+Fs7cfqj741kfHpsnt8hJuyQhDQia+jSpSD0kmPRy1979Ux2o9MZ46.WwStedfSmNazoSme5msj+ig+O54czgI.wpLE.....IUjSD4pPfIH" ],
					"embed" : 1,
					"id" : "obj-11",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 412.0, 31.0, 87.0, 51.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 202.0, 113.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 72.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 48.0, 142.0, 16.0 ],
									"text" : "loadunique ForumMaxApps"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 16.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 432.0, 184.0, 64.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Overview"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bgovercolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoveroncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hint" : "",
					"id" : "obj-169",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 432.0, 160.0, 69.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 385.0, 546.0, 69.0, 17.0 ],
					"prototypename" : "M4L.toggle",
					"rounded" : 16.0,
					"text" : "Overview",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"texton" : "Overview",
					"textoncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-5",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 88.0, 457.0, 47.0 ],
					"text" : "For more information on Spat in Max/MSP, and to discover its modular architecture, open the regular Spat launcher. You'll have direct access to the help files of all Spat external objects, and to additional tutorial patches:"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.301961, 0.337255, 0.403922, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.301961, 0.337255, 0.403922, 0.662745 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"id" : "obj-2",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 24.0, 24.0, 496.0, 168.0 ],
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
 ],
		"dependency_cache" : [  ]
	}

}
