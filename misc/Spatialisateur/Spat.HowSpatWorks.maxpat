{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 5,
			"architecture" : "x86"
		}
,
		"rect" : [ 0.0, 44.0, 1260.0, 702.0 ],
		"bglocked" : 1,
		"openinpresentation" : 0,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 8.0, 8.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 8,
					"numoutlets" : 0,
					"patching_rect" : [ 840.0, 528.0, 131.0, 18.0 ],
					"text" : "dac~ 1 2 3 4 5 6 7 8"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Output gain",
					"clip_size" : 1,
					"display_range" : [ -70.0, 30.0 ],
					"hint" : "",
					"id" : "obj-40",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 936.0, 416.0, 35.0, 95.0 ],
					"prototypename" : "M4L.live.gain~.V.extended",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "Gain7-8",
							"parameter_shortname" : "7-8",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 30.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "Gain7-8"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Output gain",
					"clip_size" : 1,
					"display_range" : [ -70.0, 30.0 ],
					"hint" : "",
					"id" : "obj-39",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 904.0, 416.0, 35.0, 95.0 ],
					"prototypename" : "M4L.live.gain~.V.extended",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "Gain5-6",
							"parameter_shortname" : "5-6",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 30.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "Gain5-6"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Output gain",
					"clip_size" : 1,
					"display_range" : [ -70.0, 30.0 ],
					"hint" : "",
					"id" : "obj-38",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 872.0, 416.0, 35.0, 95.0 ],
					"prototypename" : "M4L.live.gain~.V.extended",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "Gain3-4",
							"parameter_shortname" : "3-4",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 30.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "Gain3-4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-37",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 904.0, 254.0, 99.0, 18.0 ],
					"text" : "Azimuth of source 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-35",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 960.0, 272.0, 27.0, 18.0 ],
					"text" : "deg"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-34",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 920.0, 272.0, 40.0, 18.0 ],
					"prototypename" : "Live",
					"triscale" : 0.75
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 920.0, 296.0, 91.0, 16.0 ],
					"text" : "source 1 azim $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-95",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1002.0, 488.0, 39.0, 18.0 ],
					"text" : "Audio",
					"textcolor" : [ 0.101961, 0.121569, 0.172549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 12.0,
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 888.0, 224.0, 41.0, 18.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 12.0,
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 888.0, 328.0, 65.0, 20.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "spat.oper"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Output gain",
					"clip_size" : 1,
					"display_range" : [ -70.0, 30.0 ],
					"hint" : "",
					"id" : "obj-76",
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 840.0, 416.0, 35.0, 95.0 ],
					"prototypename" : "M4L.live.gain~.V.extended",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "Gain1-2",
							"parameter_shortname" : "1-2",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 30.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "Gain1-2"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Turn DSP on and off.",
					"bgcolor" : [ 0.415686, 0.454902, 0.52549, 1.0 ],
					"hint" : "",
					"id" : "obj-77",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offgradcolor1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"offgradcolor2" : [ 0.415686, 0.454902, 0.52549, 1.0 ],
					"ongradcolor1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"ongradcolor2" : [ 1.0, 0.74902, 0.231373, 1.0 ],
					"patching_rect" : [ 1000.0, 504.0, 42.0, 42.0 ],
					"prototypename" : "M4L.white"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@Folder", "./examples/sounds", "@File", "drumLoop.aif", "@Loop", 1 ],
					"id" : "obj-7",
					"maxclass" : "bpatcher",
					"name" : "FMA.SoundPlayer~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "" ],
					"patching_rect" : [ 840.0, 72.0, 272.0, 136.0 ],
					"varname" : "Fma.SoundPlayer~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 12.0,
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "signal", "signal", "signal", "signal", "signal", "signal", "signal", "signal", "" ],
					"patching_rect" : [ 840.0, 376.0, 146.5, 20.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "spat.spat~"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-30",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 824.0, 80.0, 19.0, 18.0 ],
					"presentation_rect" : [ 825.0, 80.0, 0.0, 0.0 ],
					"text" : ">"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-6",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 80.0, 72.0, 40.0 ],
					"presentation_rect" : [ 759.0, 74.0, 0.0, 0.0 ],
					"text" : "Click \"Folder\" to import your own sounds"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1008.0, 296.0, 73.0, 18.0 ],
					"text" : "<------- Syntax"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-96",
					"justification" : 2,
					"linecolor" : [ 1.0, 0.603922, 0.0, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 232.0, 36.0, 370.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 118.0, 518.0, 321.0, 16.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-50",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 696.0, 56.0, 32.0, 592.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 952.0, 328.0, 93.0, 18.0 ],
					"text" : "<-----------------------"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-45",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 992.0, 272.0, 53.0, 18.0 ],
					"text" : "<-----------"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-44",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 928.0, 224.0, 112.0, 18.0 ],
					"text" : "<-----------------------------"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-43",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 992.0, 376.0, 53.0, 18.0 ],
					"text" : "<-----------"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-42",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 437.0, 88.0, 18.0 ],
					"text" : "Master gain ----->"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-27",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1040.0, 392.0, 146.0, 40.0 ],
					"text" : "The spat.spat~ object embeds the Source, Room, Panning and Decoding modules."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-23",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1040.0, 224.0, 132.0, 20.0 ],
					"text" : "Spat Oper interface"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 528.0, 80.0, 18.0 ],
					"text" : "Audio outputs >"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-36",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1040.0, 272.0, 150.0, 20.0 ],
					"text" : "Remote control example"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-29",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 808.0, 346.0, 73.0, 18.0 ],
					"text" : "----------------->"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-28",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 336.0, 80.0, 29.0 ],
					"text" : "Control-to-DSP messages"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-26",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 128.0, 72.0, 29.0 ],
					"text" : "Audio source to spatialize >"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-25",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 400.0, 83.0, 18.0 ],
					"text" : "Output signals >"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-24",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 760.0, 256.0, 88.0, 18.0 ],
					"text" : "Input signal ------>"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-21",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1040.0, 376.0, 114.0, 20.0 ],
					"text" : "Signal processing"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1040.0, 328.0, 111.0, 20.0 ],
					"text" : "High level control"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-18",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 736.0, 584.0, 488.0, 74.0 ],
					"text" : "Spat.oper is a high-level control interface whose role is to provide a reduced set of controls which describe the reproduced effect through quantities that are intuitive to the user and perceptually relevant from the point of view of the listener. The core of spat.oper is a perceptual control module based on research carried out in the Ircam room acoustics team on the objective and perceptual characterization of room acoustic quality."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-19",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 736.0, 568.0, 163.0, 20.0 ],
					"text" : "About the spat.oper object"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-14",
					"linecount" : 11,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 416.0, 648.0, 154.0 ],
					"text" : "The signal processing in Spat is divided in four successive stages, separating directional effects from temporal effects :\n- Pre-processing of input signals (Source) \n- Room effects module (reverberator) (Room)\n- Directional distribution module (Panning)\n- Output equalization module (Decoding)\nThe reunion of these four modules constitutes a full processing chain from sound pickup to the ouput channels, for one source or sound event. Each one of these four modules works independently from the others (they have distinct control syntaxes) and can be used individually. Each module has a number of attributes that allow to vary its conﬁguration (for instance, varying complexities for the room effect module or different output channel conﬁgurations for the directional distribution module). This modularity allows easy conﬁguration of Spat according to the reproduction format, to the nature of input signals, or to hardware constraints (e.g. available processing power)."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-15",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 400.0, 197.0, 20.0 ],
					"text" : "DSP objects & signal processing"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-12",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 592.0, 654.0, 60.0 ],
					"text" : "These objects allow to deﬁne a selection of controls for modifying several DSP parameters simultaneously (possibly in different DSP objects). As before, a high-level control can be actuated through sliders or number boxes, or updated by an incoming control message, according to a given syntax. The Max/MSP programming environment allows the user to easily build a sequencer, automatic process or higher-level (remote) control interface which can send orders to Spat."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-13",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 576.0, 257.0, 20.0 ],
					"text" : "High-level control objects & remote control"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 400.0, 360.0, 28.0, 20.0 ],
					"text" : "Etc"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-10",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 456.0, 144.0, 24.0, 48.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-9",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 264.0, 160.0, 24.0, 96.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-8",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 128.0, 144.0, 16.0, 32.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"data" : [ 40089, "", "IBkSG0fBZn....PCIgDQRA..ALO..DvDHX....fHK8dx....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6cdGeTTzFG+2dkjqlKM58du2aRQEoHEoHEUZhEPEATeEPEPQE6Xghfn.V.EDDAkhJRSpRuWSB8.Igztj6xU1eu+wd2k6RtPBHHDY994Sxc2Ny77L6ryNOS4YmUhjDBDHPf.ABJzhpa2Y.ABDHPf.A+yPXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BxGxO+iTN3Q6ZjraDetjzcdDPt0UAU94erjugBm98sq2y0qU7Yd9i78vBDH3+3HLlKHOIsX1DVxhWJhwpa.5wtoGiMtSMNr3EuXr63thxukcAmt7yfqTtkGckI9oeZoXcaXuEHiNx1tDV5h+Ar6XiGPRcviiDAjJnFvkCHdR.Hk3NBV7hWLtbx1BR7y91CkzkSi2471GYex0mFjx9Wd0bdmWkgjjTNzS1eOfhTe+PQVoD2gvhW7hwQh4p4grEHPv+kQXLWPdxYW8LwC2u9he9TooXWBviwF.GItWzu92OLu082ftcftc+s.Us1M.o6H3Fh..jyJELrA+XX7Sbo46XdA.reocf9zu9i48m+cv5a...TAEiedyWfYmGCZryQXwrsuE8qe8C68Boj2YDexLOtcwms4bGtjeA6UyAM+Q+Su+xw+uGrRMEYEy1WD5W+5GV3NhI34QABD7eZDFyEj23czvRR4dpr86XRRRHRy5gQSQh.hHyc0KB.IU4kw1fjExq.BH+3md7kfrM7UfF0t67XZ7ulYBuYkqs7yqjS.jYBmAY5vc9JCETNOIY.Ske.5xgXh1EH3tQDFyEj+P4bYQJfepRK91edS3P6ZCvbHpyiH4m3fxTVaypU3JeLhoDpJ+9N.b6.VyHy7wHc1Usk7zwCY21gUqY5QNAlV474VAaYZEYXMCkxBDXGDxqYBv+3X2Z5vgCWAD9k17rgkxUabxqXKfYVvKYkgUjkybKWIIo.mJe.HQofbVIPff6V3FxXNcmERM0TQpollxmokBRK0Lv0psUaIm.hMlXvEubh9KIjQ5ohjSNYe+Y0VPZ8J+yQ99liLSEojp07HzaLYd2Nm9nwfXi8zHlXhAmNlXvoi4zH1yFO..jfDfrC7vMs5H5R0JX0IgaG1vaOgmAsuI0FkrDk.EuDkDkuDpQOF2JAfDN6IWOZSUzAClMiPjpN93ueUdzTd4Ecd9z4UwRG+nPQhJRX1rQntX0DC+keajk83wK2VUnbM64Q7dV56KrueGFJUIPy52mCYGWFewnFFTqQOLa1DTW75hO76VU.ZJutQHwC+G34FxChvsXFlLaBEsb0BC4s9S.IIXO0KgW8EeBzh5WSTspUCTtJUa7zu3DvAuX5..3cdtdghqpB39aayfdygAcQUZLfQ8pvpChytyEil16WFtrYEsuIUAEuTFvguhUrt2q2nnMbj3K9vGGgGtYDVXlvJiMEXO0K3QW0BUqZU2mtNax1..gLxcmLDbSB2Ygz71dWJohLugZixK2nWetdR2sy5.EbcGrXFrikUFVQ5Y53FNGcWCrffLore+7zK44I.nT19RrxeUu47U9rkR2x9mTm7Wl5PHfpriWcFMOaFJQ5odfxGXX.7wm5xXVxtKXYMY+yYt3LaoEZrBMzW91uOHIY5W8JLKmELYe2NGbFCT45rjTfWm86XOybWAo6rXqabUXDknLL0rby322RoN.FYTCfSeJOmRZd54wKewSQKl0S.vWa4akW5rGf.pX4t2gGT8mYLKOacPxc9oCg.fsZfSfG6zwxgT7nIfYttSkDOzplHA.mvRNBII+g2r2TBfy9uuBW2K2RB.18W9q4QO3ewtCPH0LdLmj6dgim.fqdeWHW5WV1AKWoKBA.6wneWtscrA9DsNBV1NNO5ljy6kteB.VlJ2ItpUuL9jOX8I.X0ZbqII4DFYWI.381+IyjR+xbXcsgTiNSbemIMdhMsT9xcqZDZBkSYdeM+ok7iLU6N4ZmROH.n4HJBW97deVMcQvXsZmy6kteJIIwxToNmstj.69HFOII28hlfx4+724sfZB2cSFmXIAV+WBr8i7incm4ngQRRFj1VxUbJ.juowcNZ665WXW2YK4q0OCVYAo+kGxxt7740Py4Jnz4yWePoZ+oLKex4Fo.8+9no.YwWx6j4Q.HAIMQC.fZe+OLt2ZUB3vsa31VZXqe2hwa8b8FKekOBl9m7oncUORbtc8KnOu1BPQqc6wv5VyvkO8dv7V7Giyj5aixXPOTqNL.TD7LO2.fZIG32W32iubBOLJYIWGd8AcO4wL0pjOTxZAFCSkCP2k8laY.deTxG5WPj0o6XzyaUXZCoy9KlfJ6qe9mj167v6L+15GaTnMkRObKC3s.0YxmDezbVFBn.1ColzYgcTerlC+snsESBE+x+F5y9SBgFVn..nNMevXhcuEPijLVzfKGFvecQjJAr3WQm+dNuLA.cgY986ARpCE8rk0.m3vGDc6U5Ll+y8MXsG7rXxsYHnnleWrr2+CvD6vvw68gKEU9AlI5esUiNeu6GZsTFL7VENNYLWEC5idHrhw9S3m1URniWiyeW1SF1SMETz5MPrv26EgAspPy2b7HCa.ttvefI89+NZQ+mIV97dZTTcRnissCPpmsDydKmAm2g27uDdi2++gHMpGO2f6C9peaJ37ojN5Za5EZyZ9D7g+wEQOu+GB0tTl7clKgJgYstcfdzfnPm6yPASZcXRu+uil2+YhkOumBEQmpr00R2LtvG6Wll+WqV3c.nVoYxFzwdiNU2RicswuD+9LGK5VHEGKaZC.l7EQB5wkLUtPHAJUPtVDjqXWKGsPxS6vRdcux7SCAO7.eNKBLNAUp4JK5erxqST+WtK0d9LXZh4oLBwnDnIs9EEQs6fQAyXtOBbk556KNQ7p2e0yd899hYhk8lOA58q8cXDC5rX+aeS3H6eivtKh2eg+Ld15XBjDSdDeGj0o0S2bI.tW7NSaZvjZfLGcWfwJ9fXka4H3MFz8.RFj0jzysK4LLu1t8T2HmoKhp1L7Qu7SfV05FlySmbHaYjauHNvIhMl87GHoR0JzjhoOnkODrPekNuyXa+GyagQVeiADliXWtGi4dOGydBxBuzUC5w9P+d3WBy5QCA8Y5GGnuFQndJCCUSo7bCsJT2lzBfMlBT62RFK44OuFyUIA.Y23TYkAn6rvK8rOV.4kSbhDf9dVaLzHKJduSrZLtmceX2IGNVymOHXzcx3vzMbl14PO5QO.7y4wNzgiGczPdcxCPYmvkKmvT6dPnWq2.BAF0AXM8KgyIIgw+J8CEUmRlWsdy3Iae4wr+scfDc.umEPkFkuVxJUM.Bn1WQkWmzy+5IRfsXHnqMHJ.HiPLEER+LWDmEvitT4QWV7nqSiDbjCIHrleKg19vCGu8i2I.LALlpWL7oex7QLu4.Pc8cqQN7kAu1lx2qEErKVY2dmR7W228QnE884g9PTGbITfqGj6HErtIP51mA47KeewM74XxmtkXNOdcCRa392M.ofDdNauk.N7yucD0sCJWmFyyFB.mtckiKtFQO+eeA59h9YrhCeJru3y.VhrDPE.VyzmFFzLdEDlFUnrs8QC3BhDbjcu0BwDJB.hO9jgMPnOHNWTxW7DXo+51gdCZfoRTUbOsr9HBcZ70kO6ImBV4xlE11tNKf5Pwi9TiB0rLQBY2.QnqTPK03qxThmdG3K+1eAolkaT0ld+3g6Y6gApBPBv5UNNVyJVD1SrNfdCVv82mgflV0HwllyqhwLgOCM3kdar6HBAsn2CGUv04v284eMNmSWHjRWELfdzSTsRF9MZw6cT3fYAfrMlS.3xWulxMEoB2CJRIiFmcSeDdpXpD5PWFH9rYNDnEddFnk7JEIHGzAj3cjG9cCsJMnA5LisEpArtCdFzlpDMTA.2NcgTb3F.pwK9CSFecKFN93EEOJYMaBtmRoGpkbhVJoAaoL0BwDygPjZT5jkszRAYnMLb1kmGmzR.RRpfJUpgssuJX2U+fdMYGl1PMivIwR+gMiGsl8.lTA.21wet83AzFJLDZfBi..tCxJBRB2LGCmNDIDhxIM..zpyLh..+3OrY7X0nGvnZ.3NSr9sGOjLYA5CAHS+K.EM1cSAeiYjdqq545WVDgnEPlQBc9aywUZXc+7xv518wgJcVPeF5Si5WFK.vIV7rmIL2jdfrNw5vNOPLnz0nEXjOV2v99yuGK9ON.zTl5gm7Q6MJsYM9zdxW7j9ZmyPQqDZSqaDhVuFH6zFlvS1c7oq7n3sxzBzqxNdjAOBXViJj541O9lu6GwkRWFkrx0GCYn8EA1Mb+P1F16FWMV1era3lZPcZ48i669ZAJhN0fxNweuw0h8bhj.IQMaZKw8zvp58DEq+mlKNil5CCVOD12AiAkpR0CCZn8ClUAj3w9CzzG9kgoV7nXVtKCBoLcE8tJYhO86WGF7S933nqYV3uNEPa64Pw80vxAIIfyench+ZW6GDtg1hTCziNeOP60wS8h.T.Wy7bPL+zaprtmqZeJGHf0Hm7He0vI.3au4XY5W73r3QZxyZr2Q9WG47zMc6KIinK0gPpVbUaXibCq+mY0JewH.3.+3ej4d8mby0NsAQ.vP0omFMnr9qku1MloY2MIcwEL.KDRfpBQGMYznxZ6VyGjI3Tl1iemD.b1q9fjj7PK6sTjkdiLrvLSURRrT8YZzoLo8TujR9VRhFMYhFzEBkp0T4tW6BX35zR.ULT8FnACF3G92ovmueMk.fFMah5CUKexILuajh16n3fyX.D.7i1ax4JLqw7SAcMySwtK5H03YIiPGKVodZlT5YP6Y4fjjtrdAZwrd13VOA5xmNFHkpPWYZAY83xHlkEvZlu6Y+zLD.Jglyoux0yeakeIGV6pJe4ede9RymM3pS.vNL4ewmj13jtOpEfRnKbIqaibM+zrXypbQ36ryj7sl4qJmqYtLorqrXMpXIH.X6d7Iw0u0cvubZikCdhqfxzMm5.LP.vpTqmm6Z26fu4yq3iAkuQskjjSXDJqY91tTlTVl7x67GIzn2itj4OO11R.M7k+rkw+d8KkyZSmRYMyawqyzBXcAcx2YfJ5px0944t1yN3a3QW2yfdQkxFOqY93m+NYPW2VA2vjwoUpq2tg+FbC+wux9DY3D.zT29HZ2ajbamisLEmRRpo4vBiF0qi.fyaqWjjoytVMkIhTmASLLSFnJIP.UTaH58z1CXXVFAs5hjTla5KGiRaSgpzNmDTwvKRI4UR2A+sO8wXHp.gjDMXz.MTj5vXSyNS8b6mlzqkZBQGCyhYpUiJhVOIlr6frFyxt3a0x56o8OCzjQCTBfybeIS2Yk.m8naOATQc5MPcgng.fi661Lc3lTVNMN5Vp3yLgZvHCyrIpVBD88KXr6+WoAi5oD.kzpiFMXfk3dmCOwxl.ATy1VGKLTcFnZIIFQYqBS0gKd7UMGpGpnJs5nA85nZ.hNNNdxTcPxz4K0RUDseF9VybAAmaPi4SIPi49vsmvGsOi4TlLsKcR9Eu6yRspAgwHXyF7rYptTpfMxtTm.cxpRVG9D+uYvKmQtuzkzI2DMqQEirn8kGJlyyjR5B7Gm9HUZPcrKlN7XLWWQqD+6ieFlRxIvUMuWRoA1Q9SzZ76jR.btq9fzsiLYyqUooNisj66LIQ6YZkyXjMlpMFMOYBV4u+9ci.fO8GuTd0TSgWJtiwk+GGm1rYkWXqeBAhf+7giiIjPBLSmt3PaWEI.35Ov44Eh4Hb+G4z2HEs2QwQmmRGml6gyHWgkQLqj.fic9qhzcVrMMoZLpRVVltCYd5M8sLLsJN0XDEs3rJ0nN7dFzmwyeUuFye8rMlOmGgR0radLlGXiN1ia0YqCRRY67P+x7YYJZD9bFoZ259w8bwr6rQLaeILT0cmGNcmzmDkyja7q+v.bhoNOn+GOY5N4A9oOjPBbUGN9fVFj5EOIm6a8LTmFUd5PJXue8UPYRZK0ywo+RCkkvywgtH3nd24ySdgTHI4q7Lcm.f64JJ0ku7tVJQH54pN7EUJCuxA4K22l6IOogyYCwx0N0dRoNLUldNKuS47b5uzPXIfm6SzEAe92c97hWU4ZyA9Ikyuw+C6UXK+lLdMl6+elhpj7LI6vWbt5o2LCSsD69qrPldl1YRwrKZ1Pnr5MsWzg6zY2qFXE68T3YRHUZO8qvAzxJRfJvU92wR61RgOQiqN0n2LO3ExfYF+dYo0qkQVz9xCd5yyjRLd9yy9EI.XkFw7XJIeEN8dFFCq70iwEe7LgDShtI4BmTeHPc3Z22EXV1yjK+SdZB.N0kpL3EYY54FB2Liy+WrXgJQC0ZX7Hm8JLkql.+iec47TojIe81WOBniuwbVMi8Bwyydh8yI+rJsG9nydajLc9RsBTUEFBOxktJcXKENhd0HBov4NO+U4UR3LrRQ.1w2ciLgDtBSL0L8YynE8eb7zm+xbm+52ykrkyvytl2iQCvVMr2ha+DmgIbw33uNmWignVEqcmeD5TVQWBi44O+CMle.RlaGPbKSsuDPMm1NNueG0MO7pmN0GpVB.No0dJR5Yj4nc7BIjHSLwDoK+8z7bH3CuJE89NaIvFd+vAVeBotvSloaN+9agQU4FQusnISar2MRhncSmo3Yj4yc0GjYkw4XMJlDUoUK68iMXN3gLXVrRVDJoMbt8KbVNklChZNNlj2rhrrOunLsC8EDHJt4ya0Wd30d1GT4F8x1D9Sa4TzYv5MbgPxomiludhpiCytBPsFZ.m7b9A9cewGxdzHyJih7c2X958uEHOU0UVL4jSloEjN7Q2NYJ9aHOfzYiImbJzpsrCofdUxZpoxDRNElo8bmBWNrxTRNAld5YF7DGLk3q9gLSNojYJoaKuUteo2oCqL4.zUtEtvaeu4hhwbUbne3h3Uhc6rTQZfPROWv1NGk8T+8Xq6cH.XwJdy4fG7f4i0uNQspAKW8aAs4LM18pA18oe.eWaF+H5JUgAvK4jjTlyeXMmZzYhG7boyyu4Of.fu2Vtb.WK+1QzbJg6mG1pLWv.rvnpTinU6d6VbZbbcPoiF2+.GDG7fGLqeiqEA.egYrobbF4lmcySUwP+1SJfZPxNRikqjQRKEqz7p1b4q50UO89nwP0vF25wQWLc9hsBDcel9LvNywnLKda8RYRxzYUiBb.y7.dDpWaFZ4bVyg7Wa7qGdYIP37uiKkrObVWksHLiLpJ2HZMqTDFyKfbCtl4x98oLj74rBxvVxGC8cxKClJVUP+qQQwUubbvRQJGTqREpYmdFro4FGZxi8AHgcdPfNVI..HgvPXQDEL4+xuGDm2fPYS2nDgG3p.YzfdHQ.2fvmCd5IOIAUPMzBf.2auUIoCpUGJzE5fvS+3CDZAvPGzf.ToEUMB83OTCHEsdeoJu2hPUXRu+2g1ceKAi6kdE7PspxXvu1Gi4+FO+0LMEFHmm2WyxAI.4jS.wBfNOtWAi4Q6IBSOPqaZsvZZTmf7ESHHd2a16pYRRY6gtziCLxf4.jpCAgGdH4R8D.Rpz.Kl7kcxN8D.p0gvCWWNRDyo60FTLFVX44ZOpVqQXIG0ICn5at7HMYPea7KRH7HCC.pTp1FryW+9oZMFP3gaDA2447bjBv4ifqWjgQKlQQJeyv9+6eAOc6uWL3tbOHikuYLh6oTHTMFgFIfG6MeEz4JaFpAvPexwgPMYFg306Nc5NfqMDY.HCPHgnKtF+NtR6bEKbCADeylTp65FHH0YCAgYtT.nsXrO4vfd..4ACJIgRUtpmi3pBRd7FdcgpJW0fH.TqVBZTm8MqZzH4Qkd7Hc.fzy1GPJlwv8b7fT2y6gznAktjQDXPYBHoWMzoWQtjtgjJUHrf3OMBt1bCtCv4cG4REb3vEb3HKX2tMbtCuI7jMq13hYQzf5MLTDyZwN9kODuzWtQjg8rfraWvVlJU.z3Ta.Rj4XytPYCsR1igYkFtBuHUGg.fw+ZeAxvgKHKKiqdjkfYL2sAsMrUnH5gOuY2ka2vsaW3B+0rwh2sCXnBVBva+To1.LYxLb4dgnnUnNn0soMnUsrUnlkKLnRxHpVipK3llDl0ZOFb4VFtc4DY3cyhfJCBOc6NfKY2HCaNvIiMAzptLD7G+9OA.fM92G8Fqn8Nbx2stzhVeL7ADFV0j5KrXQOpZH5Q4pWmfpPMiGs2sJO8MKIII+jrWi5x433..x4YNH29UueF1jTpi4sZl2yib6EsABCHz79sflO4l8W7SlRdNt2ioJ6NXDPGOCtgX+0o2N3D7acKH638BtQgdJeirBsCu8B9THkbrXxOxngM2.FKdUPHDX8qOVzhV1ZzlV2ZzhlUeDlJ64oz7hj+eQRFgWjZAyRY2NmaY2H4iub7lu+FPH0qYn3FA.IbIKCGtcAWtbBGxghRV4JCfEB6FqfR6YstUnQUIhf52kVJZsfIPLwm9SPJ1c.YYYXOyLgKIsnd50hqlTZX6m5xvgK2voC6X2+4RfM6tfgZU2bUyiAb9jc87jsaGtkcC6YlguxNIe6ZxJ2WTsF2.PaIgY9yaA1c3DTl3hGc2Xy1sgHLYBZTqB4SSNB7x02.4UlRoSu7WU4JVzklUqZUgUsJUlksTEWwoNBw.e+EtdlTlJS+yZWv3HfJFUYpHaTCqGC2XHDnBbCwlJIUVybIzCllq7eJOc6Hc9rMOZBnhUpl0kMsIMhpUqklJZ44ee1THoKN2tagpzDJqaCZDaT8UlloxzzNyyZ0Ey5xYOM6TVla9qTbZHKEqLrgMoorVUqhzn1H4duPZLkyuWFoYcTsFsrtMnwrA0qVrHc9iX5jzQ7qmUFfQV1ZvlzjFxn57Gv6ur5XEpYCXyZbcI.XyG3n+u8zc5Ys2BFtckI26e8m7mV1h4O7C+.W9J+cdgjx85tGnrtd1DK7ainHXxpfHHu5LmKiP9nOewKHoKuUqbN9LOBNe4ZsX3W2EhBJf3cMy85HlTlzsyL3PKUwHTGJeyksa51oU9DkpHTBZXkpc8YSZbCX4h1LqXiZEs6JM10pA18osWexb7inKTkTO3EyRotzxmPanVCV3AOe5T1cV705Q0UZmqF0kMsoMgZ0pggXLbtoieYRRtwozIBngUu9MgMn1Umu+FNKS73akFCUKMXIZ1nF2TV2ZUUZ.fu7L2tO85qlnqL4S1fJS.IV1JUK1jlzHV1hVTN+ijBS5DKk8nrf.QyZTuFx5WiJP.v5z2WkwlRVjzJGSq.Q6mN8tXOe+q9z9b1SRmrdk0BUqtHrwMpArZUn7bye8jHznmqzqil5MiXOQ9QunxFkTYpRsXSZT8Xws.BTGN+0ECISmis0JSydlhJ2WSttWybYYYdwM+4zhkvoEKV78WKa88vQLtowsdzKFP7cmkUtf29YnEKVXXgYgUtkck64RYu9fic.sjVrLbOdwYAP+LU9yu6XXzVBigEVXzRyGDWyAh2WnGZIyfcp00igElmv65ywCdQk0iwteqYt2FFO4FlEqbYsnDWK0ki8CVDc3Iubg8rB9HM1SXgYgO7j9AZ2Si367adKFUXgwvByBG9GsYtzubxzR3dkSW41NQRTvsRby.2cojI+OheJH31L4nZTFwtVZwhE9ZeyZCH3LOxhnEKVX3VdbFiSRY6mgu1i2Sk1drXgMqyijqXOwQxzY+aoENr4dXexbJis+zhkgyK6YgfWy60IVjhWZdrKjAIcS2xdZmySaLVZ7.4x1447jZ2jLQ9VOdG8nqpve4PJNA54+6umMuNV7b7JyGehygI3wGRxEYcE9kSbPLLKda2pGbcmUw+7yL83XmqPY701WiezoPGt7dlmFmb2svvG02SuRdESYzzhEKb+InbBsw48ZzhEkxgRVlmi6akuCCO5Rve+H41QSc6zN+fmtqJwOrvXUpSi4wRzqyEZiS8gqOsL7EJVy77AIxavMy4frl1WabCYYkmc3aFKomrrxz1nRUvWo.kvkfJ+dVEyJ9sCcknEXtq9fXXcp1Ym8IgL40krxUdfTYMikIfJUh2fM2VI6McBkkDW42Lna.QBDbyE+aW3eZcNYY2PosGffszJxxx.RRPkj+a1UDxx96CJE77afnHmf01mmfU1brBpNTVhTY3Muk+PJCklOUAofrQcI3Zy0oCv4oQQ3u+WbMJzCvfuZDbakW28J..4thWNuoIXUL2vx+RuAFnFkt1U37JqblSCPGdRePqzK3VOdt33ooGeG16NMmx2EWaDbcPdzzjreaZqAi.ZuHW04tFs24WPjtAfxlVDYdmFkNLnroIkc6vJFeKniRKv127u87faD2WtQJaGdKn4PIuCpof0YZIIU91IHyucfSA4lquRHlsi5jcEkqgHJ.scxBPjx6IOHaG9I2UR768YsmOi+PoAIb+nastpAWt99YvcjHoqQXYKOg2ZbqF+KgcmUZ3BWHAjU194Vtiu3MIlfaDxillBtg7b2diTPaN3Z+zf36qRp80lV9ssPKIoN3M6b8TumdUe9aRH6cEu.OV1NrbNkSd2Y5rkx0x4MEFxKHbiOM62gBQNeRfjgu83RGYhq5z.hzn+Q9ZHq7omjzyi0Td22aQOJuUyQW8jPs5x5v9r9W9sGY6EYb4ScTjpgxhJWRyhqDBtEv0y834eby8niulQz2W88HMJSOuLCxe7JhqY6b42DmluSrZvOWx+keH2oq.W1bWJ+mqTQx2+7hprOPH9YHOWwKHxJelVVofXHOPw9eth2+UgLueTv7hrJs.v.TmyGsQ.frNIFRspMp9C94HCeGjADG5ceQOG50+OCVX4VYhGKr6N454d7B1ndKPxTJvu5qspqik4yaLyu8NhBZ9H3D7yk7eIuxc5JvkM2khnjQvcrDnC7DDik9rqJAYURdbfFOO+3..gVE7JeyLvOL69Cy9NnxhDQxraDz+Q3.YOiVwuFbnL14Bl.dtu4H9dt2UVmR.Jyr6Tf.ABDbaBgwbA2wSpW3f38dwm.8pWOLZa65E9iCdNkoSzmQ3MhW54FNJVDQfHJeMvur+j7jRGXSqbQX869RvqmBMZ8...H.jDQAQUmANzerPDd3QfHhHBDQqdBbrqjY1JR1Il1nZIBOhHPDQDNZWueFjhcWXiKXLn4CcpXdOe6QqaSwQue8UB.23GdiQhniLBDYoKOdkYsdj9+lEJBDHPfeHLlK3NZtZbaAMpI2CF2zVBN9INBNyw+Sb+0sr3km6JgaeidNKrtuYonrUthvX5mEcq9QiYsgXAfKjXr+ENSJN.nJj7AWD5cGeTnIrRfpU0J.0act3QG7ngMBPGofOeLcCi8y1AzGYIP4KUwwQ984gM7cuAFw6tZnB.NBwHR8pQAqw6DmdESFCXRyBQV9pfJEsQ7tO6DwEsIlpcABDbah+sdf1EH35lLNM6kdsDncbcwjBc3zAyLsDYiqS4njon4IRzJOzZlBApBW6oRmtc4jws2kR8ZAqZy5.yxsxqOwtL0MS5zJGRHpXzMe.zpMGzsaWz5d+bBngS+.owis3WjRRRrOu5bYF1xhtbjESOkyS61yhW87GjQoGbHe4AoSG1oCWt4NmV+H.33m+1nKmYwKDarL6WyJhWaYBDH3eWDiLWvcrX6hG.KylaLrOaLnCUvBzpQKzaNJLmmqmfVy.m5xY3Yl1qNJcILAUp0fxTi6AkRuA31UVddZCTFsrrizv7cHij19pQ+6auPO64CgdLzmF.tv4Nex3naeGfpBECpWO.LnKDnRaHvjkRgPCMDnUqxCijF.nQanPqZUnpca.nlUtTXpCoEnZ8brHUMgC8A8wxQf.ABt0yM3aMMAB92.W.PFFCwyaTInrL45MZzWnJGyd1NCmrLRg.V7JBu6MBpzhZBfvF3ihucluouGvEY2.gXzD980prSZIox6y1q+34MZluOArTodf8suVhO6EdD7BydFn9UaA3a96ygGtlgeytPPf.ABxWDCgPvcrDZQpM5TIA9r2e13m13wQFYlAR77mDu5a88PsdCn7Qa..Ja7PojZRvVFohs8aeIR2VlvXzU.pUI4aj4RgZDssTFw1W8FwJ244.UoFRx1wg2w5v9RxApYqaEfKaXFyYN3HwcYXMizQ7m6zHKWxvyKcRbt3NGrmYp3rm73Xeq3GvebfjvH+zUgE+IiDNxzJ15QR71UQk.ABtama2yyu.AWKR8bafkx.HjzRSlLSi5Ck.f850WNcKSd3UMQBnh5zahlMajgnADnm7uujcRZkuTqfxZlSRmosa1X.BngFMYllLZfZTKw2duIS51FW5jdHB.pIT8zrISzfIKb+wmFsmZ7rDQnmRRZoYylXwKa44m0yRS0pBggElEZPeHJu.e18Ut8VXIPff6ZQLM6BtilvJcaweu6chk98q.m2dVPkpPQ66yvv80vJ.IPTtlNHLm2VChIMqvsKIDQIJCdhQNBDsNU.vY.xRi4Fhe9j6Fy4aVFrkkSnVSnnY26CgtVekoF+glzOh0UyYg+XOmExRpQnFLgxYQGBUmQr9U783aW0VgSpFQWzViA+nUEEom+F16QOCnJC3A5+vQGpaQtMTBIPf.A+Gb6bUv+sgd1FHYN1NHC32ddaNAoTvnZPzHl9sQ7qiq0YGWbc7p8w2FscAYKnTf.ABt8fXMyETnBeu7IxgQ0.9sD.bmAdsmYXX56SFEqTlBLtWWJL6DHLjKPff6TQXLWv+IQRiQzyFVDDcUZH9r9UGOa2phM0EABD7eSDSytf+CiaXylSnWuta2YDABDH3VJBi4BDHPf.AExQLM6Btq.ly2yoBDHPv+gPXLWvcED7277BDHPv+MPXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AEx4Vqw7aBaG1ASD426FFwtvs.ABDH3tIt0ZL+lv1gsTPrLKIcsE7shcgawKWNABDHPvcpbS2XtaGYhkt3eBmLda2bDnD7LTaY+Nnbvi6sHN01VK9sMePwH9EHPf.A2QxMUi4jDYlTb3wFXuvGs9ydySvR..p7a9yu1YamoGOF8SMJrwik3MAkSrko+z3Ue0E7ubWHDHPf.ABJXbS0XtjjDjTC.IfH0p9lonwE14BwZiyJ..X9ZVUF6dqqGT0MmIbOD8x.T2MEYIPf.ABDbyFM2TklmQN60Dp0KcXr8Cl.JQQciidxqhHJW8PqabUgqKrGrkikHjfDTaHLTsZVGTpvM.YmohMugSfFcuMAFUCj14N.Ng8nQkMbUzfN+z3geaCPUEJKZ481PXTsWC5pP5W4rXcaZ6vsJ8nJ0uEn1kKJL4O4SP0KtY.Bj7kOK16AONb6IEUnrECwFWBnnQqAm5zWFlJU0QKaVcgIjINw91N1+oSB5LEE5PGaKLoUExdI5ciyd38fcdzy.sgZAspCc.Qa7lamVDHPf.ABttg2jI8KeXpWC3DVxI4g+hgS.IFhlNxA0mNQUpBguwRN.O3LF.83ZaDpTS8FMy+N1jYFwuYVLzBtmzIkI4V+f9xVMhWmcyXnJwEfRne7H1IIc6Ui7U5fdhZzQ1t1UGpodSgIa8BD.bga6RTN4cy5oWmuzC.9Vu9SP.PspaAelQLPZPkVVpgsDdhe5+wP0.1zlTEBHwtM00RRYtngWV1nVOdl7QV.0GRXri8rWrNUPhSYCwcyt3Sf.ABDH35laAdydfSsslhTIbfDVIl2270nhF0fqd9KAInBU79FFbRW3b6esnbgkN910sEuo.Z7HERBntTXoW57njQYDe49RFx76Q0CweWQSFzcHn4sqa3Cl8Jv5mYeg+SHt0y7W3nU9AvUxhXme6D..P6qRYfjV8Xw6ZIX5Se939ZRUf0csOHYrjn6SeyXCadu3W+ftgU9saEI6QNpfDTqVMz3PKdvG4IwOugygGtlE8lewm.ABDHPv0I2BLlGnOeGtkvQozqFpBw.ZmJ0v9UtJHHzpVCz.0nz09dwON49hMu+K3I0WBo5lfj98HnoLk5TqxuTNt2rdX34+heFGbdiAMqN0D+QxNgmnAI.n2bwfyirZLkY703c+leC..Zj.TqQCpTzV.ToFMIDCP9Lwfh2gmGuZz6BUrnk.89UVAfQ0v+IQ2XU5OV4beZL191ET6V1Ij0Mo0jWf.ABDH3eB2hGYtGixLfi...WxtA.g0Keb75yYSnwUoXPRkQnQqUj7UsAImohidpijsLIPBwmFxJsDQBo4vOc3.m+btwEh6D3cd11gozsdiMc4r.fRW.zTwdgA2lvvJW4xQIpQawJ1ykPz5j.fTtdd0u71lMZxflB95+5nXaKdh.50.ucNg.HqDOLrWsAhiuyeAsqHW.O3+a4v9MuBNABDHPffaHt45.bHai0JnXzDRR.xDY.BydB4z+9bgT8NEJe7aBwcEYr8kzVnwjKDdzwidU8vQ8COBrqqbEzpmEPRkJHoVBS39JOFOIJ2z1Ohaz00ijbfu8E5Hl19pFp.NJH5LJRXgB..Uf.PMRI9Dg4pVVzkdNPzlZWbjXbR.TFTVF.D1HAfLR4BmCNxzF90ebFXGuwTgT0qA5wCjJFVo8XLO9sfN0lQhZUq5fCe3TQq6WogvG2EHPf.A2tQ8jm7jm7MKgQRnRsVnIDCnicn8nbE2DLU7phV0rFA0pUA8RpQ0ZSGP3ItQrniGBdo9cen10uSXhe7Wi1Tyh.0Z0i1TuZiHJU4Py52ShQ2mZfp2v1iFTiJfGnIUAQThJg1du2Klv.6LJskPwYNyYfjjVTk51LDUIMiZeO2Ol5W9lnAE2HjTEB5PaaJV5n6OV3IUgVVyhfMuhuFCcXuB5v.dLT8pWKz91dOvPnpfNIUnrM3dP2erdhFTbs3pYnA8XLOCpZXQfFzuGFsrJk.Eq52CZ98zJzfJWIXJ5hiN9f8Au+X6ELnV0sjcbNABDHPffBJRj2J2mRkQvlI+CMyGA8YElwwVym+OR5m4LmAgGd3vhEKAOBNtD5QnkDUaFa.u2HaKf7Egj5RgYu5ChmrS09ejt8MADBK4BDHPffaybSeZ1Cjfuj7tcmFrKa5elnYN9LXnILLfms+3UduGAGdsME1RJFHUr5hZU97wKzIBtQZOGmTFRRhW3bBDHPff6L3V7HyCBDvdBmFmKS8nJkujAbb+MfFn2rGb6qm4LwhvCOxfNxbk3q7+LRMQD2YuBznSOpTUp.z3mvnhxfjjTtzYt0cfyzPdEeABDHPff+M4eei42DIemlcABDHPff6BPLWwBDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AExQXLWf.ABDHnPNBi4BDHPf.AExoPkw7BwNdu.ABDHPvsLJTYLOudltClQdggeABDHPvcKbKdGf6eNAaiYY26d2XyadyH4jSF50qG5zo75No6cu6nhUrh.HuM7KPf.ABD7eMJTtowbzidTTm5TG31saeGyjISHt3hCQEUT2FyYBDHPf.A+6SgloY2+9bT0pVUT0pV0.B+i+3OVXHWf.ABDbWIEZLl6+zlqVsZL1wNVeGyfACn28t22txZBDHHenP3D.JPPgJJzXLOmLrgMLLrgML..T25VWDVXgcaNGIPff7BgOrHPvsVti2A3xKToREdgW3EvW8UeE5RW5BTopPa+RDH3+zT9xW9a2YAABJTgjjDt7kuLxLyLKvooPqwb.fpUspglzjlfW9keYwqiTABtCl3hKta2YAABJTQ6ZW6tthegZi4pToB+5u9qHjPB41cVQf.ABDH31FE5ma5niN5a2YAABDTH.lm+Pv+bDEn2tIniL26TVuwMtw+syO20PyadyQngFJ.B9Fii.ABt4hDTL4H48G9g3dv+ob6nry2Uy.95cqDTi4dqTul0rFLtwMt+UyP2MPpolJhO93Q4JW4.fvSeEH3eKjDM5eqi+0KaEFx8mq4ZlqSmNXwhk+sxK2UQJojxs6rf.A28gjL..t5EhC6ZO6EG7DwAURpQXgWbTwpVATkpTcTxhZApDFGJ3P2voa.sZT+unRkQlobYrle9mQrI6DUoC8GcutE4eQ8emG4xXtX5lt0iXCzPffaGHC.UXGeyagVOnWEtfxrhk88iJSD+ec9LQqJk9aeYyBQP3Be6iEBFzV5HRJ10fH+WSypvqNxdgosnsC.fV7Qs8tdi44xA3DFxu0inLVffaG3Fq5+8Hn4C5UgqhzBLt29iwYhOIjZJIgybp8guZluG5y81NDlt.Gg40SWuckYxXE65B2f4OliOyqvyqXb8NHg+4CpPBDEsrcCsuY0DpkKX57RGZiHSGtA+mneWWD6521NpY2dejXpogeej01m7ClNu9nv4fsJT+noIPf.AELjQFwGK5+z+Q.ikE66XqC0KxrG8cXVhDCcD0CCcDuXtRo2tdmyYszUVY.6t.LZznu3rjO5YvL07zn6MtT9VJ27a1NohvgjjDxzVFPFgBS5UZZNiLx.tjjfECFf+KLbvkVdqCWYYENgdnOTI3cLbj.RR.1rkAjcqAFLFhR9zSF2a92t0zgJclQHZxN+lslzhG3s+Y7.4z.HAfrcXykVnOzr6bzE27bPE64qi3hKNXHj.6zja2tgMa1PH5L4SW9iLciLs5DlLoCvUB3JIAzvN+.HpvLqjdG1f8rbCcFL.0p8ebpJ41rxvJbAUvfACP4xgruxBaVSGRgZF5zdsKGuSlB8OZZBDHPP9iJjPL6GNxxAZPmdXT6Hx8znm8zs6Dy5QJOtuwNOegk4U1BJdnkDy6noAhzvBG4i.SlBClMYBksFMEe3udb7ZOcWw.esEg+Z7sEEqrk.k3A9DXE.tS8L3MepAg5UghixWgxixW8Fgm5M9bbUWD.Nvm22xgZ2ltfGoasDlLXFgGlQ7buvDvy9ncBQDlIDtkHP+G47wUkCzfo2estELdnVpu3T1T9ch6Z4PRRBu0OtW.jJdkteOPqNyvnNMnpMsKHtDsC.Yr0u7iQ2ZQsgQClgIKlQ2F7nveedq..3Od29fh0vQh47gONLYILDUQKNVQLJ94S.l5nS70Od4QD0oWvJ.16x+PHULIzoN0VHoQOLnSKZz8NPbjDx.mcmKAMq2+Oj0UiG0olUBEurQiCeEqP1gML4Q+nnJEQCLa1LhL5hhQOm0gLA.reHzonBEuzbW.ZYcqLLGldTyV0VnRe8wwAvhFYCvX9peCadFiDEM5nfovLCMktFXPiYhvomBnDOzuimcHcEVhvBLYxHJZYqNd4YsM.nB+8JmMZXo0BClCC5CQE50n+.X8Fp90seDFyEHPvcEXMizfaY.CkoU4iCtQ3Jky.6Nk8aluIfy3QlNkgyKtOrfOeQvQWmJNdrG.cMoSiUehqhF2k9htUQ.zkwgOe1y.e9q1QXvcFXjkut30ly2fCTmgf288ecTwSrWLmIMRD0P9VjE.bk4YwQ1xZPx07oQrG42Pyc6.S+ilJVz5LhiE2AvD6ZkvOLqgh496WxS1QY9r8dJ3P1NHxda+TlJgmYFtfyytCrzUtYHMv4hc7GKDM7faAIloS3xVpXjS90vZ1ar3W15gvVW76g+7alNdnV+gvkDfrSWHg8NK7hu0JwO+kuKJBMg5Vrf3GARDYk3YPJVc.RBZKIfq.bBVUbnymD9fATerm+bI3O29ofcmZviz7R.nOL7Ve7GfYOs4hxZQGhc+eKd8O46PHkazHtXOFFcWpB9zm59vrV+EA.g7UcfO3IFBJyCMJ7NOQcPoJW4wOLqwC..CMcz3wZeMv9V0rPKGvXv124lP0CMErnY+IHYqN.bmIZbmeDLiErJzomYpX66bi3gJ+Uv110EgqKsJzztORruKzQrr+9XXgSru3m9jWBuvRN0MZUraqHllcABDbWApfGCfY4zuil8jFGzoBOmGRhHjHJNJRoH3g9IL6uQEdqyednmZggPqO1x6MT3tsOHdnN2R.Hgqd5sgEZOCzim3SwxlyyAU.neN5Ndtd0NLiuaPXySty..ntC6Svu7tCBp.vW7iiA0r2aBq3P+.pXTZvnl1mhOZk2ON7ERARrj.R4bLXL2eWBffPazkBlCGf69awwdr2DeksjfNYsHkSsLb5KjAJ88Nd3NwSiDjJFLEgAjQVeIxHqWCpHfDpDl051A5ZChB2aeFNB0nZbnM+q3rVUAIP3JSIz5t1dkG2ubT98Iu2DQsJUDnJu8Tv6tnGDG+JmEOa2dHzl5LM7oaIcziN1KTLyZAfC7yy6i.Powi9+5.NvANAJWqd.nYwaEKewaEisEUCx.n7cZN3qlzS.ypeVLhzbfvzGGp6HeGTqgNTzvJTbHOzeDOdOdHnJyDvvpSIvq7mwB.IjYJWB1RIYTz50ern26EgdspPy1b7vlcfM+NsG.ZwyLqAAMW5DvXKdXT8hsXLmObkXZ8cLvPApV0cNHLlKPff6JHpRWVnUKvE15GCGt5CBUiDB95i5w3tuVGydjvR.f5pBl012JVS0ZIl1D2N9nIdDb3T9JTyPyxSzytCBYYKUH6VF2+C2UH4cQp0DApQsaD3JO.RLQkQTa1fQeSSp4hWJnB5P3dVq4PMVZDZn.g3IS3c4.jjHxtKJlxNuS5wltDfgZgO6yGCZQ+mFFTmaMFYXuLtTRuCx7JmDY.fLW2TQ2WW1m4FiJb3voaHKAvVLDz0FDEHIzYJR.XEe4ndP7w6K63u4ymRPKB0JoTZERIqIZG7q6FjJkidNGHri316w..vqMvtGfLbb3iAhpAI.Lpo1aXVM.fVDVXZAr6Robfx.PKZzC0cL0mn13Ul2QA.fFcl.Hga2NgraWvXa6Izo0aIbHPuNWHlceX.jEl9H5Olg+OUCWdWHcWDFzT3ZsyusYLeO+xbwXd+E.RUnwOvihW+EGFLEhJ.4LvtW4xv71vAQ3RIiZ1wmD8uyMUoJqa63E6e+Qbp5LVz2+TPqDv49sOCO4Wra78e2bgtLtL50.FJZ8S71nD64kvWsY2nGiZZ3Y6SiPn.XcKbx30m05.jTip27Ni2bhiFE0Tn3pwrM7wu93w5iwMLDVTXt+v2ixXR2szy+su8seK4kOgUqVgISlPaaaauoKaA+2kxW9xiErfEbKQ1YkUV2Rj60KEspsDipClw6r1cgl0lmEy5GlBZQYy9goJ8DOE97973nge9RfS.r6cdLjI.L.UvdRWDYBOiiOqjvUbWMb1ydIriMrBziA9TniMObD6QlB..x.Y6zagFpQnRkJr1U983YtuI...BG3X6eu..H7HCEI3KGjsCYQ3+HckCXTuTBPYrwRJFFkAH9Ur+KXGUtx5PJomT1Rz1UPk63Tf0DFKl1X5Ddsu8ivOtgAi9T8lfH.PIdjYic+MOIz4oO.W9zGAQZTqRhUCnE9uQ6X.iZAa.8LYuYDf5WT83XAszVkGG6yMHBLuKyr8icIXFM3daCv1iE+vd1E5a8Kpx4psKhSdIUPkuRG+JW7kVO39xXBUqNXo0b.XOGcMHtuXf3gm49A.fFMg.UZz.qq8yPFN5ELEhVOBQCZzCzNfeYM3sV4twDdvZobXmog8crTPQzHozorBSa3.7Zvjm7juVAecirrrmuDCaJjHzpmlLYjZA3dOSZT1sc9BMtVD.TmACznA8TBfkXPSiY4lT1QZrrkLRVxxNblkGQcz4LbhvqBiyZVz5ENHMqWCKhk5vPzYfgpUCgFcbqWxF25mMDB.pUmdZxndpUiNth+9rzk8TXCpbwHfFZNLyTenZnTSFEiyg6apm69SJojBiKt3tkI6MrgMbKQ1B9uKkqbkqPorudQNk8wGzndB.BKEmS8ymGW5O7cbRiYvrrJls3h1d7bsSocD.bhe614w14OxVnXBfSe+IwzO7WxPMZge2Z2N221WACOTvRV9mj1kSkisQpoD5HWxl2Nm3i1KtxibVV5hGIAj3C+xeF271VGGWWjHfJZHrIwjcameVWkXqd14R5oMsyskOfRn07PYn76zu7gYT5AG9WdvfdNs+eY5TC.avv+TdwXODevlWVB.Nt4sSl199XVt52N9Ka8fbVCuQDPKmypODcZKU13xDFAzvQ7lKfaeaqmiuuMmQThxvTyxMW6T5AQKdcld.EdAS614WzcPT9tvzH4tW33I.3p12ETBNqSy9.vmYtqfxTl+7XuGBI87CVx54VWyh3r1zo3Y21WwPj.irzUlqdiam+1urP1.cgxVOzkPYaGf2K.+n8lTfp01dYcjj3.l4AHyJFNX.1hg7dL0zSluRyqGUGpQt88eXlhszYMpXIH.X6d7Iw0u0syuZZikuzGuNZ+bqg0TmVB.9ju874V17uygbeMmR04iYl23UwtoQaaaauth++tFy894k9SVQ.hd9177IdUdjMrZlbFN4A9lwPs.rK+uufm9RWgW97mhS9QpAgJsbtaOlfZL+veghw7yaMKlwENBMqWKipLUim7bWgGeGqmyacmlWdOKkPBrNcn27HwcIl7UtDOvNWKi+pYx8MuQPfJyO8WNLslYl7D6bUzhNvF+7qH30cuIfvXtf6z3loAWecZ+VfruYPFW87769rWi2WiprhQcO+Uy6q+bVe2xYBo6fVuxIX2pToTBqjsjuxTFKi.fy+PoQGodbNgA2CFhmzUq6cfbIa8bjj7vabdLTsp8Hy1wMeNaLyDiku8n5OgT155k9fulIXyMIyheRu.a+y+U9xeWbqSi.cfGxJIoal9UNLix.3yL+ilqyEYRZOsywWrOMymrepQ7XD.b7e+tnyTOLGXSqiuvZVuGEOyUsQRxysyemipusyWXksV2GesYuD5Tl72lZOoTGlJSmtytga29ec0smqyN4BGTkHpYOYZjbOKQwX9ZNZ7JwJqSxGBfi8q9Ukx9qb.978nwdzoNNm0eFJ61IWwW8FrQUQx2waaeeFtpcbQR6GlsGfe5ARIvS7LO.qGj3H9liRxr3Fm1nnI.FVTEgcscESQNsdDLlrHS8hmjy8sdFpWS1k+i7yVmRYvA2H6bSC02wqXc5Lm2ZhgYOTtaUVAxeti0XdfEIIxdTCkBvF2wAvSkRljLS9AcATUHCkw6e5RZWrZ.79l7u5mw7mHHFysQqdLl231Nw.z1Nl+KSI.9+97+JG4pL3L6ixEw522AygL3Ay97HOHA.K88Oc57l1YefHLlK3NMtaYj4AfralbxIyjSNYldlJ2sGXGQxhojbJzkSWAM4omZJL4DSft8jFuo0o0TXhIlJyQeZnMqovDtRxLcmYGfrrbt57SAKum6CYMsTXZ4XHkxxjxzES+pWkImp0fJhzSOUlRJJ42an7R.5K3oOfiK6lIkPxLUq1CHLYWtXJImBSIyLCx4W9OSoomdpLszU5nRZojb.k+xT45UhImBsYOGAPxLRKYlb5omsVt8YC2GWuFy+WaMyCbkGhBeyFO.V5b9LLlWc5nxguIrgitIHkAPnMNJ3a0pIfTHg.y.vlreaVCxI.2x.PM.zJ4Q1Y6gm5ZQi7kdHo3yID.RdVKnrW2EMHhvq..pFl3HGBhvywe1g+BHDckF+atSCKPff+cgPBgGd3Abr.8n8PfYKgD7kMk.lBKv2aEdSqFiVPTF7EMes8oynEnynmi6YM0KH6Fj498HhrGOZO60XG.vn4b+dzPQ7pgoHhHPY52FYiISg4eJxmbSf5LmGKmmOxfPk2Fg8oBUHxnytb2aZjTqFVBOvygr23cjPNKpxY4hQil8IKyVxw0Unb8xDxAdDfAyg6m2qK6I+VHZ8xwskmybBj1oQ7VsfGa7eJV829B.3B33wkNpYyaDrssO.e7udX3zkaHK6Da5qlA1E.ZYchF.RPCjfCW6V4Y.U1M129NXt278r6YeETRwqNKZYTd6j8WqZ9HC6N.c6FNbXCxxpQEpccAvZvecwvPqZUqQaZSqQipdQfMmxExtTJPffqGJHFRyS+eJ+RpGaA4YxuN1RmycLUkiOu94FeKkNX5LuyGp79jAbCpMeN6VPxu45oF7l11jsuGhwBUbava1kP5wsFTyl7hH5niFx1SE..QFYX39lzug58S0AS9AqClQwKFz.YbkDtBhp3cCSnaMDRZAdglpGPtJtL...B.IQTPTEOyxOOpQkJGBQRFm+BWDH7p.W9oA5mtfDPYumgfebh+F5yaLKTjUsXDgAsHKWx3q+i8fG3I+JzfYTK7QORiw2L1hCMRDW8JWFE69mKhcMUUrq5HPf.ABtimaK1pLVi9g2r+OHBQsDB0X3n+ialnqMrBPi9Hwe8m+Nl7ftWnSqVnQan3+ydm2wGUEaOv+d2MIa1jroG5oQuKffBHQPrgfhumEDKn7CafhJXGa.VAUr7riHh9rfnnh0msGfUJ5iphnhIABPBBo21M68d98GaI6lrogoPByW9DRt2YtmyL2x4L84Bl0CvV10G3dNWZlYr7Mvhl4YiEyZXKtNv68rWCozsDITSZXJ3fIojSgN0oH8SeZlsx4Lu2k2ZgWEIDYXXNnPH5Xhijh0JlsFKe1G95LiyKIB0RvDTHgyTt8mg+6qcwJG4JTnPghVEnIRMuebN+4Oel6bmaitR8zeMhgABfISU2soggqlJOPg4IbMMSUqeTbo.pwVIQLLv.MLYRqZQwvPPPvbMnyFKJnfBH+7ymjSN4lDYu4MuY07LWQChTRIklj08flZYqPQaUFyXFCqYMqodG+VjEMFuC3ASlbuCB6iuWw0xPXUchKUYmGplbx6Rv9dcUcrWXJfCrMWEpnxA8PU0mBEJTnPwQpTmNyaNbpoUkCpdclO7GbC02KqoavTnPwQe3qcCMMMRIkTZYSPJTzJiryN6FT7qSm4JmZM9TK8rgBEsIvW6Fomd5sfoDEJN5.0X7pE.UAjTzVFUgUUnn4GkybEJTznhpvpJTz7ixYtBEJTnPQqbp09LeMqYMr5Uu5Fckt10t1VzoN0l27lYPCZPsX5WSSiku7k2hoeEJZtQM6PTnnokZ0YdSgibvkyrFx7mqwlF572SgBE+8P4HWghlVTMytBEJTnPQqbTNyUnPgBEJZkixYtBEJTnPQqbTNyUnPgBEJZkixYtBEJTnPQqbTNyUnPgBEJZkixYtBEJTnPQqbTNyUnPgBEJZkixYtBEJTnPQqbZ1bl2RuSJ0RqeEJTnPghlJpy8y7FKJu7xYkqbk3zoS.XYKaY.PW5RW3TNkSoIW+ZZZ7Ue0Wwd1ydH6ry1q9CJnf3bO2yEqVs1jmFTnPgBEJZJPSZlpxpttN8t28l+3O9C+N+sbK2BO7C+vMGIAtsa61plt5ZW6J6bm6jfBpYqbMMoTPAEvl27laQ2HaTz5iTRIExHiLZoSFJTn3vjlslY2rYyrpUsJBIjP767SdxSt4JIvjlzj76XqVsxG9geXaFG4JTnPghiNoYc.v0291WRKsz7db25V2XvCdvMa5evCdvzst0M.WM69kbIWB8su8sYS+JTnPgBEMETqUI8jNoS5u8.GyyVenG43aS4oqqyIcRmzeK42PQWW2a5Ycqacd0ey8.ja4Ke4zgNzglUcpPQKASdxSlryN6V5jgBEs5ngrUcWqNyGyXFCycty8ua5wOra2NIkTRDe7wyO+y+biprqMDQ7VvhDRHAhN5nYSaZSX1r4pEdSMETPAje942rnKEJZoYcqacp9iWghFHiYLioAE+l84YtEKVXiabiLsoMslU85qi5S+zOc9pu5q75HupgqPghFWTSMTEJZZoEYjekTRIwUcUWUKgpAfm7IeRhKt3ZwzuBEskwiiaeKfrpvxJTT+4vovusHq.bhHXylsVBUCfWG4pZKnPQiOZZZUy4s5aMEJp+b3T325zYdSwGgGoTJ8iTRGJTzVG02ZJTzzRc5Lu47ivJK3fQylNUnPgh1ZzPpDlpUSZavQTazJUVvginRVJTn3nTZIczUa5ttRWMjJg0VqUSNZsvIJuldQ0Z.JTnveZIczUa5NPg4qSL+cnU+bt0z3DroytZMkdaqU3j5KJm4dQcqPgBEG4S8wIl+NzBryspJmlFmfME1Uq9rkndcUswqw9QjdvJu3BvPZ7KQmSmUPwEWLdJsXENJihJHezEpuEdUgBEGkPokTLkWA3asKE.6kTHkVbIM55SDcJqrRbaiJfwfRJoXb3Ti.Ui2hKtXJu7xCfbcRwEWL55FtOV745MP.JujBQZjqEstQ4d0Y8EQDJt3hn7xc3+4Ab5vAEWbw3zv23CfAEWbwXXTci3EWbw9MUIaK6PukyYt3u+yiq2chAbwOM20T6GgEYLLpycl7KknSQ4jNu4heH5aeNVFwHNANiK7F3s9ruFc2O3LJOe9l2YoLgg0K52PNNF7nl.K7k+PJ28C7x1wKSrw1WdhW5IYvcs8XKpHYVK5M4K+72ftmXmIxnigS9hlE+xeUJplZWghFeZs0rmGbGqmKdjoP3QXiHBKDl+q+s.PY4tGd8EcCDczQQ31hfzN+Yym+K6060k8e787z220SO6Q2oa8p2zst2c50vlD10EdzYdtXVapjiCWNRO3OsJzzzXQevVALXO+3+ka8BNEBKrHvVTwv+287rruBc.TNOwk0O52YLUl8L9mDQD1H5Xhlk+iYgta8tme4a37NdqXKRaX0pUF+U7f3P.DG7aewaynOldgMa1H3jNNdnW8iwttA51syw2mNy.t3mi6dp8CqQDEm5kcOryR0CXEa73D7qe04PW6Zpz69LX5Z2FLW4Me+7e29twoa64evhlIZZckW4otSBxrUBp2ihm4C+MzsWHa5yeGtvwNT54fGAC4DFGy8YeKJvttWcrme4a37FtUrYKRrZ0BG23uRJvgNFNJi29ouU5UGrfMa1nWG6XYUaY2nCT792DiI4NiMa1vbh8jqXNuANvfh12lXvQaCa1hBSozett66cvIs9dWrAgTKLu4MuZK3+9X35GCCCYn8ncBXVN6q91k27ItEIhAdUx9JuT4DGXWDHXYZ21iHq3MdNIlHrHZZZxi8EaWDCGx00kNIlAgScFxKt7+sbxVsHnYRrcEugXWLjB25y3td2iR9reZSxRm03E.wbHgJu1+46ju70mqDFHw2oGPJuoM25k7yOeIiLxnIS1qYMqoIQ1JZ6RxImbqRY2TvqOqKSHXqxR9z0Hevhe.YDW0KHhQEx3FyPDHHYduxWHa9G9BItHsJ15XWkCURERY4sGI43sHnElLsYOe4odhGQrFZHR2F5IHkqaHyYFSPP6Lk8a2kNxYCui.HyYYaPpnr7jglXjRPVrJu8WrN4aV4SHwBRW6yCIUHEKK3TQ.jQe0KT9ss7kxE2SMgfGqr8BEohc+gRXVBV.jY8vKUd+E+vRe6+3jCIh7mewhEKZlkdN7yT1319U4ZFXOEvh7FqdWht8Rjg0i1IPPxDu5aWd4G3FjPF1sIGP2+6EFU4dymrzYI.xsrnmVdna+xjD.gfBUtuU8KhHh7N24zE.QafmsrjU7RRJsOL4w9lbjG9zOQIHP33OO4Edm2RNiXiR.MIhI+zRwhHUj0+wa93Fd3kJu+KtPYz8u6RFEZWVy6u.APF+k7vxN11Fkyb3cWHnPkOcGGT9z4OAAPd107+jk7fWuDQTmfTfS6xm397uvp2n7z24zjNlz3D6UMybDNidzitAE+VVm49vv5Q6DKgmrj9AKSDCcYu+UgR9+9GKQBxPOsEINb+R1e8yuuzmNEsvIbGR1GZuRDgFjjb+NKoX2Qvv99j6YxcW.juYOE61YdmkOceU3pfCN+coSAgz063SEQDWuT2y1K1h5BkhzqoTWiKJm4JNRCky7J4sl6UIXND41elkK6Ze4IEVR4hyxKPFbpwHViXjxV9kcJ6bm+p7.WPmDrDqr08Wn7mq8ojf.4r++daQWDQLJQthnrIc6XOAobmFxblw3EMlX0bleGuxFkBy7ykX.Y.SaoxN24NkctisICa.IIwzwjjBrWjKm4GysJ+k6z2u9IySzzLKu8uWfr16YTBlBUd3O8W8l9KsrREQD4Ut8KW.j69E9LYm6bmxN9p+k.Hy34+HuNysDdJR5GrDQzqP18AKQDQD8p4BuR9zkNKANG4Oc3Jl+z68.BfLgq8NDQpzY9a80oKhnK6My8Kk59dmkv5m7G+UwhHhXXOa4ecU8QzzPd1+2gju6gNCASV8IenKUTQohggS4YuxiUfAI+mMsSYm6bmxa83yP.jaeI+fr1EblBfbU22+V1yAJTJM+7DCQj0tfyTzzzjYtv2TxI+hkxJH+Fg2LZdog5LuEei7VbuAmH.VNwaiNGWn.Pmh2FYsycPg.25RtdB1cGBDeeOKdnyu27OexMRlEjO5NcxXlwcQ3tifVHcjy5Rudt2ke8jUVEwwDN.QPGr4JqpYtCDQTvP6RmA.SAEDViJJzxw.slwtSYfCbfMYM4ypV0pZRjqh11DczQ2jH2BKrvlD41Tw4bq2GycKKl4esSlG6likEuxMxEbhlo37xixJ96YXCZ..fCGN.Bm8Wnc5g4PPidv7+WmGlvU+e6e6UqgP3UWYhPA69mHOf7V5Uv.dsf.MnBGNHrX6DNb3zU7ByFVceIw10imfDcDzIieeanEWRbgokpWQFpkPQ.907c0E.O30LAVnYSHFU..6J8C5R03wlaX.Phw4xFoIbYOtpVmD2MktFkitSffMwPlvz374NoPmNckacaDc3CtyHXhNkTGvQI6kRJr.Fx0uB5Z7gCXfVHsmy85mO2zhmD+uMsehYW+LDWhbgmXp.FfXhfBxJB4ye7i+D.blG+.vDUtyWVXl6mS3NecF5qOHV7cOE92KHBt0WccLuyIZNgY85zwGKYd5a6BYo2mMtm2XCbamUT0im9sdoEe.v46fRPKrfwjOe.XIrnHXM3C+fev6YMJLc9ge5fP7cDqVLiISlXSewaQE5tig3jcukMC.gFQv9nG76sSw6+AFRfd0sokst0sR94mei9OYlYlMq4CEscno38w7yOeRJojZoyZMHJ0YnL2UVNq3oW.o0kbYpWxMyt0igHhIJBy13IuhKixsaGQ2IUT5dXLcKVrW5gnB9cd6u42..MMS3xlhK6Jt9+uh8UfKmykTt6A4l.15z.HJfS+d9.Jrb6Xub63rBgr115I1vC1+Dm.ZZd1gG0HxHR.onCx59yC5WzzPn6Q1QPCdxOY6Tlc63nBCb5zf27tlj6znFZVCxkS.O1B0qfss0el7sW8wOjq5dnAZgfl68npCryeh2FPSyrq7n3JmZBsJsnpoQPlChs8gOO4UtNtb63jLW25P.hJAqXK73fhNHqaWGDvjOCFuHo6C8X.FMaYO4gc61oBGNPrmK2+Me5Tb9kw2u9ejudkKl3sELOzTFA67uJmhyuL1c56juZ4OMAQEb+W9ovgJ0UANj1nCBtVbm4f6AkfFP4d9.vEQ26+I8pSv+dlmAO5q8Urye4mnSc8XYge6ev+2CdELf1mDAEpU15pdLt365YXy6Xa7fWXLbtyYo.2JitOw5NCpE3Zcq46KrZUazb1V8gdiAp6MJZqxBt0KfIbCOCC6ebdLp1CZ4d.JivXfsOQJsnOgy9FdD9os8Kr7E+fz+n5N6HmRnyGy+fXsZhm3xOSVe54PA4rW1ltS73krmQ1AP6.rj29qn7b2M2zblGfIPSH7NNRRtygymcu+Ct8G6cXG6XSrnYNdR8XGIEUQUbp517nHBZXlwcy2CTdtb9CrybcO96v12xOvbl2iR4nwnOoiED35N8ylm6K2Hq+qeetoyZ.LmW6+5UF3vsHcK2+XsOIC7X5OC5xVV0FNvh6+Wj2mubMag+Z2aioM0KG.NliehUFmpTunPr1Nhuq8jh+kUx4Oi4vOtielG4xRjQdEOFN0lA2vYzUN4q617Oer00witf6gbKSmAexSBMVK+yycxrwsuS9xO3UX.VhkE95+DO2I2El4B+DRZjmMcp8wfgHTtih34N4tvc9xajtMpIhsvCkJzMnBcCWEups5ffq1ZC9ly9Len8rcRTm6KKN81eMtFcb+368TxjNogJlb8dhP78Vl70dGRdk4PDQjCtyUKS+hNKwrIMOSvLIsINMYG45pyoJdqOuXh9I+TQdzTQROS.YJO+VccntcYTCsmhsntTUelq3nVT8Ydkr7matRRtskXxbPxEbaKSJTLjB+y0IW6TFuDrY21hHdYXm5EJ6sfxEwnBYUK7Njg22D8ZGBvaelu6eZUx3N1T7d9S4TOQW866a9ihH5x5VxhjwO796M7X5PujIe82oTlyhjELdDF2BjBMbYa7.67SjfA489ihDQzkuekOqLjtXw60F+wb1RghHhQIxmdW2fzqDamno4x9Xm59wIux2scQ2dI9XysRxb8KUrXBoSIccUe.iYHxm9xyxsdbIu35ROjq81dY4Pk4Jxuk69LeOE4vskbCwvvPxOyeTtooewRjVCVb0zBHC6zmrrgLNjagqKe269rxf6bvdyGg09dI6sD6REEeHYVSahR6h1idCWFvILd48+tzkO89OOIxvC080zAYBW1sH4UVoxmd+mmDRvA497cQN2oe2RYU3dDW2JgFZelqIRMWEq4O+4ybm6baRJDgGDQpGkTRnfCcPLLqQTQDKlLax+R+IP4kV.EUhAgFSTXKXSt6am5WymW+RCMdTPAEP94mOImbxMIxdyadyL5QO5FcYWSzbe+6nAZtumlRJoPFYjQqNY2TgHUPAGJeHTaDcDg5WX1KsPJ0gAgGUTDRUdFoanSdGpHrXwfqLwj4G64wvOu9uAKlbEuBKLeLERzTEQ5khJpPz0MvVjQgYSUoeAqiWGxK+bQSBlHhzFAY1m.LbR9EVLlBJThzihqp7ber3n.RM0TQR6EHike99qRA9zkMalvz9M11gdW5jdADZjwiUKtppknU2VacTVwTPQUPnQGI1Bwb.iSA4kKBASX1rQH9Lptb5nTJpD6XI7HHrP7z8CFn6vNEVhcB1lUhHHKUFe6kPQk5fPiJJrZ5HhFgtAwXFyXXMqYM0632hO.3peFrzHp3RvuyT46hBnoQngGEgFtuWQM+1umP7XvrZoAQ7zAQJpEJnfBXVyZVjRJozRmTZSxS7DOA4kWdszIiiJQSKXhN9Dpd.BXIrHwRX9bJeJ3kYSlI9DhFLJgHzL4Mbv0XCJxHilJGXbU2FiMaQR.8b68v.Dl6SESTwFXSdlBx8faTPPb0a1ZUQNtOt372Cm8jtSl48b1t6CbezulmFZGrX0BwXscU1cadbjGvjmflaO8gXMBRvZkg3pDD32EGYLwhlH9OFmDgfBILhIjv7W3XBSgXkXBwpemUDgfrDNwXIb+Nm298uMHs3NyObQqJ+U0pIi2hIZPUGZ.Z390lp4v1UbEMWuv27Or3Z8wTm5TaVaEfilXYKaYpV8nkBA2ioFSUdHDPCBZA3jtbZZ3ptFtcjW4yQo5xsJRzW6V9Gm.l.BbPUqLCZ9IGwckV7cH5Yqc8mm3w6uuWR0TjuVSq19VumBKfOUVxqm9pj1zbmO8VnGWmRymyUeHPEhPSSCCDL4aN1i9ZiZU+HLm40bIVqVLqhQtpYvy6gA9kh.qAOCWNS0RbTnn4gV6NxaUm90.escTq4h.3nRSKDN9qYljT7cBylzpRC8Uejas4ru1CodHbWAWsmMtjYMcYBPp8Yjb8ytaXqF7bTq1jCXZym6E0xtlYc9dT.B1T.yIG41b60oOs5fp8HwSylzx7QX8WmsZMRfZjfq3nC78azV0N1annAPHbEO3C03HpFXHMEZycuYRuG94yiO7l.Uqn5sxQCzOQ0JlR.6C4lXNZy41e2GZJTzZiiZbjC36lXRaBT82XqBp0lYu7xKmBJnflqzxQMT06oGcYnSwQinrizVAkm8lK7rR2UeoVclOtwMN17l2bsJfIO4IyxW9xq2JTDgS5jNopMj627l2LCZPCpdKG.xN6rIhHhfHhHhZMdKaYKioN0oVqM02YdlmIezG8Q0pbpOwo9xvGtpspTbzAKe4KuNsinPgB+49u+6uAE+Z0YdsMJk83XzhEKGVil4.cMMT4jYlYRzQGMQEUsul6t5UuZ+jcfbpa1r4ZT+dhesEGEJTDXN5qfqFHRye2U17P0mcPJNxfC6mJsleQsgl1aMmWa6SMzujdVp98Y7HXDnMp4ZTFF0Q3JTTSXpMrMCki7iTQ8jQQqO7yQcM7Jr60cee2Hep5TUQPphLL75vWvDXHXXX3W3JTnPwQhnblqnUGBBFNsy+dQ2IS3LuHF+YdA7fK4Cwoaes1S+i4z6a+4zG2EvTlxj45u8GiM9G43mL1wZWAolRJjZpoRpi6FH2RcBXBSN1C2wozcdhUsd94OXgjZWSkgMiEfcm0RAGTnPghVXNBaQiQgh5F8xygm9llDy94VGQESTXRuT9zOdEjUwqfEMyyCmkte97crCrs+bHzfznfC9V7zu3SxZ21NHsNGN128WxjFyEvAhLdhMhfH2O6ew0svgxqLuoPP3fs9U6hzsesby+u+fnL6frV1iRFyalzq1W6CzREJTnnkBUUMTzJCCdzyexL6m6m34+nMRFYjAomd57dO0L34l8j3Bdpu18dIcbrx+2uP5omNo+aeKmWG2CitWoxOevbXV8+LI6gcdjQFY35m09X7F26z401YI.ZDFvN91n4+7yoSFo+q7kevGPOZmxQtBEJNxEUMyaigHBYmc1jYlY1jqqBKrvlbcTMzcxmjy9Ip1GGWvIO.hNTS.gy3tj4R6uqWl8+teNNOsTA.qgDAgGQ3DdOFAS+1uUdmK8gIur+CdghrCabMLqq8JAfRR+MAfrxNenqBhFbby9R3TSIFLHZNoSMol+7oaZNdNBtlSqGUsJsoPQaLTNyaiQadiwZffAlMo42VDoIMMLooU43U2aPtF.alC0lq3ETnzSfnOuygG5gtcLvDl3A3IMLgsDZGPlnIvv5Sbthue6pSssoM+6NJTzFFky71fzgNzglj8J8pRAET.4latMu0nSyLCNZa7ca+O4K27evocLISPNKmM+UKkbyuL52fOVLQtf.NzsittUJsfz48W5RPKzXH7X5.Gmsf40Vyl4OsGNCOknwjQE7qa3ao.acj3BwkZDMeVOBzZ4lasMGOGAHnfTlBTnn0LpufU72ll0ZzoYlG+ceSJYn8ly436GVC2BlLbRIkUNCeZOIu6i8OfctTP6PLg9jLgDjYpn7hnLG13c2wd4XZe37xYsA1YzCgw1qNS3QXEMQmRJsTdzsjOGSjPoUKe01enkHhnZlcEJZEixYdaLNZXSawrsdvyt48Q3S9xYSGpPDzHti8bY4OxLvZPlnHMAjfnWCbvXyrAnYlK75d.9m8Jb.HnHGDu159Ht5a6QnhJp.MSlYp29ixTFPTPEkxwN5QS7cJAu5qsdqr2xtSIpPghFCB3VfZc8Q8e9m+IqXEq.vUSstfEr..WKGqiXDinZwWDgRKsTd9m+4ohJp..VvBV.6e+6mbyMW5W+5Gomd57C+vO..m0YcVz2912ZbKTb8qe8r5UuZxO+7IzPCkPCMT.37Nuyit28tC.aaaaiO9i+X.3a+1u0aZbbiabLnAMHDQH8zS2a9vtc6ALeTQEUvy8bOGkVZo9EmXiMVt7K+xwrYy058plaN5vfrIrXsi7uV0m31QTU1unEMfn3oe6OkQ04vvPy8NTuOkyomG23Y0qd7U+88f6HyuJ6a.s0ui1b7NipV+JTzDibXPokVpzm9zGAWlGE.wjISxl1zlpwqwoSmxvG9v86Z7bc9dbpolpTTQEUq5+29seSBN3f865RHgDj7xKOuwonhJRZW6ZmewI7vCWNvANPCJeXXXHmxobJUKcOgILAwvvvabNRg7yOeYMqYMs4zU8BCQDQWJb6un.wIeSVEKhn6SXstH4jStMotTnPQiOMnNCTb2bbVsZkUspU4WIsiKt3nm8rm030X1rYV0pVk2ZQ6gpVS9m3IdhZbWPyir5V25V0z0hVzhH5ni16wQDQDbkW4U5WblvDl.IjPkMepm7gmZWqooUs7gllFuwa7FUKMcK2xs3M+2PqwgbTPSg2hfF.l.iJ.zwItO1aXJTnPQaSpSm4953wWmVImbx94XbjibjDVXgUsq22qocsqcb5m9o683gMrgwTlxT7dbDQDACaXCqFSKdjkISl3ttq6xqSXa1rwYe1mc0Rymy4bN90L3m7IexUSlImbxdcdKhvHFwHpV9HgDRfYLiY3MMLjgLDF0nFUMlNqKpZ2GnnwEa87hI6r2IinSU88Q0ZqtBEJZaRc5Lup05ziymfCNXtwa7FAbUq6+0+5eUuT3Lm4L8J2ku7ky4e9muWGt27MeyzwN1wpoq.wEbAW.W0UcU.v.Fv.7qlydRyCYHCga4VtEuowy+7O+pImPBIDl0rlk237TO0SEP8M8oOcrXwBhHrzktzFs9JW0OhMAXIRZW6aGVp5FqhntWqPgh1lTqil8S5jNo5b+6d3Ce3zt10NV5RWZ8VoidzilCcnCwq9puJ.LoIMI1wN1AhHLu4Mu5sbhLxHwrYyz6d2at268dCXbrXwBojRJLhQLBdxm7IqQYMpQMJrXwRMlOzzzHszRist0sxJW4J48du2qdmNqJ1samoO8o6cNDKpAGTiNZd+OeNm5dbKBolZpbYW1k0RmLTnnUEKaYKiLxHi5+ETacn97l27pyNceG6XGxt28taPcTed4km7zO8S6cfi4zoSY0qd0MHYHhH555xvG9vEmNcVqwaQKZQ0Yb1yd1irt0stZMN4me9xTm5Tavoy.ImLxHi+1xoljcy4.fipLv.U+z39SyEMkC.N0fqSghFNidzitAE++1yy7d0qd0fulniNZthq3J7VSIylMSZokVCVNlLYhO6y9r5rIuu1q8Zqy3zktzE+Zh+.QTQEEO8S+zM3zYaUhJpnT84eSHp6sJTnn9xe6k1JMMsCqluzhEK9c7gaePGYjQ1f0UMQ8IMDd3gWujUacTNZZ5Q0s.JTnn9xeam4GNF0aNcDTe0kx4TCC0HxWgBEJNxgFkZl2bbMGtTe0UMEOkip5FUMHUnPghVVZw1AIBjSRwmv7Dt3a.AJxdwHfQy0bKt1cHWag5wQkxktBEscn998ru1oD2qSA9YapFW6BB74kZ3uqOTSV3pKEU0qRBv49APE0B..f.PRDEDUfZthKU+r0950fHkxl+k+DPHuC8W7WGH2Z7dUt+0eQNG3uvP.DcNvAN.4lWgtEjqiyImbb+68yAx4PTl8JBnrJsvbImbN.U3rozZsAhgC9wMuSJyP767szzh4LOP0lSymvzbejuSwH+dYSqp27LUCKxWlnn7OD6JirP2aoEBrdq0z6Q.OrTnPg+TU+O0Wy3Z0YDc88tu1ozbufY5ckeD.LUCNA82zpmXn4yIpI6NhtcROy8fCmhe4GSUccRnFrm4uhp7O+qryhz28dqZvUFuZnE1p5Y8e8ZvekKFN4ymyoyndjOEcGkxoN7dRGSJI1vuePp58jxN3dnu8HURr2GC6uP6Tx91NcOkNywcZiG6FBkr2sS2StyzgNzAZe6aOcnCchNzwDHrj5NyX9Kgcs+B7QZFbmm4oSG6TJ7UadeALezXgtihXF+i9wk8jeiO49V9cVwlsTPCt4p0.D+uN+eYymjdcH5EbGWFcO0D46xrv5u98sjshvQBOrTnPg+3qIAgJc7Tm1aZ.8LjWIUCWiVUSD0G0UK5eeq+onqojDK9Gy1mnY3+0HTYEZBfr7s0M8v4bVik9129xty2t6vLp1032e68P+imq7qmy4edecKdgLtE9s7syeJngFlzBAc6kvYc5ilOKix7pCG4rNNgSbjbfBKkf.BVSCQKD+kmoP.MMhKwtyash2fW9UdYV5K9u3x5dB77y6p4XFPu4Q+3+zaziCPLCDRSY29YhfBMNdo4MGV4sdZrpeHilPc0vnYyCkll1gkC8Zu+X8+k4Zpg6ufIMMts4tD5emhzu3WW5VgBEsdv2OYav6WBU6DtrW4aADpcAHUOQTGZplZ95D56Yy7m+SwD5uuSU1JMUKd0SMa9VSS3SdtWfsTbkm6tugYxsbWOMsylk.2xnd5RQOKhUdiRUzcUNWkBvIu5GsBBNr6m9jTj9E+Clwuv4dLyf7c.nWNyYn+S1zNxx+Jq4qR74lSrcrCLoy+B4xlxTYpW9LYYe22yOrjqmJJ3u3dl7fYuE4rRAHfdSdinZv.mxUy0FjNKbY+miXZy1l0papoUN20se4LsoMMt0Yc8LqYc+70aIcugWQA+FW74clj1IlFi4Jtc91+zcynnWN274+O3Z+G2Au58ccbhmXZLlobm70+gqvyeu6fwckmLOvysbF23NER6DSi6eweFkZ350ir191YcqYajao53X2qloMlqf25K+LlzYdZbhi4jYIe9VA.QzQzcv6+RKhaXZSioMsowkeCWCWwBdar2bdiRgBE0OzKjO4kePRKszHszNYtiG6MY2E6x3tttCduW5Q3pm9Mwzt7Yyq7weuKCuB3XO+Wtvy37YS+wl4Zmx+fS7DSiE9Va.CNH20LmJokVZLum+8v0F1rAEriOfy3BuG9weYCL9Serj1XGGqI8hn7hSmydBmFmXZowBei05N9v1+hkxIOlEv9s6xqT9+92yXFyX3cWeF.NXt24j4FWz6v7tyYRZmXZbS22hIWcWliK6u1Ea3+tY1cANbKMC9gU8bLlwLZRKsSjy3LuF14gJCGEmC26Y3R2m3nGCK6+rIWNA0KfEO0QxkL6ajq35tbtvI++wmu6xXme+lXCeyuP4t6SYQuT9w2+U4Zm0svcbiWMuyW+yniam59Xy8Ul6UynOwz3jtr4xOlouMss+jy27X75e7V4I9lYhmICrFP3QbtbmSXfTRQ+a9+tmmgU9byl+09xl1c72.StiUt+d3xAd0cIY3tuoqr7Ggvvm1Sxa9PWFkURo7Aey1AL7V..yApaYaTwDXtKbcq7VYcK9t342bdMQ5oARsshxTeVA3pe3danTJRlPuPHHKRXgElDRPlDzPl26sYwPJUVTZZRvgDpDQDQHlzzjvrEkbfxzECGEJI0oXE.wRXQHQFkMQSCI1NlnX2ogr2s8eDqAgfVPhsHiThHLqBfbKq3mDQDYNyX7Bf7C6uTovs9LhIzDvjq3ZMDAMjkrk7Dwvorp4etBDjDpUqhlFhllIIxgb+RgMR2IDosyJ.mh1NzpYEfy2sxVCmxCLxAIfIwZ3QHQDQXhFHO+lyWzseHYwyZrBXRBMrvDqgDj.H29q+0hCcQJd6unnooIfYIBaQJgYIH2GiDZXQH1BOTQCjY7R6TDQj87cOp65KFjDYjQJVsDj6i0jHrEoDQXVD.4gVUlhHh7IKcVhFiW90RckTyYCqT.jaaYaPbYGzkMnPsFtDosHDMPh7xdEwgHRVeyCKfIYI+rqsz48swUHQ.hlVnRDQDtXwRvx79uoKexRmkfoPjvCObIXPnS8U9qRbHezbuAIrPcsEQawZXR3gGtrneLWYTCsGRnQDormhpPLzKWtog1O242vDqg5J82wK8wE65he1bsFdjRjQFg.H83XOAwty.s0O6PV5jPzLML42Ky0yIc6kHCsGsShLpKTJtfsKmmMqdWYCCM7IJa3P1kqL4NJVioiRNEXWJdu+hXyZvR2N1SPJ2ogeGW8G9UdO8ZWxGHhnK2+Id7BAYU9zMu2F3KU5d+q56NkrgHRIY+CR60Phd1qpIYavtgtBv0LUybSdJ3.lAF0c8wrq+72YaaZ8btm5fXd+yAwOj9gHj1OV9pMtM16d2CK7hFBNbpSNGrTuRIhXaGabGYxAOv93tN0NRAEVNGvtSzvUeZOvIdWrmbNHY8qeK.ry09gAL0XfvzelOmry4u322xmQmCGt2G9yo78+MLk4tJt4m9S32+ycwKcMi.IzX3a+vYhsl5aQJTzFkF0otnOhpj89C7j+3lI79NU9kckI6MyL4K+3UwojRHb+iarbUOw2y8t3Olc9G+I+51+Il2LOKVvEehLskrdWywEwB2+asNxJmbHis8tjpHvYMW90z2C6922L8tyQwqeiKjh81c0clG9c2HG3f4vO9eddBEniW5ivd1e17ma+GHdaV3cezkgtn6MMV0kgJ2CgNLg.c8R4+k9d3.6824X6cGoh28t4PkpiXxDd6Bwh2AWyYMIJNoylUt1MSVYsG9ks9iLoA1dhN1T4895+Goum8xO8R2.rub3WyyAmzsbe76e4CBzU93eKCRO8LXFGSzn4Ssd29aLG9W+3Oy3u0WjedWYve9aaka5Lim8+Z2Nu5FpreniH11wV9scyek8t4lNgHH8LODGvtS.C+et5LW9nU.wbrcmjBsxmSdiR38i+c5amdmT7XMx3Xc63UXXQCV8qxy0UCV666QFDr0Pvut.vm66MLpTFMj2TCJhDIxXA8WdU37Hf9k8u8x4ZCAMMWtciMp3nCsuSzg12It6q7evJ+7MStEowLe6ujBNz9X2+7FYKq6..ZfIMDw0C4PrDJozgXH3PzXLm3v399luEMuMkhAQ12ASTgFLR6RggfF108TPfpeidHoc7DVngPHINDhMFara8hPj3wAlI4t2EhM5nocIXwU4HMat922Y0SxJqJ6unFSicEVXCXP9oPgOjYlY1jHWmNc1jH27x3a4.NfErrGkTZeL.vXG+DQpnHdoctGhp8Iv0MkSinsXBzRfa3FuOdjW7SYm+6O.iSHUfnYBSbnDUn.8XrzudAC3zOWRtcQi3zJwlPrjUwk4Zf3B.8gwcFCBKACI02QP3VgwO1wSTgaEoicEqgYEQb.ZlcOMxpoQ8s6y2uimt09XHDwICJ5DX66MCb52zcBJY2+.ue1vM9tyi+YZtV5riJJW4UN6qmRJHW1Sl6f2Z86B.zEHrvsgdjgADLwEcBjPDtRKhWmkkwW7lOND9UwKuvKmDPCMIAdzk9d7QsKMVwmuSt7i0UyeGRngRxcHJBwjvoN1gwh90rbay0+Afrib2MuKPuRHJBwmrtmwQmnAgFWWY4K5VXI+0PXfcIJ.mthnlqg1uQMU2R28Et+FfMQt6+f.FDVHA66c0FWpAC+Z.lLGNVrFARQkdDwTWtYdHZ6Z5i4s7WBXMx1C.ZnwFV70RJojBW1SrV5dei10aBFUNpKE2+CfDSJkpO8NJy8zIIXKz6J+BLfTg6d2RSyLgFpUnXgf5TZ7tO0z35NqgQJozUl38tVhO1QQWi0ZSxKJGtKEtJTbzHRMzGnVBp5CtVMMMLax0Od930rYyno4Y.joATAhtG45xXhTgtq+zjFZA4ttNZFtFTXTlK+O9pGcAM.MylvTPlwiQGSngPV7Wt6+ducjKPk051y.30DIGbH9O6cpxf5MjfL4u4LA98u70nGojBmx4ceLhz5sa8V04cqA9Zkzk82Jfh.5aXDhmIArFfEKDEPw9TAWAMLDWxHwj6FZApxuZfISgTYVqVLocLm2sxSMiSwkcupjTMUSyV.zBfLyg631udLaIbN2QLL2o0ZmCKGtAJM4VPFRon6rzZbRQ2bSyZMy8rrG3vnBpP2ITQY7rK2USgGpo8wyb0OOc6pdR9wWXl7Q2zZgu.zMzAspulo6o.AUqfSBflTquS42bG0cqE.tZ7qt1+ACU7JDZjQyce6OBSYpmOgGbiydWtuzktzEuaApMlTPAEPt4laitbUz1mlh2GgC+8cgpRUK3aTcXfDoI3tuxEvkt14gsPLQI4W.gDkMFVjAy67mExWto+jSePIhoJJi09guBkWtShdvGKZhquQzDeZJXwyuD+m.6hIvjle1S73x1uqymqwrVv.agslQ9L73hle+29UW5qpYJO1qzbWn.sJ8hK.g2t9wPCCVvreR9G8eAL3tDM5NsidPgwF+l2m8WPZro+7iom6cIdSCFhmzeEbv7JGGAA4Wr3UclHbF3XNAp39dBVvacAbO+igfYoB9nEburQMSLmA2NeReFtbl5t7NhVfKTUPwzANGfMVdY0+Vwz2660hmVcCCJt3hPLDz0cRt6embem6IvJ9USjRuuQFPWiC+ZhdAbVZt7LO+axv9mWLiL0n8ScdvYo4S5Yd.5b26AgErqPcTZt7G+QNz9t0UhKb2CiO8xYW6JChoycmXByr20.ELzwoCCzB1xQDtya1m7zZ.e9ccZjRJoRJojBO0K+IL7YsbFce6BQqI7yu4b4TNuyio7La.m1KgwMhwwVKopRoxGbNAWu7BHhIuurUN9OIP7esD2u1GvcL.zLSdaYU.lHHyAQ6iMJLJnX+hzQBMmhBEJ.ac8T3xGZ+n3eZgz8TSgTRw0Oq3OryqtlUxkzk74bF9.IkTRgTSMENqY9nL3K+I4sd3y16RRkC2+Fb0D0FT8VLys+VLPvIF9rBvY3pu2Cfk7dOfimPMCW+I2aRIkT3zl5bcoC2g6ZT0aBC21qbP0ssnAnE+wwJ9vmkP20RYjcOURMUW1MelMrGrDQvfrZl7HSi9L1YCjGSZj8i+8OjCg2wARWHclX+ceeYxujWcKXlwLmOfiomchEN4Q5J7TSkyeAeDw0gyja4LFj+IDe8NKBF9LE17h4337lZuYOe6V4WCvT+wUTCbeh62bo2OzADxXyq26y1TRIE50wLRdkcBWv7WBaeqKfvL4JIVt6jiSMH6M8hLqaZlLowc23HfFscxac4Cfd129vS9C6w6YemG9NneGSe41Wv638bE8yuD8tW8gAdduFNwcKDKBUbncxAyC50LlBAeDf27lYm4txwVsZkfLYBygDAW1rVFe+icADjoNvM9suDcOFaj9V1JW4zFJIkXWvxnOM5PnloKIlLojThDjIM.SDTzwQJolLVLogEqgRxojLctuw3tzslIojSlDiOd.H136BIkTRXyRPDbXwQxImHIGgU2IIM5ThIRx8KV182sBt0Y8wDQTgPo4kM27Uc1zydjJ+6Mlk2j+Q.OyTnPA.lBgG6aVCu1CMChHzPHnfBlnS3eRWiJTr1tQxR1xdXRCpqDZvAQvVijwd0OBe2KbcDanlInfiljSNYr48CZM5XJoPpwYAOdu5PmRjjRJILaBBwRzjbxohUMSnoogofBhjRIYRLZ20JSykMpN1oNfFPRC4L4Eu6oRGiIbPKHVzS8PjbxISbsyJfFcNkjIkg2dBFPSyDsuCchTRNYrXxDVBuCjbxIS6B2BfFodRWEu2Kd2jXWhCylLi0PimdGcTbhm60Qu6YGozRxmoMyqgjSpKXwrI5aRgioDRiG+NuZhOxvHDKgwUdNCiN24jHkjSlPBxDAYMV91+6Wv7tzSFqgDLAGbn7+MumkMsqUQzVLCZl7Zy0j61+N3XimT5VRDhIW0T2+VJIHNsqYZDhyMwWrc2SUKMSz4jR108PMP7YfP69VNwlXWH4jSxUWhDhURJ4THotzILooQPVrQxozMRNoDIBa1vVjQxvGwv4hu14yZ2dV7F2yTI7f8LjBgj5XmI4TRlHCODRnOiiXB0DPz9svB4a58XO6KgjRryjbrV8d19z+tPhIlHI0yd58bViqGLfjRjK4xFNA6tfdZZZj019LxybDLiK5X4Hgky0loollGJQlXuPl3SrIwvvPz0cOEG7Fttnqq6cX9anqGPozPvPbMEJLj5XZGn6PN8iuOh036jry8kuTVYEK4r+eVtwQgPZOfTze6TRknlZZJNRiVMSMspfGaE5NcY6nZgKFtCKve8W8ypWumdRdhecj.kF7rVplhuaal9NMnLL7Iu41Nmu3zoy5bZSUS26ZnIOQpPthSoWRnQc0RI+MmoV+cmnW1K92kXCsGxqrwCT65I.4au2KzqbJ3oan628Q8h1gLgHCV9Gy+0EmM9yJMQjiXmZZfqRt3tzKtKUmo.LhGLYpx9gRyj+IOoVVD.DQB3TSPCWCfBM7skhBfbzLwfsEJ3vA6YO6ibxMeNvdxjcueHhPU0GWghiDwSsCMY1jKaGd9z1ak+zbGlOeC6ym+AvBjqAOsOalJU+uM7K90QBrFpYXsLUpp4A6ClLUYm7KhfllO4MM28lqus9s6A8G9z8.UEu26pGHhTKsNoItryXRTdAu.6JqhqwX82SG0OLoENqdGajKcnITqwSSq54au2KbeeUSS7VabO2A23a7D7IkGIW2jNU+GaisfzLN.3bcCph7g8Wtqdd.7zbM9GG.+lNBhHUqerpJtBqpCzF2BPy+i0PCCD2i3T71LYy+C+.508OONiiuuTgISnYXPri5J4OemajvE+RnJTn3HQzpxuCDRUsKToMFuhw2MSkp82UcjoW2NBqp7cIppZuxiUIOWTfxG9Lmnq5xupmzR0tF2UoI.2SBT5px.qt90BXd2yvM1DiZ1ykM29BY3y9Ynj241bqipa5Lv50k77rzeW2Kk2l76XQpzGQPg0QFXJ3cP6oUSiJu.MhDkp2epdy2hPEkkMW9c+h7feZ5L1dW6EVn4jlGm4duQFAu11yFSgEekgU0al91WLdKvoVMNe+pYc4i.pzis2S64SFe+vIjP6BS89WBmyMu.JsbmDjkHI9XBqdkEUnPQK.988d0crFHyFRUFY5PU5+2FzhJg+NTBjicopx2Wk3mtpxjbpFRC9Ofw704mGcGfLff6ool+oupWffZR+9TPAuh2irLgmlCPvDGyE+Dj93JxGcDf7P.cTW8BpTyOK7DWO4ISAttVZ9a5utotSWAasCr5eIWRHtnpGE5n4ilGm49jWiM91WuiaUOe85FW8nzW00KtQFc7DYsqkZWVJTnn4A+9zq5NRqWUFqAGgZh.WC8.KNsZKvFr972FTf7d5e7qVv0oMrpzpoAVAdkS6hqQZMyrNu+zDzSw0kN0zHg3hx8edjis+l0Qyds0m2Mm3+zT6HizjBEJTnPwgK0ZMyW3BWHO9i+30p.7sYZZpo4TW0WNr1ZWA1xV1RSPpQghiLI5nitkNInPQqB73SojRp1BrRsRs5LuzRKs1BF.RIkTHiLxnFCesqcsLnAMHhJpnpU4r10tVF8nGcsFmTSMURO8zq03DczQS94mesFmwLlwvZVyZp033YcpttVUrNoS5jX0qd00ZbTn3nYpM6CJTnnwgl8U.t+NbjTMx8vQhoIEJNRC02IJTzzRqJm4JTnn0IGIMPgTnnsHJm4JTnPgBEsxQ4LWgBEJTnnUNJm4JTnPgBEsxQ4LWgBEJTnnUN0Km40zHQswbDpVWahJMjqsgltNbxepQmqBEJTn3HEpWNyqoQhZCYDpVWN+p6MQkZVdUeCLngMxYObxepQmqBEJTn3HENral8FZMSarc9UWN3UnPgBEJNZgCam4GNNm8tu.2D4z8vMM44GEJTnPghVibXsqoYXXfCGN.b4Lr7xK2kvBJHBJHWhrhJp.cccb3vAkWd4XwhE.vhEKdc5pqqSEUTA.diG.gDRHd2f38MN9pqfCNXLa1r2ya2tcuoOOwwrYyDbvA687dRSFFFdiC.gFZnd+amNchSmN8JuZKMooo4mr7MNJTnPgBEMWbX4LunhJhwLlwPd4kG6cu6kd26dillFqXEqfgMrgA.e1m8YbcW20Q4kWNgDRHnooQLwDCe228cDVXt1iv2291GiYLiAcccra2NVrXgHiLRV8pWMwEWb.Pt4lKibjijJpnBu5xrYy7ge3GRe6aeAbUPfQNxQRt4lKEVXgz6d2a.3FtgafYO6Y6Mc+vO7CyK9huHYmc1diSRIkDe0W8Udc5uoMsIlzjlDNc5DvUABhN5nYsqcsdWe4yN6rYzidz3zoSuxJrvBiu9q+ZhOde1q1Un3nTTaMvJTz7RcVMx.MRwiJpnX5Se5r6cuaz00IyLyjCcnCQ25V27Fud0qdw9129H6rylcu6cSlYlISe5SmvBKLuxLwDSj3hKNxLyLImbxgcu6cyYcVmEwEWbdiSBIj.ibjijLyLSu5p3hK1uM.EKVrvXG6XIyLyDQDxLyLYO6YOL1wNV+R6CZPChLyLSra2t23bMWy0PvAGrW80st0MNzgNDYkUVr28tW18t2MW1kcYDUTQ4MNctyclANvA5mr5Se5i2BfzPQ0D+JZqgxQtBEGdbX2suxgI1saWhM1XE.APl3Dmnnqq6MbcccYricrdCuG8nGhc61ECCC+jyy7LOi23X0pUYu6cuUSW6ZW6RBIjP7FuW8UeUQDwOYURIkH8oO8wabdlm4Yplbb3vgbbG2w4MNyZVypZwwvvPl3Dmn23DarwJEVXgUKde228chYyl8FuMrgMT+u40DR94murl0rlV5jghVYjbxI2RmDTnPweCp2cvqTkRJDRHgvEewWLfq9udNyYN90ewlLYh4O+46sOzmxTlh2la2WF+3Gu2l39BuvKjN0oNUM804N2YZe6aO.LwINQtjK4R.7uz+gEVXL8oOc.ncsqcLiYLipkGBN3f4ge3GFMMMBN3f4ptpqpZwQSSi4Lm43MMcQWzEgMa1pVZpe8qeDYjQ5McOzgNz.caSgBEJTnnIGMopdo8gTSM0Zs59UTQEru8sOBN3f85Dtpr28tWb5zIctyc1qi8px9129nhJpfN0oN42.VyWxImbn7xKm129162.VyWb3vA6e+6mPBID5XG6XMlt28t2Mfql4ulZNv5SZZ+6e+3vgiZMNABMMM91u8aoycty06qo9RAET.adyatN2a3UnvWRIkTZx12wG0nFEYkUVMIxVgh1xzP9lrVG.bScpSk4N24VqBX6ae6XylM+5CaeY8qe8zt10NRM0TqQYX2tc9tu66pVebW03bAWvEv69tuaMNhwEQ3zNsSiUrhUPLwDSMJqq4ZtFl+7mOIjPB0XbxLyLonhJh92+9Wqooq65tNV7hWbMFm.QAET.4me9MnqQghVqjUVY0jUPAEJZqxXFyXZPw+vZzr6K0lyN.N9i+3qSY3Y.rUWwYkqbk05T+RSSi+y+4+3cJqUS7TO0SUmwolJbRUSSO2y8b0Y7TnPgBEJZJoU0jhttb.2XFm5KMlxRgBEJTn3vglskyUWWzgq196ifQKmxUnPgh1pHU420PTb4yvPMUbah3vtY1OrlGosfS8TsVWMBgBEJTz5.sp76ZJJZZ.ZnVBBZZnQwCWcWRqJqU7A2+dY+4UdsD2ZV10kdpwvMrSV6dOjagkVujSiYZRgBEJZKf.fnSVYkE4bf7cebcecE8W6g88WE4sl4JZZnQwYdcUK8JedWLW5v5B+yE+y0amf9Ja+0SUeovv+v87ll.1OvlHwjSh4rv2A.132uF9nO8Kvtt+NkqwlhW7WetziQ.RSJTn3nMpuN0pkq1uK2vmi71B1MnJMT83Jtkbf0RUCqJGKfH5nAnWZ1z+91Slv4detqscckVJiWXbIyob2K2ssxJc4b3cKSUXfZh+1ilc+Pv0CWO+1M91D2AGAzdKlCfSPCp2ksP.zLUkSYx+2q7ooezL4ZPpYhP.faZ1WE+3ub.90rN.IGsk.lN8CMbm17MMpZ1dEJNZGwyZP+gcY50b++UZawWKYdMiooUM6pANAAApcr0.Dw2l31j6yIUyIqmvcgAnYBML6t.EdRuAds9HPXoCBwFb0sW1ftk4U0J6t0DMt2Y7wAp+kjTP2otqeCU6kMCcczESArnZF55nqq6eXZFtkpuptpkzDDAbXnim2A8jld34e+73O8aPGhzh2yoooggtA55UVxupmbBzsKiZJxJTnnMMF0XKyIhfttN5Fh23pqqiQMTCaCcvH.Aoa3ZmdDvkMrJ0.555U2rSURN9Yizmzpt6MRJeOmWas9Qk17zEwaZTpSOwFnWgKcTQUBQDcLpRqhBPVa4q3hdp0624DDb5rBz84bJasAlFUm495.2yKI4susxI2wNPbwEKQGch74+tuW.7um+ERrwEGwESzbcOwGQET4ypL2vaPe6Z7DWbwwPO0Klk+C+o6l7QibyXi7fydZL4Iegb5SbZro8Wh2FE.f7+iUyodb8jDhMNZeOOYPprzsu+hdbt269Uo.6FbnM8FbBQmFO6JdSFTOSj36Xh7rewu5JO.X3zNu3CLaF2nFEmvIbBbhiKMF0LWJkI39qO22BUs1tBEGUg31ilmpQHRw7PWPZL6W3S44uuqj3hKN52vNIJygNu48eoDWbwQLGyox5ytxwLTAYsEtzgGMwFWrzgN1I13tOn6PzYyuyCShwl.wGe7bw2wRHWbYlwQIGfStScj3hKdhY3iiUn5+Tt...GfjDQAQk7waCAX8K+gHtnmAGvtqzSta8SIlnilE+k6..N4iqub5y6S3Uu2Sg3RHdtfa4ovyRW019zmfD6XbDWbwwEMmWjbA2CAcH+c80bom9fHtXiiD5RenfhJCsZxat.B4wkbrCj3hONhN5Nxc84dBzfO94mCwDSbDabwR2mx8PoU3p.Qo+eeB50vm.u8scJLxwkFi55dYxqfr3x6RmH93Sf3hIZV924xtrxVaMPssvsOu4Mu+VK76ElwWJmU2QfNHGyvFtbrCp2RXfLwGeSh3rD4kukIHPXROG7wI8u6IJXND4Y9xsIhXHe5ydSBfDSGRRFxPFjzwnBRR5zeIQWDY+63KjXsEpXNnnjALv9IcLtHDMMM4AeiuTLDQxMieTz.I7nSPNtQLRYP8saBfL863MEQDYTCsGhUaQI6onJj87cOp.HlLEoLrieDR+RscBf7z+PNhgiBjEe0iUfXjt2ytKlLqIDRXxwegKSJ12LpQ0y60F4me9RFYjweq6s0lrUazJJZnzTtQqzVcSbopaZThTrL6SvkKvN1s9KCen8Sr4YCapCcUFwHFpDa3gHZblR5E6TD6GTN6Q1Cwj4DjgdbGmzytDqP2OKY8+UIRI658DzLKcquGiLv91MwDHIO8WSLDc4qdxKR.j9crGuzyT5fLjQMSwoHxmrzYIv3k8Z2UpIm0+NBfbaKy0l.0P6Q7RPlGfzidkhz+dlrXJhNKuwtJwssVSRW62PjienGqDbPljNLtaUJW2PJ6PoKgGZHRvgFlLzgOBYnCtehIMMYXi5NB38jxy82kqZPHPbR+FxvkgMj9KwhI4Dl4RD6EmkzinQ5R26izmt1IAPdfOZyhiRxUFynFrDmUDhMIoeCreR+O6EKq7kmkP3cP56.GjzNPnOSPx2YS2yyizXzidzMn32z4LWuX4Bixl.cV9trK08IKSlXub4LOmMrRAPlxs7xtCyo77SRSLMnYI4WdwxPRMZwR31jccvxDCQWLjhkbNXwhTzOKmJHvYK+3Ac8js77xThL7PEZWukrKwg7xy5TD.4C17AbI4CtI2NyeCQDQRaX8TBMhH8yY9C8M6SDQj72+lkXrfj3jeUoz+3cDHX4E9tcKhHx5l6HEhtGRVEa2Sl7v5VSSoy77xKOkybEMHLLLZxclWcGesMvvGa.FFEI2xIfzuY7phSWAJu5sbJBzGY6GxoHFh7l20UJZZZxKut8Ia6YtHARQ9u+kq6MNJ4fReZulD8YsDo3e+8jqXdelXHhXTd9RRcLZoyIekhcCQd6YMbAPd4u5OEQDo37yUDwkybMFureONycai8Nd4MJhHxP6Q6jPrZS149KVDCc421atht8Rjg1i1IsOwKPJS2UZ9ct9jEBMV4myNe4ytuyVzzLKO7mktXXXHFkteIJaVqQm4uwcNcQCjk94+t6yXW9WiGYjy7ED6Ems7Ze75EW1MKStmgibp2ym3NdkK2xIfL9G868JqM7ounjSINDQDorM7PBzKYSE8264UqIZnNyabG.btpqOfFhyJPDCRYPIywFWntGwmUfm1HIq+Xinoow68p2IC8+9zfYm7SaPfX2KYW1+e6cmGeTUc1.G+2clLyclI6YH.ID1Q1TfJhQzfBHtfRspTZE23UjVoshBpPqU3Ugph6UJE0hfKuhZCpHBVEDEAUT1sBnHZjEEBKASHgrMyjYlm2+XlLIgLIrlHAe994ChYtm64dtStbdN2y4bO27ojBKDyK5YnMtcDdOhkl6FJ8qWOuuALw29A3rbGZhsYlTaXQ28fHq+2UQNGnDJ3fkggw8w.5Un0d8.ABMpMFQJg0dPWRJQW.fCmMGSmNvvNDSBoRGAd+2YYzuTyj+uUuavp8ps2FGQyIknYZSaZjTRIcLrm0OOd7vfG7fOgmupScU4PhMkoLkFj7unhJ5Txm5CQDLp1Dwsxyw11tNfU.L.2c6L.hC2waELf11gjPDvg3kUs3WECCCtgAkIoYSHfEu704ID+V1JV53n3ouq73wenovV+lul7xuTRI76NpycTSDym5pYjCoW7di32wc7mmHYlXkkhPuvmpZNxaALBWikA3nSigV2hXAC3zROY7V5OvAKX+r+CNe56Y2GrZH7kaZ2XDvE4WtW9wCTFhLdF4E2NLLf.AC0s300vVGr7RAfKLy1FttwPiAuErh8XaAWYeJg4MqIy679qkOdUPGFbkWWDZ70ohpFy9y9RGIe0mtDl967Ar5k8tgxec7xqSM.AyCOKMrFCVLrD5GqbxdZDZTsCZ.IkZqQDgQ+GeEd76c.gRf+JfXrg2R2AhXAC+Q4QrPp.Dvp+vSBNIT15J1PuNR8K1Ik3sfzembnKzpRkgcEnxw5txg9IXkSjMCaXwhUvGDSpW.yI66jya3+O7FSMzl644d6zRW1ibtZTsIW2QSEViabi6HZ8eWoZLzP9hP4EdgWnAKu+oTT+26hELBV8l6CF3Kxj4wzoMB8C1oscqGHucJr9MrbZYj8H.fUjh2BWcBciExowC+5OGor32CjPA5Z0Yb4Txt+btiQdCLim8IY0qc070qcEgqcKGJr7.zR6VQrdHOW2BXbFcD6UqXa0Z73vkKROtGf0u9wEpNyv0CCvRRx.5mChwnpLQj59VXr5JNDffFDotwvGZB3qLFP+NO13t8wBW5hHwMtX9p5I57lemYyYdE+Axn6Ck2d5Cmy3hxFKm50lvSXZXlm+FfgESFvkzY1wWuYlvK8tr0b2ME7i4Q4d.KAEZQG5NMGXtu1egrW5my912tX8q7C31um4fO6oRxo0RJYI2AS6UVD6de6mubcKkEt7uk3Z6ExMclveaL+It2G8+P9G3G4SW7+le6e8MHgVkFc0sIspK8AV9cyC+LuJ4lWdj+OdfpJaQt1I7yItT0FDQBewhQjKUiwqWftw3l7ivh+n0yx9v+djKrEQhz1kSAuwCkRcTRLhV+9EMVnmW7PIV9Htlq+N4q1VtrucucdkmXp7m9auG6bkKlEfKl65WA20P5AVBOweKtzh4stu6fOYmt39d12j3i0AGnXu3OPPhyUJ.alks9sRfxKhW+MdypJWUN4jKNxStN.XMFmjQxMi71+8xrm+GGpt1u3yXZ2ycyNKrbRqGmOVVwTXrS5w4G169H+BJDInT8JNqgN141hAvecFyguYm4RA4mGGH7rryWQeI6Nm735dr2mKsWYf32.eU3kxJuDDBP.eF7UKcEjy9xiOYguAKXouK94x4s9z4QxU3GPn7xJEeUbny3dEPC3DfKnHhraYLYU4bhL7eLBOA3DQ11qMNIbOUK.hgggXo8iQxOXPYiu98Wy8Cj1bMgl.bErskJIXCwBFBXIx1usY9IhHh3svcVi7rxiwsTsI.WkiY9OrhGKzDdaC4KhHhuCteI8ThUZ8veIIfHxZlwuR.D2o0J4wl4qK4784W0Dd6XbX.aHGybk5jMmpNA3psRjwkExk+PeRj4Hvheg6PL3xkcEdbrWy+9dDvh7uW4dDQD41+MmSspmKgrdZYuqalg+4tH8saUsstd6ulb+8slous85bkx7EP14+8sDGVqV8og+6Jm.bYdZMWR3WNKohCoTm6G9OqQ5ADC6tkuH2CJ98TjjP7NpUYrO0wXlKRQxz9UTqzetiY1R4EtIo0fXygoXygYjs0x1bIhmfh7ut09E9yBUm9MO1gIFfjTbsqF0m+zKaWM.+t6jOmDLl4gY.PZ7DKo.tfW80Yy4tmHa5LxJc.n8+lmjOuEmCy689B7KVIVWsgq8luFRwvfTF1jX+a8xY1uz73.dBhMalbNCo+XHBI29AxlVyxYAy+8IWe9vpYJbU2vn3r6TnwH2dhYPEEmKyctuIe2txOxwsOmeu.fadzikAu+hIIGVwnC8mIO4IS+yHd.vhcmbW+k6AK87Wva9WuEt6G4c3L6+kQWSyIO+TtVlvXhkQ8beHy7F5MV06FWoTQXiKezSlyqSsKRWveZ+hKi6axmGIFdL+xnWChIOY6zq1Dp9lGYVymrF7L4KxwCAsZit26AxkcIW.MyUErh2xf+yp1NwGe23JkcvAJobtnquuz6wuAjm+MnTO9vlYm42by+ZbZyBspWCgOdguLu4m7kXIFGbYC97Yoevmv4e1sF.9c29DH+zxrVC+X5CbLrmu8LYlu5+gx8.1LSmycPW.8Hs3whAj+t2Fu+hVDqYy6Lx9jVaFXc7cPBL1E3mNm8qxZ9lsE9yDZee6MNRna7LybZrhsuW5XexhlU5ZX0aIHM+BFJ1MBxMd+uB628L4fdDb47Wv0Mxyh1lXWvSEN4BuvdvxV1pvhUGLjd0rSP+95TKFhT2qSfSYJSg669tuFghQvZuBt8SLweozo10N1WENX66XGjpSqDTxm6HqTY5w7nTzGOdRffgWUkN5J4EUTQTXgEpiYt5mEZW6ZWC5Xx2T1gaBzJGxbw4P+4SXkCQvvHzDl6XcR8dLerajOdMULfAL.V9xW9Qb5a3ty7iJ0Sf7JmfYzH7K7pcPLhwA24.bvXeseje4fFHoztLnrsuE95MHbE2auI1JK25UgJk5XjwgTw1gFr9PCbGs.6QK.eUY6Q1xjsQ32nYPCS8r0WiPNrqPsMPMf4TM+jEL+HN3rQM9qFV03fXka8k2.o2yGfa4AeN1xWsA.C5xvtel2DFPs5pJkRoNpcHUrczDzpxzFs8opOo9Cj2XEn7H4XDsxRPjvqa8ZMtGN+jELuoQ6rRgq9O+24WNtGJzZprQLXZOlvKYgMUNGTJUSQGKAZOZ2mSlti2nUVrfAXnAxORbRR2rejq95ZoFpikM6l07yQB8vqoQzUJUCjn+brS8VmSM2mi+JnZXGidiC6moNx0j68IW800RMTGq57y0q6TJUioip5bN9qfpgpd1nNz.Zf7iK06cl6wiGJpnhZrJK+rg9cp5mazq4UpiN090Qa8qdezz99u+6OrYP+5W+XEqXE041W0pVEcu6cmDRHg5MeV0pVE8su8Mpaqxh34e9me8dr.nm8rmrwMtw5MMCe3CmryN65MM6ZW6B.xHiLp2xz0dsW6gMuhlV0pVQLwzjaTNTpCqCs6R20t10QcESJ0OmUY7k10t1cDuO0azjijmCZqVsVuoaG6XGzl1zFRLwDqyzTY5NbGuC2wBB0UMGtzXZZdD+LdehLuhFcbhTmpoxqmq7Z6V0pVoWiqTMvZxMl4mpQqjScplJuqhFy42hR8ycZvbkRcBkF7VoZ7oAyUJkRoZhSClqTJkR0DmFLWoTJkpINMXtRoTJUSbZvbkRoTpl3ZzBlWOqMMmv1mizze7LaaOVNOTJkRoZH0nELu9BfVWAHOz2cuGOGiijiWCQYRoTJkpg1Qz5I5gtJkUd4ky9129..+98yN1wN.fjSNYRHgDvvvfCbfCPQEUD6cu6ke3G9AhO93AfzSOcra2N.TbwES94mOFFFr28t2H4SpolJwFarQNV4kWd.gVqZ+9u+6QDgTRIkHKQrABDfbyMWBFLHACFLR9DarwRpolZjxed4kGkUVY3wimHowhEKjQFYfEKgZWSgEVHEVXgjat4F4bGfVzhVfSmNAfRJoDxO+7A.ud8F0xsRoTJUiF4XPYkUlzl1zFwzzT.DSSSwoSmx5W+5ijlErfEHlllhc61ESSSwzzTZSaZi3wimHoYO6YORrwFq3vgiHoK8zSWN3AOXjzje94KIlXhhCGNDCCCwgCGhKWtjMu4MGIMd85UZcqacMJOlllxi7HORMJ2SbhSTLMMECCiHooG8nGhe+9ijl0st0INb3Hx1c3vgz5V2ZozRKMRZ16d2qjPBIHlllhEKVDSSSwsa20nbqTJkR0X4Xpa1c5zIyYNygJpnBfP2cpKWtnyctyU1.AxLyLwhEK3ymO750KACFj4Lm4foYUuavaYKaIWzEcQ3wiG74yG974iG7AevH2EO.ojRJbO2y8fGOdPDAOd7PG6XGoicriQRic614AevGDud8hggAd85kDSLQtga3FpQ4dnCcn30qWDQhTleoW5kvpUqQRSW5RWHt3hCud8hWudwmOeLyYNSb4xUjzzhVzBF8nGcjyKud8xHFwHpQ4VoTJkpQywZq.pnhJjVzhVH.BfLhQLhZklgNzgJ.hgggzqd0KohJpHx1BFLnHhHyYNyIRdDarwJETPARvfAircQBcmvNb3HR5V9xWdsxGQDIyLyLRZV7hWbTK2W5kdoQJSO5i9nQMMiXDiHR9zhVzBo7xKuVkoMrgMHwDSLQRWN4jyQ32bJkRoTmXcLGLWDQl0rlkXXXHtc6V1912ds19V25VkjSNYwvvPl6bmqHRMC9JRntQO0TSULLLjYLiYD0iiOe9jd1ydJ.xjlzjpyxyq8ZulXXXHcpScpNSSN4jiXylMwoSmxN24NiZZ1912t31sawvvPl0rlUTSSokVpz912dAPdhm3IpyimRoTJUCMqSdxSdxGq2Ueu6cuYKaYKLrgMLFxPFRs1dxImLETPAzgNzAlzjlTjOu5SlNmNcha2twue+LsoMsndbrXwBojRJr3EuXxN6rqQWdKUax441sadpm5oX7ie7jUVYE07JkTRgW4UdEF1vFFCe3COpoIojRBCCCRIkTXpScpQMMwDSL31saV4JWIyadyKpoQoTJkpwfgHGeOeU6d26FGNbPJojRT291111HwDSD2tcWmu6tCDH.4jSNz0t10573DHP.l9zmNiabiqdeDzd3G9gYricrQl44QyK9huHCaXCi3hKtZ85ZrRETPA3wiGRO8zq2xzK9huHiZTipNSiRoTJUCsi6f4mrotZvfRoTJ0opNhdNyqOA86k47DODexWrQ91cseDLnS8rObtW40xu+RNapcXUAQLnx3sBDkzTep+8n9BjGvSQr9080bFYlINsaAiPkFLvHJMBPPDBWNiVdFjEM0QxRKe.732+HqpjczeBoTJkRcb43ZEfSBTF+ei+x3ltmGhE7Qqkqb3ijq4J6GK7k+WL5KMSd7O3aIXs1KCLLp5SCFHP8bDp8dWYjxJ6NAwuO7WYxNL8wv9+74RV8OKVw2s+HwaMB++U6FAXfgQkSV8nUND1+Wt.9nk+c0rjYTWkakRoTpFFGyAyEDlyUdwby+i0vzm2mxV29N3tt0alwL9oxN+9b3Qlvv3ubICfm9ixk.97RE9pLncP74ye3vc+HiYnil8FH.9EAB3Ged8gWe9BOV1UU774s5A88iHBAqnLt392GdiuofPaux61OxHGDf8uu7nfhJE.RsOWGq9y9L5emSsZkEeQ9iHAvmGe.93G229nhfg9JRDCB32G6Ye4QQk3MpsYHfuxXe6aeTpGeGOespTJkRcT6XNpigDj2diaiTRqE7+bYmIwaV0cL6JoVws7Gl.trmGYOukwnadpbdC5dIf.4s12BSSSxdkalIe9ox+ZgOGoESLj4s+7rxm7JwgCSbXZhkdMH9muyZwePge7aWBo3Xvr4PwjYIOvvXvOvhY4u7DYoe1l355tab57p36JObYK7cY+B25MPySuUzb2IvPl3hwSAeImUe6KaYukg2CtWtwN2dLMMwzgItRzMu4LlHltRhgjYyHi11Vr2sQvJ2SE3aueACu2lz1LRiz6voyl1eYU6KBffEv0OndSFst0jnqyf2aGd3v1MAJkRoTmfbreKjRErl.9vgsKBG1shAV.opgKNgV1Zhwtc780aKzXOCg+OgBx4uD+Lv6Y1z5VlIKb4KmW7NtbvvIc5JFKevxVJO6v5D29UzWl1hVEPfPcGd06I7J7Phsb.3xgMl7KsH9vk83zJyp086b.dyEmMYc2uNq7iVDmcyrBFgVo2rRo7B23oSoW5MwG9AKgt0gzv0kLA5dKcA1rvkN92kM84eJsL+2lq4OLaxYAOBa47dBV0ZVM20upcLva4UozHmNBEuoWl4thX30W0FXQuw+KVHH5.mqTJkpwxwQ+AakXLrPffEPvJGh3pE+xSgERv.Av7rOMrXHDDypRjAXK9lS+GXeHojRkA1+9SO6PZXEqz711CFz.tP9826LX1izJKeEgFSZAGHUannEbPO6WVDiEC514ddz+92EbVs38FjLsqG8hOcpWMSeMd3OeaCjXLpLTuEJKvA4Bt3qfr5WVzml0Lr1ttfMaVvYbIxvurygSqa8jearwQfu3yoC2zywWL8+HIa5mCt+kRA+mboBH7riyf3Za+nql4vUeVcmClVlbgsKzyA+oXOn.JkRoNI0wdvbK13w+0ck8u2Exyrj+KPvp0yxEwLu+aiRJyGWyP5CkCHrefH2bbDFRUeP.BTs1CXPBtNS7EL.hePXO3Kb9aMlp5JcQDrZwZn.mGxMC+XuzhYAO+SvZezqhS+rtaxuhJSfICchyf665GL8nm8jrWaJL8QcQXMTFhDt0IUd5T7VeWNqSOS9GKIGRoSCCiyKdrfAACF9YTOodyx1zp3wGyujgcgmK25qEpAH5iHmRoTpFCGWyTqq9AmE+Fm13Nu7dyULgYvN1cd7C63K4FiKMtqmcYbN+k2k+XVskyoWV4.EsVJ5fkvJ+3EUUTxJ.eeyVYs6nH1zZ+uTHV3fkT.EUbIrg4+jbMO0Z4BN61hYxMCSy0wms9cPw65y4eO+ERUyxbgOe0alc80qiMkawUqzUBydlykrF5umQO7qkcrw+Aas.Og1jgEbEivAKoWLo+0Kv9KdIbi8zcT9xITv3h25Wwds3h68l+0ziVGG3Rnhx7D4wqq3MMad4Oq.98S8U456SL7bO2Rob8lxUJkR0H43aZWGWW4Uyeer3m+uQQyerz9LZAss88fbupQyRV2V3Se3KCqXmQk8Boul6gLRukLy2eC.fcaVf3OctogmFWX6Sgd9mdRJIfvFew+LI2rlSu+0OEi8u+1L9qp+Dey6ASXnmG21E1ANme2+.ulBNcZEawkDOvHu.l50kIs4zyjO5GpdvbSxeEYSyRxMSXZeL2+arZNql6..LpHOl2yNQfOlwLzqlt2kNwUe2uBUXyDCCiHuaysgfAISKOiKfd3Yczg1lNO2R1IFK8uxU9vyO7Lx2ANZ4oy7d3ahl41MeTo+BVS1iDG5DfSoTJUijSbq.bhPYkUFUDH.IlPB0dyACPQd7SRtrSM6Ob+TZokiSGwwpm10vss8AwJmwnHfOK3vdMaqgmxJFSmwS068ZAvaYkigKmXxgJHdKsLDa1wgc6QVXX197mDc3293LkmNatnt2Ldlw96H6Mua9ue2d3LRO1nd5ELXETQ.aX2Z.NX49IwXsQ0aKTv.9obOdwtcmXyl9nooTJkpwyw8J.WDFF3J1nGHD.CKVIIWVixVhgXiMz6AbKB3vHFrQLXydsSoCW098EtAfCW005vtELiMtpUDC0J.q1cBABvOr4ui8z7Jveffgxn5YLtsXwFlV.vJIFasOOrXMFhM1SbecpTJkRcj5jpnOhCeHNiRT7SvxXH2Fy+I1M21eeZrnWSvh0X3Ner4SWSy0gemUJkRoNIyIUunUjf9IfXgXr13zM0AB3mfACEL2pknbW455rtRoTpl.NoJXdiIMNsRoTpSUbx2L05mjlVTyWLJ+rr0MJkRoZx5juf4MF2tbMVeYBxg90fdG6JkRoZJ4juf4MFpQz5ed9UfRoTpScnQxTJkRoZhSClqTJkR0DmFLWoTJkpINMXtRoTJUSbZvbkRoTpl39+EYFM698ap9k.....jTQNQjqBAlf" ],
					"embed" : 1,
					"id" : "obj-5",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 88.0, 120.0, 499.0, 275.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 88.0, 648.0, 20.0 ],
					"text" : "The library of Max/MSP objects which compose Spat is divided in two main categories: DSP objects and control objects."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-1",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 72.0, 221.0, 20.0 ],
					"text" : "General description of Spat modules"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 24.0,
					"frgb" : 0.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 32.0, 32.0, 196.0, 33.0 ],
					"text" : "How Spat works",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 202.0, 113.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 72.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 48.0, 142.0, 16.0 ],
									"text" : "loadunique ForumMaxApps"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 16.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 608.0, 64.0, 64.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Overview"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bgovercolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoveroncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hint" : "",
					"id" : "obj-169",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 608.0, 40.0, 69.0, 17.0 ],
					"prototypename" : "M4L.toggle",
					"rounded" : 16.0,
					"text" : "Overview",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"texton" : "Overview",
					"textoncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-48",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 736.0, 32.0, 299.0, 22.0 ],
					"text" : "Typical implementation of Spat in Max/MSP",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 0.301961, 0.337255, 0.403922, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.301961, 0.337255, 0.403922, 0.662745 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"id" : "obj-2",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 16.0, 16.0, 1224.0, 664.0 ],
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 5 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 7 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 6 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-76", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-76", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-76", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-76", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-39" : [ "Gain5-6", "5-6", 0 ],
			"obj-38" : [ "Gain3-4", "3-4", 0 ],
			"obj-76" : [ "Gain1-2", "1-2", 0 ],
			"obj-7::obj-82" : [ "Gain", "Gain", 0 ],
			"obj-40" : [ "Gain7-8", "7-8", 0 ],
			"obj-7::obj-39" : [ "Loop", "Loop", 0 ],
			"obj-7::obj-50" : [ "Play", "Play", 1 ],
			"obj-7::obj-65" : [ "Transp", "Transp", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "FMA.SoundPlayer~.maxpat",
				"bootpath" : "/Users/Manuel/Documents/Max/Packages/ForumMaxApps/misc/Utilities",
				"patcherrelativepath" : "../Utilities",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "spat.spat~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat.oper.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
