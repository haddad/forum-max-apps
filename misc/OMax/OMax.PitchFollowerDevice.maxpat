{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 5,
			"minor" : 1,
			"revision" : 9
		}
,
		"rect" : [ 23.0, 67.0, 464.0, 586.0 ],
		"bglocked" : 1,
		"defrect" : [ 23.0, 67.0, 464.0, 586.0 ],
		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
		"openinpresentation" : 1,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 8.0, 8.0 ],
		"gridsnaponopen" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"boxes" : [ 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Dumpout",
					"patching_rect" : [ 368.0, 504.0, 54.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-1",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "outlet",
					"patching_rect" : [ 344.0, 504.0, 18.0, 18.0 ],
					"numinlets" : 1,
					"id" : "obj-3",
					"numoutlets" : 0,
					"comment" : "Dumpout"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Raw pitch",
					"patching_rect" : [ 240.0, 368.0, 58.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-60",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "outlet",
					"patching_rect" : [ 216.0, 368.0, 18.0, 18.0 ],
					"numinlets" : 1,
					"id" : "obj-54",
					"numoutlets" : 0,
					"comment" : "Raw pitch (float Hz)"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Raw pitch (Hz):",
					"frgb" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"patching_rect" : [ 248.0, 344.0, 83.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-51",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 112.0, 83.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Activity",
					"patching_rect" : [ 136.0, 368.0, 47.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-50",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"outlettype" : [ "float", "bang" ],
					"tricolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triscale" : 0.75,
					"patching_rect" : [ 216.0, 344.0, 41.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triangle" : 0,
					"numinlets" : 1,
					"ignoreclick" : 1,
					"hbgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"id" : "obj-46",
					"fontname" : "Arial Bold",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 112.0, 41.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "outlet",
					"patching_rect" : [ 115.0, 368.0, 18.0, 18.0 ],
					"numinlets" : 1,
					"id" : "obj-25",
					"numoutlets" : 0,
					"comment" : "Activity (int 0/1)"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Velocity:",
					"frgb" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"patching_rect" : [ 152.0, 536.0, 52.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-20",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 144.0, 52.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Pitch:",
					"frgb" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"patching_rect" : [ 112.0, 536.0, 38.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-17",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 128.0, 38.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "$1 $2",
					"outlettype" : [ "" ],
					"patching_rect" : [ 64.0, 344.0, 36.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-16",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "number",
					"outlettype" : [ "int", "bang" ],
					"tricolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triscale" : 0.75,
					"patching_rect" : [ 152.0, 520.0, 41.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triangle" : 0,
					"numinlets" : 1,
					"ignoreclick" : 1,
					"hbgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"id" : "obj-13",
					"fontname" : "Arial Bold",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 144.0, 24.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "MIDI note (list: pitch vel)",
					"linecount" : 2,
					"patching_rect" : [ 36.0, 525.0, 76.0, 29.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-81",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "outlet",
					"patching_rect" : [ 64.0, 504.0, 18.0, 18.0 ],
					"numinlets" : 1,
					"id" : "obj-82",
					"numoutlets" : 0,
					"comment" : "MIDI note (list: pitch vel)"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "stripnote",
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 112.0, 400.0, 59.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-79",
					"fontname" : "Arial Bold",
					"numoutlets" : 2
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "number",
					"outlettype" : [ "int", "bang" ],
					"tricolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triscale" : 0.75,
					"patching_rect" : [ 112.0, 520.0, 41.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"format" : 4,
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"triangle" : 0,
					"numinlets" : 1,
					"ignoreclick" : 1,
					"hbgcolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"id" : "obj-75",
					"fontname" : "Arial Bold",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 128.0, 34.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Analysis presets:",
					"frgb" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"patching_rect" : [ 168.0, 24.0, 93.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-40",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 8.0, 93.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.menu",
					"annotation" : "Select a preset for analysis.",
					"varname" : "Presets",
					"outlettype" : [ "", "", "float" ],
					"patching_rect" : [ 176.0, 40.0, 72.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"numinlets" : 1,
					"pictures" : [  ],
					"id" : "obj-37",
					"numoutlets" : 3,
					"presentation_rect" : [ 176.0, 8.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Presets",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : -1,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 1,
							"parameter_enum" : [ "Default", "Sax", "SaxT/ClarBb", "BassClar", "Bass", "Fagott", "Guitar", "Voice", "Violin", "Harmonica", "Flute" ],
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 10,
							"parameter_mmax" : 127.0,
							"parameter_mmin" : 0.0,
							"parameter_type" : 2,
							"parameter_initial_enable" : 0,
							"parameter_shortname" : "Presets",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "umenu",
					"outlettype" : [ "int", "", "" ],
					"types" : [  ],
					"patching_rect" : [ 176.0, 64.0, 99.0, 18.0 ],
					"fontsize" : 10.0,
					"arrowframe" : 0,
					"items" : [ 0.5, 0.5, 50, 36, 1, ",", 0.52, 0.56, 55, 43, 1, ",", 0.48, 0.57, 57, 43, 1, ",", 0.5, 0.58, 66, 30, 2, ",", 0.44, 0.6, 67, 28, 2, ",", 0.31, 0.45, 57, 30, 2, ",", 0.52, 0.63, 63, 38, 2, ",", 0.4, 0.56, 55, 39, 2, ",", 0.4, 0.5, 45, 43, 1, ",", 0.5, 0.55, 50, 31, 2, ",", 0.56, 0.7, 60, 31, 2 ],
					"numinlets" : 1,
					"id" : "obj-22",
					"fontname" : "Arial Bold",
					"numoutlets" : 3
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "unpack 0. 0. 0. 0. 0",
					"outlettype" : [ "float", "float", "float", "float", "int" ],
					"patching_rect" : [ 216.0, 88.0, 114.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-5",
					"fontname" : "Arial Bold",
					"numoutlets" : 5
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Audio input",
					"patching_rect" : [ 80.0, 16.0, 66.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-49",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "R",
					"patching_rect" : [ 130.0, 40.0, 19.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-48",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "L",
					"patching_rect" : [ 68.0, 40.0, 19.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-47",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "inlet",
					"outlettype" : [ "" ],
					"patching_rect" : [ 112.0, 40.0, 17.0, 17.0 ],
					"numinlets" : 0,
					"id" : "obj-44",
					"numoutlets" : 1,
					"comment" : "Audio in R"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Channel",
					"patching_rect" : [ 64.0, 72.0, 50.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-107",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 16.0, 8.0, 50.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "+ 1",
					"outlettype" : [ "int" ],
					"patching_rect" : [ 64.0, 112.0, 32.5, 18.0 ],
					"fontsize" : 10.0,
					"textcolor" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"color" : [ 0.572549, 0.615686, 0.658824, 1.0 ],
					"bgcolor" : [ 0.909804, 0.909804, 0.909804, 1.0 ],
					"numinlets" : 2,
					"id" : "obj-104",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.menu",
					"annotation" : "Select input channel(s).",
					"varname" : "Channel",
					"outlettype" : [ "", "", "float" ],
					"patching_rect" : [ 64.0, 88.0, 48.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"numinlets" : 1,
					"pictures" : [  ],
					"id" : "obj-103",
					"numoutlets" : 3,
					"presentation_rect" : [ 16.0, 24.0, 48.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Channel",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_enum" : [ "Left", "Right", "Both" ],
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 10,
							"parameter_mmax" : 127.0,
							"parameter_mmin" : 0.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_type" : 2,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "Channel",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "selector~ 3",
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 64.0, 144.0, 91.0, 18.0 ],
					"fontsize" : 10.0,
					"textcolor" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"color" : [ 0.572549, 0.615686, 0.658824, 1.0 ],
					"bgcolor" : [ 0.909804, 0.909804, 0.909804, 1.0 ],
					"numinlets" : 4,
					"id" : "obj-102",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "*~ 0.707",
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 136.0, 112.0, 48.0, 18.0 ],
					"fontsize" : 10.0,
					"textcolor" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"color" : [ 0.572549, 0.615686, 0.658824, 1.0 ],
					"bgcolor" : [ 0.909804, 0.909804, 0.909804, 1.0 ],
					"numinlets" : 2,
					"id" : "obj-101",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "mtof",
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.0, 256.0, 32.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-9",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Downsampling:",
					"patching_rect" : [ 353.0, 208.0, 85.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-39",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 88.0, 85.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "YinSampl $1",
					"outlettype" : [ "" ],
					"patching_rect" : [ 319.0, 305.0, 70.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-38",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.menu",
					"annotation" : "Analysis downsampling (0 = off, 1 | 2 | 3 = downsampling by 2 | 4 | 8). Downsampling is an important factor in terms of the CPU use of the analysis engine.",
					"varname" : "Downsamp",
					"outlettype" : [ "", "", "float" ],
					"patching_rect" : [ 311.0, 208.0, 35.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"numinlets" : 1,
					"pictures" : [  ],
					"id" : "obj-36",
					"numoutlets" : 3,
					"presentation_rect" : [ 176.0, 88.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Downsamp",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_enum" : [ "0", "1", "2", "3" ],
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 10,
							"parameter_mmax" : 127.0,
							"parameter_mmin" : 0.0,
							"parameter_initial" : [ 1 ],
							"parameter_type" : 2,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "Downsamp",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "YinFreq $1",
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.0, 280.0, 61.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-31",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Minimum pitch:",
					"patching_rect" : [ 344.0, 184.0, 84.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-34",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 72.0, 84.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.numbox",
					"annotation" : "The minimum pitch to search.",
					"varname" : "MinPitch",
					"outlettype" : [ "", "float" ],
					"patching_rect" : [ 287.0, 184.0, 48.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"appearance" : 1,
					"numinlets" : 1,
					"id" : "obj-35",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 72.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "MinPitch",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 8,
							"parameter_mmax" : 48.0,
							"parameter_mmin" : 24.0,
							"parameter_initial" : [ 36 ],
							"parameter_type" : 0,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "MinPitch",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "Window $1",
					"outlettype" : [ "" ],
					"patching_rect" : [ 264.0, 232.0, 63.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-28",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Statistics win:",
					"patching_rect" : [ 320.0, 160.0, 78.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-29",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 56.0, 78.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.numbox",
					"annotation" : "Defines a time window after which an onset is validated if the estimated pitch remains stable during that time.",
					"varname" : "StatsWin",
					"outlettype" : [ "", "float" ],
					"patching_rect" : [ 264.0, 160.0, 48.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"appearance" : 1,
					"numinlets" : 1,
					"id" : "obj-30",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 56.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "StatsWin",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 2,
							"parameter_mmax" : 100.0,
							"parameter_mmin" : 10.0,
							"parameter_initial" : [ 50 ],
							"parameter_type" : 0,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "StatsWin",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "Proba $1",
					"outlettype" : [ "" ],
					"patching_rect" : [ 240.0, 208.0, 53.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-21",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Consistency:",
					"patching_rect" : [ 296.0, 136.0, 73.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-23",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 40.0, 73.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.numbox",
					"annotation" : "Consistency.",
					"varname" : "Consistency",
					"outlettype" : [ "", "float" ],
					"patching_rect" : [ 240.0, 136.0, 48.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"appearance" : 1,
					"numinlets" : 1,
					"id" : "obj-26",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 40.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Consistency",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 1,
							"parameter_mmax" : 1.0,
							"parameter_mmin" : 0.0,
							"parameter_initial" : [ 0.5 ],
							"parameter_type" : 0,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "Consistency",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "Quality $1",
					"outlettype" : [ "" ],
					"patching_rect" : [ 216.0, 184.0, 58.0, 16.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-19",
					"fontname" : "Arial Bold",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "inlet",
					"outlettype" : [ "" ],
					"patching_rect" : [ 88.0, 40.0, 17.0, 17.0 ],
					"numinlets" : 0,
					"id" : "obj-15",
					"numoutlets" : 1,
					"comment" : "Audio in L"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "< Pattr storage",
					"patching_rect" : [ 312.0, 441.0, 76.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-43",
					"fontname" : "Arial",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Messages",
					"patching_rect" : [ 240.0, 408.0, 58.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-12",
					"fontname" : "Arial Bold",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.gain~",
					"annotation" : "Input level.",
					"varname" : "Level",
					"prototypename" : "M4L.live.gain~.V.extended",
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"patching_rect" : [ 64.0, 184.0, 48.0, 96.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"clip_size" : 1,
					"display_range" : [ -70.0, 30.0 ],
					"numinlets" : 2,
					"id" : "obj-8",
					"numoutlets" : 5,
					"presentation_rect" : [ 16.0, 43.0, 48.0, 117.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Level",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 4,
							"parameter_mmax" : 30.0,
							"parameter_mmin" : -70.0,
							"parameter_initial" : [ 0.0 ],
							"parameter_type" : 0,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "Level",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "textbutton",
					"mode" : 1,
					"outlettype" : [ "", "", "int" ],
					"border" : 0,
					"bordercolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"bgoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"patching_rect" : [ 117.0, 346.0, 13.0, 13.0 ],
					"text" : "",
					"fontsize" : 10.0,
					"presentation" : 1,
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"rounded" : 24.0,
					"textcolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"texton" : "",
					"bgcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoncolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"numinlets" : 1,
					"borderoncolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"ignoreclick" : 1,
					"textovercolor" : [ 0.698039, 0.698039, 0.698039, 0.0 ],
					"id" : "obj-2",
					"fontname" : "Arial Bold",
					"numoutlets" : 3,
					"presentation_rect" : [ 301.0, 146.0, 13.0, 13.0 ],
					"bgovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Note:",
					"frgb" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"patching_rect" : [ 136.0, 344.0, 38.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-10",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 248.0, 144.0, 38.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "UI components:",
					"patching_rect" : [ 216.0, 536.0, 81.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-6",
					"fontname" : "Arial",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "route restore",
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 288.0, 473.0, 75.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-41",
					"fontname" : "Arial Bold",
					"numoutlets" : 2
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "inlet",
					"outlettype" : [ "" ],
					"patching_rect" : [ 216.0, 408.0, 17.0, 17.0 ],
					"numinlets" : 0,
					"id" : "obj-52",
					"numoutlets" : 1,
					"comment" : "Parameters values, getattributes, getstate"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"varname" : "u342003354",
					"text" : "autopattr @autorestore 0",
					"linecount" : 2,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 216.0, 440.0, 91.0, 29.0 ],
					"fontsize" : 10.0,
					"numinlets" : 1,
					"id" : "obj-55",
					"fontname" : "Arial Bold",
					"numoutlets" : 4,
					"restore" : 					{
						"Channel" : [ 0.0 ],
						"Consistency" : [ 0.5 ],
						"Downsamp" : [ 1.0 ],
						"Level" : [ 0.0 ],
						"MinPitch" : [ 36.0 ],
						"Presets" : [ 0.0 ],
						"StatsWin" : [ 50.0 ],
						"Threshold" : [ 0.5 ]
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Noise thresh:",
					"patching_rect" : [ 272.0, 112.0, 74.0, 18.0 ],
					"fontsize" : 10.0,
					"presentation" : 1,
					"numinlets" : 1,
					"id" : "obj-18",
					"fontname" : "Arial Bold",
					"numoutlets" : 0,
					"presentation_rect" : [ 80.0, 24.0, 74.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "live.numbox",
					"annotation" : "Defines a 'quality' factor under which the input signal is 'rejected' from the pitch detection. This is similar to a noise threshold..",
					"varname" : "Threshold",
					"outlettype" : [ "", "float" ],
					"patching_rect" : [ 216.0, 112.0, 48.0, 15.0 ],
					"presentation" : 1,
					"parameter_enable" : 1,
					"appearance" : 1,
					"numinlets" : 1,
					"id" : "obj-4",
					"numoutlets" : 2,
					"presentation_rect" : [ 176.0, 24.0, 49.0, 15.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "Threshold",
							"parameter_modmin" : 0.0,
							"parameter_linknames" : 1,
							"parameter_modmode" : 0,
							"parameter_info" : "",
							"parameter_units" : "",
							"parameter_order" : 0,
							"parameter_defer" : 0,
							"parameter_speedlim" : 1.0,
							"parameter_steps" : 0,
							"parameter_invisible" : 0,
							"parameter_exponent" : 1.0,
							"parameter_annotation_name" : "",
							"parameter_unitstyle" : 1,
							"parameter_mmax" : 1.0,
							"parameter_mmin" : 0.0,
							"parameter_initial" : [ 0.5 ],
							"parameter_type" : 0,
							"parameter_initial_enable" : 1,
							"parameter_shortname" : "Threshold",
							"parameter_modmax" : 127.0
						}

					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"varname" : "Yin+",
					"text" : "OMax.yin+ #0",
					"outlettype" : [ "", "int", "int", "float" ],
					"patching_rect" : [ 64.0, 320.0, 171.0, 18.0 ],
					"fontsize" : 10.0,
					"numinlets" : 2,
					"id" : "obj-33",
					"fontname" : "Arial Bold",
					"numoutlets" : 4
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "panel",
					"border" : 1,
					"bordercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"grad2" : [ 0.415686, 0.454902, 0.52549, 1.0 ],
					"patching_rect" : [ 296.0, 536.0, 16.0, 16.0 ],
					"presentation" : 1,
					"rounded" : 16,
					"bgcolor" : [ 0.094118, 0.113725, 0.137255, 0.0 ],
					"numinlets" : 1,
					"id" : "obj-27",
					"numoutlets" : 0,
					"presentation_rect" : [ 241.0, 7.0, 49.0, 137.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "nslider",
					"outlettype" : [ "int", "int" ],
					"bordercolor" : [ 0.494118, 0.556863, 0.607843, 0.0 ],
					"patching_rect" : [ 112.0, 424.0, 32.0, 88.888878 ],
					"presentation" : 1,
					"rounded" : 16,
					"bgcolor" : [ 0.909804, 0.909804, 0.909804, 1.0 ],
					"numinlets" : 2,
					"ignoreclick" : 1,
					"id" : "obj-24",
					"numoutlets" : 2,
					"fgcolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"presentation_rect" : [ 241.0, 6.0, 49.639999, 137.888885 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "slider",
					"prototypename" : "M4L.black.H",
					"outlettype" : [ "" ],
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 0.0 ],
					"patching_rect" : [ 152.0, 424.0, 16.0, 96.0 ],
					"presentation" : 1,
					"floatoutput" : 1,
					"bgcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"numinlets" : 1,
					"ignoreclick" : 1,
					"knobcolor" : [ 0.909804, 0.909804, 0.909804, 1.0 ],
					"id" : "obj-14",
					"numoutlets" : 1,
					"presentation_rect" : [ 296.0, 7.0, 24.0, 137.0 ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "panel",
					"prototypename" : "M4L.horizontal-black",
					"bordercolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"patching_rect" : [ 320.0, 536.0, 16.0, 16.0 ],
					"presentation" : 1,
					"rounded" : 16,
					"background" : 1,
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"numinlets" : 1,
					"id" : "obj-142",
					"numoutlets" : 0,
					"presentation_rect" : [ 0.0, 0.0, 339.0, 168.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"source" : [ "obj-79", 1 ],
					"destination" : [ "obj-14", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-16", 0 ],
					"destination" : [ "obj-79", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-79", 0 ],
					"destination" : [ "obj-24", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-14", 0 ],
					"destination" : [ "obj-13", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-46", 0 ],
					"destination" : [ "obj-54", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-33", 3 ],
					"destination" : [ "obj-46", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-2", 0 ],
					"destination" : [ "obj-25", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-16", 0 ],
					"destination" : [ "obj-82", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-33", 0 ],
					"destination" : [ "obj-16", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-38", 0 ],
					"destination" : [ "obj-33", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-31", 0 ],
					"destination" : [ "obj-33", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-28", 0 ],
					"destination" : [ "obj-33", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-21", 0 ],
					"destination" : [ "obj-33", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-19", 0 ],
					"destination" : [ "obj-33", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-33", 1 ],
					"destination" : [ "obj-2", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-8", 0 ],
					"destination" : [ "obj-33", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-102", 0 ],
					"destination" : [ "obj-8", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-24", 0 ],
					"destination" : [ "obj-75", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-37", 0 ],
					"destination" : [ "obj-22", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-4", 0 ],
					"destination" : [ "obj-19", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-26", 0 ],
					"destination" : [ "obj-21", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-30", 0 ],
					"destination" : [ "obj-28", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-36", 1 ],
					"destination" : [ "obj-38", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-35", 0 ],
					"destination" : [ "obj-9", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-9", 0 ],
					"destination" : [ "obj-31", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-101", 0 ],
					"destination" : [ "obj-102", 3 ],
					"hidden" : 0,
					"color" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-103", 0 ],
					"destination" : [ "obj-104", 0 ],
					"hidden" : 0,
					"color" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-104", 0 ],
					"destination" : [ "obj-102", 0 ],
					"hidden" : 0,
					"color" : [ 0.0, 0.019608, 0.078431, 1.0 ],
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-15", 0 ],
					"destination" : [ "obj-102", 1 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-44", 0 ],
					"destination" : [ "obj-102", 2 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-15", 0 ],
					"destination" : [ "obj-101", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-44", 0 ],
					"destination" : [ "obj-101", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 3 ],
					"destination" : [ "obj-35", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 2 ],
					"destination" : [ "obj-30", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 1 ],
					"destination" : [ "obj-26", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 0 ],
					"destination" : [ "obj-4", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-22", 1 ],
					"destination" : [ "obj-5", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 4 ],
					"destination" : [ "obj-36", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-41", 1 ],
					"destination" : [ "obj-3", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-55", 3 ],
					"destination" : [ "obj-41", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-52", 0 ],
					"destination" : [ "obj-55", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
 ],
		"parameters" : 		{
			"obj-26" : [ "Consistency", "Consistency", 0 ],
			"obj-36" : [ "Downsamp", "Downsamp", 0 ],
			"obj-37" : [ "Presets", "Presets", -1 ],
			"obj-8" : [ "Level", "Level", 0 ],
			"obj-35" : [ "MinPitch", "MinPitch", 0 ],
			"obj-103" : [ "Channel", "Channel", 0 ],
			"obj-4" : [ "Threshold", "Threshold", 0 ],
			"obj-30" : [ "StatsWin", "StatsWin", 0 ]
		}

	}

}
