{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 5,
			"architecture" : "x86"
		}
,
		"rect" : [ 25.0, 69.0, 547.0, 568.0 ],
		"bglocked" : 1,
		"openinpresentation" : 1,
		"default_fontsize" : 10.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial Bold",
		"gridonopen" : 0,
		"gridsize" : [ 8.0, 8.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"boxanimatetime" : 500,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-76",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 64.0, 496.0, 36.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 145.0, 342.0, 36.0, 18.0 ],
					"text" : "LSPs"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "signal", "" ],
					"patching_rect" : [ 96.0, 408.0, 147.0, 18.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "spat.spat~ @numspeakers 4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 120.0, 368.0, 141.0, 18.0 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "spat.oper @numspeakers 4"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-50",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 285.0, 528.0, 24.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 172.0, 434.0, 24.0, 18.0 ],
					"text" : "Ls",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-51",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 229.0, 528.0, 25.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 157.0, 434.0, 24.0, 18.0 ],
					"text" : "Rs",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-52",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.0, 528.0, 19.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 145.0, 434.0, 21.0, 18.0 ],
					"text" : "R",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 120.0, 528.0, 19.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 131.0, 434.0, 20.0, 18.0 ],
					"text" : "L",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 0,
					"patching_rect" : [ 96.0, 544.0, 188.5, 18.0 ],
					"text" : "dac~ 1 2 3 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "live.meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "list" ],
					"patching_rect" : [ 288.0, 480.0, 16.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 178.0, 361.0, 6.0, 69.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "live.meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "list" ],
					"patching_rect" : [ 232.0, 480.0, 16.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 164.0, 361.0, 6.0, 69.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "live.meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "list" ],
					"patching_rect" : [ 176.0, 480.0, 16.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 150.0, 361.0, 6.0, 69.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "signal", "signal", "signal", "signal" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 91.0, 104.0, 467.0, 257.0 ],
						"bglocked" : 1,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-38",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 150.0, 203.0, 24.0, 18.0 ],
									"text" : "Ls"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-39",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 110.0, 203.0, 25.0, 18.0 ],
									"text" : "Rs"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-40",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 72.0, 203.0, 19.0, 18.0 ],
									"text" : "R"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-41",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 32.0, 203.0, 19.0, 18.0 ],
									"text" : "L"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-25",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 32.0, 184.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-24",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 72.0, 184.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 112.0, 184.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-22",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 152.0, 184.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-21",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "signal", "bang" ],
									"patching_rect" : [ 192.0, 112.0, 34.0, 18.0 ],
									"text" : "line~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-20",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 192.0, 88.0, 36.0, 16.0 ],
									"text" : "$1 50"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-18",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 192.0, 64.0, 38.0, 18.0 ],
									"text" : "dbtoa"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-16",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 186.0, 13.0, 33.0, 18.0 ],
									"text" : "Gain"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-17",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 192.0, 32.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-9",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 154.0, 13.0, 22.0, 18.0 ],
									"text" : "4"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 152.0, 32.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-6",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 114.0, 13.0, 19.0, 18.0 ],
									"text" : "3"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-7",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 112.0, 32.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-3",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 74.0, 13.0, 19.0, 18.0 ],
									"text" : "2"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 72.0, 32.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 5,
									"numoutlets" : 5,
									"outlettype" : [ "signal", "signal", "signal", "signal", "" ],
									"patching_rect" : [ 32.0, 145.0, 179.0, 18.0 ],
									"saved_object_attributes" : 									{
										"parameter_enable" : 0
									}
,
									"text" : "spat.times~ @numchannels 4"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-14",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 34.0, 13.0, 19.0, 18.0 ],
									"text" : "1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-12",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "signal" ],
									"patching_rect" : [ 32.0, 32.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-29",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 192.0, 96.0, 29.0 ],
									"text" : "More about the spat.times~ object:"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-5",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 352.0, 208.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-8",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 352.0, 184.0, 89.0, 16.0 ],
									"text" : "help spat.times~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 216.0, 144.0, 36.0, 18.0 ],
									"text" : "<------"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold Italic",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-47",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 144.0, 58.0, 18.0 ],
									"text" : "Attributes"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 11.0,
									"frgb" : 0.0,
									"id" : "obj-46",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 56.0, 74.0, 19.0 ],
									"text" : "Spat.times~"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-36",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 160.0, 104.0, 29.0 ],
									"text" : "@numchannels: number of channels"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-31",
									"linecount" : 5,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 72.0, 133.0, 62.0 ],
									"text" : "Similar to times~ (*~) with multichannel inputs/ouputs.\nAll input signals are multiplied by the rightmost gain (float or signal)"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold Italic",
									"fontsize" : 12.0,
									"frgb" : 0.0,
									"id" : "obj-19",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 248.0, 24.0, 166.0, 20.0 ],
									"text" : "Controlling the overall gain"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-23", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-24", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-25", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 3 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-20", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-21", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-20", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 4 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-21", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 2 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 96.0, 448.0, 189.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Gain~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "live.meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "list" ],
					"patching_rect" : [ 120.0, 480.0, 16.0, 48.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 136.0, 361.0, 6.0, 69.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "live.slider",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "float" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 266.0, 344.0, 40.0, 96.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 88.0, 344.0, 40.0, 104.0 ],
					"prototypename" : "gain",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "OutputGain",
							"parameter_shortname" : "Master",
							"parameter_type" : 0,
							"parameter_mmin" : -70.0,
							"parameter_mmax" : 30.0,
							"parameter_initial_enable" : 1,
							"parameter_initial" : [ 0 ],
							"parameter_unitstyle" : 4
						}

					}
,
					"varname" : "OutputGain"
				}

			}
, 			{
				"box" : 				{
					"args" : [ "@Folder", "./examples/sounds", "@File", "drumLoop.aif", "@Loop", 1 ],
					"id" : "obj-23",
					"maxclass" : "bpatcher",
					"name" : "FMA.SoundPlayer~.maxpat",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "" ],
					"patching_rect" : [ 96.0, 152.0, 272.0, 136.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 80.0, 176.0, 272.0, 136.0 ],
					"varname" : "Fma.SoundPlayer~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-72",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 368.0, 320.0, 78.0, 18.0 ],
					"text" : "UI components"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-12",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 194.0, 352.0, 48.0, 19.0 ],
					"text" : "control",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-10",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 168.0, 352.0, 34.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 288.0, 368.0, 34.0, 19.0 ],
					"text" : "Spat",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-95",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 382.0, 504.0, 31.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 207.0, 342.0, 32.0, 18.0 ],
					"text" : "DSP",
					"textcolor" : [ 0.101961, 0.121569, 0.172549, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 120.0, 344.0, 34.0, 16.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"automation" : "Arm",
					"automationon" : "Trigger",
					"fontsize" : 18.0,
					"id" : "obj-66",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 120.0, 312.0, 64.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 312.0, 416.0, 72.0, 24.0 ],
					"prototypename" : "numbers.default",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "OpenOper",
							"parameter_shortname" : "OpenOper",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Arm", "Trigger" ],
							"parameter_invisible" : 2
						}

					}
,
					"text" : "Oper",
					"texton" : "Oper",
					"varname" : "OpenOper"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-81",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 488.0, 42.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 80.0, 320.0, 42.0, 19.0 ],
					"text" : "Audio",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 237.0, 109.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 104.0, 72.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-4",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 104.0, 48.0, 96.0, 16.0 ],
									"text" : "load QuickRecord"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-5",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 128.0, 16.0, 45.0, 18.0 ],
									"text" : "Record"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 48.0, 16.0, 38.0, 18.0 ],
									"text" : "Setup"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-22",
									"linecount" : 2,
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 24.0, 48.0, 55.0, 27.0 ],
									"text" : ";\rdsp open"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-56",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 24.0, 16.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-58",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 104.0, 16.0, 18.0, 18.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-22", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-56", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-58", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 440.0, 544.0, 51.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Audio"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Open QuickRecord.",
					"automation" : "Arm",
					"automationon" : "Trigger",
					"hint" : "",
					"id" : "obj-167",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 472.0, 520.0, 48.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 200.0, 424.0, 44.0, 16.0 ],
					"prototypename" : "numbers.default",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "AudioRecord",
							"parameter_shortname" : "Record",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Arm", "Trigger" ],
							"parameter_invisible" : 2
						}

					}
,
					"text" : "Record",
					"texton" : "Record",
					"varname" : "AudioRecord"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Load DSP Status",
					"automation" : "Arm",
					"automationon" : "Trigger",
					"hint" : "",
					"id" : "obj-165",
					"maxclass" : "live.text",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 440.0, 496.0, 48.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 200.0, 406.0, 44.0, 16.0 ],
					"prototypename" : "numbers.default",
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_linknames" : 1,
							"parameter_longname" : "AudioSetup",
							"parameter_shortname" : "Setup",
							"parameter_type" : 2,
							"parameter_mmax" : 1.0,
							"parameter_enum" : [ "Arm", "Trigger" ],
							"parameter_invisible" : 2
						}

					}
,
					"text" : "Setup",
					"texton" : "Setup",
					"varname" : "AudioSetup"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Turn DSP on and off.",
					"bgcolor" : [ 0.415686, 0.454902, 0.52549, 1.0 ],
					"hint" : "",
					"id" : "obj-77",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"offgradcolor1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"offgradcolor2" : [ 0.415686, 0.454902, 0.52549, 1.0 ],
					"ongradcolor1" : [ 1.0, 1.0, 1.0, 1.0 ],
					"ongradcolor2" : [ 1.0, 0.74902, 0.231373, 1.0 ],
					"patching_rect" : [ 376.0, 520.0, 42.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 202.0, 360.0, 40.0, 40.0 ],
					"prototypename" : "M4L.white"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-6",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 168.0, 392.0, 60.0, 19.0 ],
					"text" : "Spat DSP",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-156",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 96.0, 136.0, 54.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 80.0, 160.0, 54.0, 19.0 ],
					"text" : "Source1",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-7",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 188.0, 315.0, 102.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 296.0, 392.0, 102.0, 20.0 ],
					"text" : "Open Spat Oper"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"bordercolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"id" : "obj-8",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 336.0, 16.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 288.0, 384.0, 120.0, 72.0 ],
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"bordercolor" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"id" : "obj-73",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 400.0, 336.0, 16.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 80.0, 336.0, 176.0, 120.0 ],
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-70",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 776.0, 256.0, 19.0, 18.0 ],
					"text" : "m"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-68",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 704.0, 256.0, 27.0, 18.0 ],
					"text" : "deg"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-67",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 589.0, 240.0, 46.0, 18.0 ],
					"text" : "Speaker"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-59",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 733.0, 240.0, 49.0, 18.0 ],
					"text" : "Distance"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-58",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 661.0, 240.0, 47.0, 18.0 ],
					"text" : "Azimuth"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-65",
					"maxclass" : "number",
					"maximum" : 4,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 592.0, 256.0, 39.0, 18.0 ],
					"prototypename" : "Live",
					"triscale" : 0.75
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 280.0, 163.0, 18.0 ],
					"text" : "pak 1 -45. 1."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-54",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 440.0, 160.0, 27.0 ],
					"text" : "speakers xy -0.707 0.707 0.707 0.707 0.707 -0.707 -0.707 -0.707"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-48",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 384.0, 195.0, 16.0 ],
					"text" : "speakers ad -45. 1. 45. 1. 135 1. -135. 1."
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 536.0, 351.0, 19.0, 18.0 ],
					"text" : "t l"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-28",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 736.0, 256.0, 39.0, 18.0 ],
					"prototypename" : "Live",
					"triscale" : 0.75
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-27",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 664.0, 256.0, 39.0, 18.0 ],
					"prototypename" : "Live",
					"triscale" : 0.75
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 304.0, 104.0, 16.0 ],
					"text" : "speaker $1 ad $2 $3"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 187.0, 145.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 112.0, 64.0, 39.0, 16.0 ],
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-6",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 112.0, 94.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 72.0, 64.0, 39.0, 16.0 ],
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-4",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 72.0, 94.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 64.0, 39.0, 16.0 ],
									"text" : "set $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-37",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 3,
									"outlettype" : [ "int", "float", "float" ],
									"patching_rect" : [ 32.0, 40.0, 99.0, 18.0 ],
									"text" : "unpack 1 -45. 1."
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-32",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 16.0, 96.0, 18.0 ],
									"text" : "loadmess 1 -45. 1."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-55",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 32.0, 94.0, 18.0, 18.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-55", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-37", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-32", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-37", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 592.0, 232.0, 163.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Init"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-87",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 560.0, 432.0, 16.0, 128.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-85",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 488.0, 168.0, 31.0 ],
					"text" : "More about defining sources and speakers positions in Spat:"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-79",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 408.0, 208.0, 31.0 ],
					"text" : "It is also possible to define positions using cartesian coordinates (x/y pairs):"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-74",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 344.0, 256.0, 31.0 ],
					"text" : "Speakers positions can be adjusted using one single message made of azimuth/distance pairs:"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-71",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 704.0, 304.0, 144.0, 29.0 ],
					"text" : "< Syntax (\"ad\" stands for azimuth-distance coordinates)"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-19",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 104.0, 133.0, 68.0 ],
					"text" : "Ctrl-click on the Viewer and turn the \"speakers editable\" option on, then place the speakers wherever you'd like to:"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-15",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 200.0, 256.0, 31.0 ],
					"text" : "Or you can adjust the speakers positions by sending the appropriate message to the spat.oper:"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-13",
					"linecount" : 3,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 592.0, 48.0, 277.0, 43.0 ],
					"text" : "Once you've set the number of speakers, you can also adjust their positions (according to your own setup), either graphically:"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Italic",
					"fontsize" : 9.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 104.0, 608.0, 122.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 111.0, 511.0, 128.0, 17.0 ],
					"text" : "IRCAM 3D Audio Processor"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 24.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-161",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 600.0, 64.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 48.0, 503.0, 65.0, 33.0 ],
					"text" : "Spat",
					"textcolor" : [ 1.0, 0.603922, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-96",
					"justification" : 2,
					"linecolor" : [ 1.0, 0.603922, 0.0, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 104.0, 608.0, 688.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 111.0, 511.0, 305.0, 16.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 25.0, 69.0, 202.0, 113.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 72.0, 50.0, 18.0 ],
									"text" : "pcontrol"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-2",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 48.0, 142.0, 16.0 ],
									"text" : "loadunique ForumMaxApps"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-51",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 32.0, 16.0, 18.0, 18.0 ],
									"prototypename" : "M4L.Arial10"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-51", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 800.0, 632.0, 64.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p Overview"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bgovercolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoveroncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hint" : "",
					"id" : "obj-169",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 800.0, 612.0, 69.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 424.0, 515.0, 69.0, 17.0 ],
					"prototypename" : "M4L.toggle",
					"rounded" : 16.0,
					"text" : "Overview",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"texton" : "Overview",
					"textoncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-18",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 376.0, 63.0, 19.0 ],
					"text" : "Attributes"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-17",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 392.0, 143.0, 31.0 ],
					"text" : "@numspeakers: the number of output speakers"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 5,
							"architecture" : "x86"
						}
,
						"rect" : [ 79.0, 92.0, 389.0, 315.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 10.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"gridonopen" : 0,
						"gridsize" : [ 8.0, 8.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-8",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 168.0, 272.0, 40.0, 18.0 ],
									"text" : "To tab"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 144.0, 272.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-118",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 144.0, 232.0, 65.0, 18.0 ],
									"text" : "loadmess 0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-5",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 168.0, 16.0, 37.0, 18.0 ],
									"text" : "Mode"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-4",
									"linecount" : 2,
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 48.0, 16.0, 72.0, 29.0 ],
									"text" : "Set current window size"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-3",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 280.0, 16.0, 93.0, 18.0 ],
									"text" : "From thispatcher"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"frgb" : 0.0,
									"id" : "obj-2",
									"maxclass" : "comment",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 48.0, 272.0, 79.0, 18.0 ],
									"text" : "To thispatcher"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-115",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 144.0, 184.0, 192.0, 16.0 ],
									"text" : "window size $1 $2 $3 $4, window exec"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-113",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 3,
									"outlettype" : [ "bang", "bang", "" ],
									"patching_rect" : [ 144.0, 96.0, 46.0, 18.0 ],
									"text" : "sel 0 1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-112",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 184.0, 144.0, 91.0, 16.0 ],
									"text" : "26 69 960 747"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-111",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 144.0, 120.0, 91.0, 16.0 ],
									"text" : "25 69 572 637"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-106",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 216.0, 72.0, 32.5, 18.0 ],
									"text" : "+ 1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-105",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 216.0, 96.0, 59.0, 18.0 ],
									"text" : "gate 2 1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-103",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 48.0, 184.0, 84.0, 16.0 ],
									"text" : "presentation $1"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-101",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 48.0, 160.0, 32.5, 18.0 ],
									"text" : "== 0"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-94",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 256.0, 48.0, 75.0, 18.0 ],
									"text" : "route window"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-91",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 256.0, 72.0, 57.0, 18.0 ],
									"text" : "route size"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial Bold",
									"fontsize" : 10.0,
									"id" : "obj-87",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 24.0, 72.0, 84.0, 16.0 ],
									"text" : "window getsize"
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-23",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 144.0, 16.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-25",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 24.0, 16.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-26",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 256.333313, 16.0, 18.0, 18.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-29",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 24.0, 272.0, 18.0, 18.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-103", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-101", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-103", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-105", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-105", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-106", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-111", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-115", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-112", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-111", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-113", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-112", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-113", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-115", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-118", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-101", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-106", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-113", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-23", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-87", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-25", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-94", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-26", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-29", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-87", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-105", 1 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-91", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-91", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-94", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 296.0, 8.0, 78.0, 18.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial Bold",
						"default_fontsize" : 10.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial Bold",
						"fontsize" : 10.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p WindowSize"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 24.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 18.0, 131.0, 33.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 48.0, 36.0, 131.0, 33.0 ],
					"text" : "Quad Spat",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"bgovercolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"bgoveroncolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hidden" : 1,
					"hint" : "",
					"id" : "obj-41",
					"maxclass" : "textbutton",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 208.0, 8.0, 87.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 220.0, 16.0, 87.0, 17.0 ],
					"prototypename" : "M4L.toggle",
					"rounded" : 12.0,
					"text" : "Set current size",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"texton" : "Set size",
					"textoncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textovercolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"textoveroncolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"annotation" : "Toggle Presentation/Patching views.",
					"background" : 1,
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"borderoncolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"clicktabcolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"clicktextcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"hint" : "",
					"hovertabcolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"hovertextcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"htabcolor" : [ 0.921569, 0.94902, 0.05098, 1.0 ],
					"htextcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"id" : "obj-78",
					"margin" : 2,
					"maxclass" : "tab",
					"multiline" : 0,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 384.0, 27.0, 152.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 344.0, 14.0, 152.0, 17.0 ],
					"prototypename" : "M4L.1",
					"rounded" : 16.0,
					"spacing_y" : 0.0,
					"tabcolor" : [ 0.568627, 0.619608, 0.662745, 0.0 ],
					"tabs" : [ "Presentation", "Patching" ],
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ],
					"valign" : 2
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-22",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 348.0, 29.0, 36.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 308.0, 16.0, 36.0, 18.0 ],
					"text" : "View:",
					"textcolor" : [ 0.301961, 0.337255, 0.403922, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 376.0, 8.0, 68.0, 18.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Italic",
					"fontsize" : 11.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-5",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 40.0, 48.0, 506.0, 56.0 ],
					"presentation" : 1,
					"presentation_linecount" : 5,
					"presentation_rect" : [ 48.0, 64.0, 440.0, 68.0 ],
					"text" : "Changing the loudspeakers setup is very easy in Spat: you just need to set the desired number of loudspeakers by adjusting the value of the @numspeakers attribute in the Spat objects. Thus it is possible to create any configuration. The control parameters remain the same, whatever configuration is in use. In this patch we've created a quadraphonic Spat which feeds four audio channels."
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"border" : 1,
					"bordercolor" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"grad2" : [ 0.094118, 0.113725, 0.137255, 1.0 ],
					"hint" : "",
					"id" : "obj-9",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 424.0, 336.0, 16.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 40.0, 32.0, 456.0, 112.0 ],
					"prototypename" : "M4L.horizontal-black",
					"rounded" : 16
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 288.0, 448.0, 88.0, 18.0 ],
					"text" : "<---------------------"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-4",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 376.0, 440.0, 104.0, 29.0 ],
					"text" : "Double-click to see the embedded patch"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial",
					"fontsize" : 10.0,
					"frgb" : 0.0,
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 288.0, 544.0, 80.0, 18.0 ],
					"text" : "< Audio outputs"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold Italic",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"hint" : "",
					"id" : "obj-1",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 176.0, 29.0, 169.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 184.0, 47.0, 163.0, 20.0 ],
					"text" : "A quadraphonic spatializer"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"id" : "obj-86",
					"justification" : 1,
					"linecolor" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"maxclass" : "live.line",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 560.0, 48.0, 16.0, 272.0 ],
					"prototypename" : "M4L.live.line.dark.H"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 544.0, 50.0, 18.0 ],
					"text" : "pcontrol"
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"fontname" : "Arial Bold",
					"fontsize" : 10.0,
					"id" : "obj-83",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 592.0, 520.0, 134.0, 16.0 ],
					"text" : "loadunique Spat.Positions"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"background" : 1,
					"data" : [ 7460, "", "IBkSG0fBZn....PCIgDQRA...HJ...vQHX.....g6woJ....DLmPIQEBHf.B7g.YHB..brcRDEDU3wY6ctGeTUct2+6beljL4FjKxE4R.LhjHnnhmJDiHsUTRrGaQTQJ9JR8UaOR0VnPawZTwl1JJpzS7Us4HkHZA5aChIZULuDPITIHlvgDwQHBIQHCjIYljL2m06er2yjYRlIIDHXNeb984y9CL68y541dMqm0yJqm0nPHDBhhn3aXn7aZEHJhB.T2tS2rle4g9lVOhhukC0InSCZUEcfwn3aVDsGXTLr.pa2oab402EDlMyqNYROcCry2ooKH76BOTxLusQyzLzI+e2Zqz92zpy4HF6UmNy8JTvd9aeMeoiKPLUuAxagohli1BauJ6Qfngd+1EjPy5zojEtvwgNcJYSa53CiC0qgYO2zYRzN69u2FQxsObES86LJtlIoDy+ySyIMCwesikUsvDolRNBu0m5cvwzDimabloh5Q5f29SbFAhF58am28XRJIs7HORlnRkB9K+kigKWWXFccnBNbB3wGt9lVQFDvsGe.dvi7m0pQCwnRKIFi7qQc5Xd24X4NtACCbl5zGdA73oueuMT62NuBMmd554+3mlIM2bW7pE+k3y2.bII0qkLFkJvkfybFGzdfvLJQude3vAj5DMvHzBmsA6zRXBCoOECLgQnjN95N4j8JVgRRcLZwnVE35r1C54B7qgt81sSUudk.RxMDcb7ZPqK2T2w5g6WuRvgO.0jQl5vVCcJqiJYrSTGZQfsy5hVZu+8q8sc.ILFCLp3flq2NNbH.TfWu9vkWn4JOFqY+P6NkkSxIv2aVoBMXmsr6NQOP.SRuZF6nzPb.G+X1699xuy75wKt75iLxLVz5xKG+XN5llH32.XrSLVhSqOZtd6mWgrGzglSOc87vO7kgMat4+p3igZEJ.UJ521Mg4MAd34mLp7eCusye5WXhlPC2+SmMSCqbfiogYNM+eq1KGZ60wquW+gMTy7+IWFyKS8A3YCUbT1vNrA.wOiwvZWRZcyefF16QYCa2FfB7qgwpRIZAl2OIKlelZwbUGk08VR7Xz4NN9Y4MRzISqyS0BuXgmjl.H9QvS+DiGUmpcLGeBLlXfFK6v7rGJVV0iMAR2eiPPYq6f79lijmnusCPMy+guLl2j7+bAdQAfaToRIZUAi76LQ90+vjn9sUCubswySuxwJY2iebrwWXb3sgSvuXCV3NdroxMLFMAIaGT1KTGu+w8AJk7HphKY9kOUFL93kIoKqroB+B9TqDheSireiTRjGYEYv3iw+qIGT1Fk44f.CpPyiXDZ4AevofBEvq9plNGBGaf79dIiJmcvNds5YSauQpZesxYBljXhmYNM8X5.MQEGvJdQES+NlDWmrCZF24TXdYpGK02DEUzWR8VDL9byf4OZomqKVUzZisxNdyixqs8uFq.i+5SiQ2CMwFvLtyLY9YpEmMzTfNgLgzj5DZsc1dQ0SYGpSzkdp7iuSiAZqG.com.igNnpJZh8ZxM2v+9XHccP8u+WRQu1WREU0LmvZj8D8mcLs6bRLuIoGmm5LrohNJu+g5LjubELTqQIXsc1wdkFSxqYKTVYMx6ToU.EnWmWLcfFYSu1QorCzAfdl67SJT6YLIw3U2IU89MxgOkaHl3YI+uGUDjnFt+UjAiOFubnx9BdsczBcoROyeYiiThrI2m3bNzrd8pXYOvjH93UyetnufSY9bH8M8pIVU.n.GmsS1+mYi8G3g9G92A674qkxOlzcMG2zYgYpmqdFZYOeHbMyv.fMJ4O2L0Ab5cOBd7aOQF+jTiqS3hl18w42sa+7rcl7btDlS79nSu9vkeY3zCiOmwyRlUrfEy76WeyAB2jyMkJ5.9zcdL9vi3ANVSbMSeJjxTSBCdam18IPM.cYkW5W84Tmb6t44K8u5DNo1OqSp8y5SGQ+XGv0lcr.13UV2wkjwQZGKOzzYgYRfPydjiWJD9vkWWrmcbFl+Mj.pOaq7NuaqAjVwOYscK5OWE23LiCUHjduKaOdM2BO0S9UzB.usEVweLKlb5ifqOtFY2sGZnYCW8kvzhAbZpQdk2sMf1HsoNBtsIYjrS1GkGwn.QFQLzrJUJHmbREiF0vN2YS30qjpb+KMCROMCrqccJLcTamag0cai8UuKxKyXYgq7pIeqcvtdiuj2+y8Pfg+c1E07URgd.vzQ6.xLITnTAZSJNFsN.LxO8EtlPXcZSJNzt61.zv7tywx0mchjTLxATb5OjhrLhOEtuaGfN40J3Dzl+vM.pTK8uy3tmAa7tCR.wqijUoD6xgx55DmkuLn1U2Gag7lTpLgu6TYiy0MGtxF301QDFRLdi8sc7oJYbw.zkGrFrt4+ekCMqV17TnPoz6ACpPMfJ0pB48htQmH+vaOcxZBwhNYlzEJjnQ1dbd1NCxO3iN6BPmBhyfRz1Qngl0jjjSR2jjlBPvPopte2ct.0Q5A4laZbq2pTbBOdD7NuSSbi2XZL0ol.M2rcJu7lO2kFPEubszbtihaa1oxXRJNl+CNMz7BGhxNteJBcdlZUGbGc+euzN6cGmFq9elFk34qsCnl6YMYyLSArzPKriJswn9tSjYlD8.doKmpHFcwxbuCib3saqmDPCUcBpoU+NHkfG6gLEBUpCUOM+omjeca1XQ4MJxZ7FXZ4NYViguf08VgqyX+XG5hKruXb6QzK+S3PHKjynSg09KtThA2TeUmfC0hd9A4kZuaTH1iBh37.BBNOUK7d+KGnVVY0nwGGoOlNRegHFZtr2qYRdDZ45mUJLm4jJ0bXKL+4OJ74SvesjigcWCx0sBn1OnQp8CZjYtjow8MSCLoLzhKS9jd8nVAJCJyrLlhzrgs0gabYwJeUWPVwnhl+Dyr6dllVBijojB38qajey5+Z.Hmqc7LyzUi9fCM2Uq7GeFq7ydxLX72vT3gZ+v77umzpi4PdsQDVam26cCyzNBrx.hd42bYpUdk02Jj83YiKKEhcjpC+zd5O6.kbZuvDzol385SJIIzRFooFvaDBM6CTKcCUpUDPtoNo3IFfu3ebXdwOzCnOYxKuTA4rj8aOpBwdTRB5.7Zm5OUP9MjBMasQm3EPmJe7devo6s8MHfpm4IK328A+ySgJkJ50kIS135u9TvfAUb0W0HPqVk7we7Y3es+yFV562qDGA+1eWFLYiJPSbFXlyHYRMNkzTsmlZZTAW0bSiT0oma36XDEN7xDm8331yNFvoEJdSshSk9nMUwx0M4X4JxYDLBEdvPpwxMcKigqPWGbzl0vbt4jIFi5Yzpgq36OQ9dSQKfVL31FG9q7w0N2zXjpbwGW1WS4MHXNyLdReJoxna+rb3lEz7W5gqK2j3RxHMtxKQfaUpY5W2kvseSwvQOXm3QeLL2bSDks1NUb.GArsbdfKmEecFPsPASMqjXxiRK9Zocp3fNBiun+rCmD+kmFSNY8b0yxHIlZb7Ct2L3xRTAfW9hOxLmztBh6RSh4LUCX4yMyAOgOTYWIyXdijjSNFFgvKpwC1iKAl8TMPbiPOFhQOKXoiiTz.ZzphydDKX1WLjStIRrImDW6DThWWpYt2eFbEIpBKG7D71G1EpTpRxuozAe7G1Fc1ZmXb5oykllQt4YX.rKXTSMYx+1SCE02Flcet22nOmfmSm9XO6oEouOpUINc5k28cGbgjkfO7pSOSO2KkkrjIvzRWEm5PMvl2q6fnwKVvHyeQSl7lU7fyN3Me1ig+4+d722DuVEVvoJ8Lq4mAKYQSfYMsDHQM.NaiO7.cBnkoOuKkYMAAG5.RYNN87tLlWJcOhG.74mhB2zowIvzWTljaJ.VakBegSPicAiY5igkrjIy7ycjL9zzIubNB7.30SnqYpaOpH8LSi6XISl4Oy3vqEKr4sb1H5I5S6.38K5KndKBTkT7bC2PpjtGqb3FbQ3BM6vs+Qx5j89Ic.nkYM+Iv8mWRXduml5sHPWJIw7l+n4RrZgC2naH9DXI+zwBHO8itbPrYdIrn6OClY5Zvpol34Ko63rg32vGauv+appAGnK8jIukLYVTdilLGuAhMhVbeCEs4vknu1FXFMplmtfqDe9fstsuhOZemIhzNPQBInk3MpDm1bPKABKokG72ekjEmk07qNFjhVhWGXtQWD97xUyXGiZ.e8hF8Ink30BVMKc+TGiAz4zMmzrmvxoHgTGidzA3z1.awoQuZF6HUCN8bNHqHaGAqClazQD7CgQMRPOi1Hg7GKH0wnGcAzKkjwD0FXQ20mfZbztGIcYhZ.aN4jlGXqjh9DzxnGgRrYyCsbN5eCFJDBg3m+yqtOIZAKXzTc0sRyMOT9WmUC+jee1jIV3O8qNFCW21DQwPChXVyAi29suXzsPvW+0NXT5c8+H+6.GEmeneCMGEQwECDcGZGECKfhnUwWTLb.pa2o69mpnHJFhQzQDihgEH5jCihgEHZn4nXXAtfFZ9Mdi2f5pqNdxm7IuPwxuEAOT+GVFGUyTX9yNyA1B7NfXqY1y+X23dZ4vMkYD11pdZg87OpD2SaNbSYFlclyECHt..qVsJVzhVj31tsaSzQGcbgfkeKDVEaHaDj8FDsJDBgvtn7BVrH+UuUg0yK1VkHWPjcgUEYZrsOwMBhrJbemOR57Bm2glOwW8UrvaOelxkcYT7VdK7nVCQC2OXfG7MN.qpoKmtQI14X0sYJs4qGqNciO.OV9bd++4+Mi6lxiolx.bLSeJHF.OpUD42KdUhAfDTo7ar2cpSPml9mpHfibjiv2ctykrxJK11e6sPilAHu7XCyVb.pUiQiIg9d3SsYwL1rCFLZjjLpO77nuE.d7nF0pAalaFGpMRJIYLvyrX1Bd.Tqu272h4lwtaMj7nRgdI49QuCG5S9gGrXtEriAFUJFHUk.JUQ75zfQRhG5Mby+KOfd4cdpkupLtqe7pnfJagqeLgFl0iMKXwgGPs9frU.gZTCjbrFHAcZvbyMiaMFH0TRp6v+B0nAPsZ0DR+AO1vbK1vsFiLpTBhmCEXvNTZs0VqXjibjhoMsoIrYy1.tcsTUwhrk1hxRWABEIDB6lDEs7r69YfH+B1Z2OWXUTb9HH2h5Nbk8ZDKFD4tgpCkl7KPTxFVtLeJTzpPHZstREKNaBg+EVsL2sVmnv7CVuVtnpVbOvz6vg9geBq0IJH3mma9R7Oe+1Vn1p0p2PH5MfXCU2pP31jnfbC894Fb3b6UKxGD4VPQhBWbPzkegBS16ll7.wM97UGP8Zs5RD41Cd1m164InMGtDmqWG5H0KRK8zEIkbxhCU2meNz1lDOQVHHmGWr6i1f3SOPEhsriCJNiCWh1bzpXiKPxnezWeuh5NoIw1d1kI.DWwZqPt8xzjyFEM5mmssewsBha8Y2enz.BHGwys8cJ1Qk0INyIeOwUH6TWyV1qn1iVmn7c72EevwsFTaxQ7JUVmn188lhaEDrfWUVN8kdGtq9iesJdk6RRWdhsueQcG8fhM9yxQRmWvFCPyyErs1VChs8R+GB.wRe1+t3C10dE0dltDs01AEO5csJwVp7fh5NdshmaYYI.Da7yZMf+Yg96PsfUI1xtpPrwUlmzmW4NCPys.ha3OI6COYEhYCBtq0K93i2f3Cd8UI.DOz1McN2WYfdcNOhXas0lHyLyTnPgBQYkU14VicaRrBPP1qVTSqtC4Q1MUh.Pj8p2UP2UdTAxWTs0f9bHiHJ+M9dNhH4J1Zc1CvoZJZwB.whKpldoV9kctan6IzW0FxU.4KpxZeq2gC8G+b2PoROOjDHrJJJ2fssdaq1qqXAfnnZrKhDrWSQcOZYP9mrWQvI8zpXC4h.VrnF6hdMhXMEK4qJwu+yu8u3RDQVxmeHhKnsKWtnvBKjG8QeTb5zo+v3b228cS80WO+xe4uja4Vtkys4AnNCt2hVLTyyP1Iqga5deR1eyR6bS2sKU.S2e9yHnFXjq91W74lL7ibuS9dY16Ykk+2+x6087K6JdjYgBEJPgBELqGoB.q8qdGNze7y9YkpmlqLyQFR6zEO8Ib6V58fS2gtuPat12k+vO+dQgBEXH6GLrscBYdYz8r7LvHmL.1nqvkah7YYv8b4FjzeMShmGfM23P14ETDyZ9YKrPdxGes.fPkZ9cO0SyK97OGkUVYL0qXZ7K9MqcPkgUFK8uPcW4h3u9Z+QV2qtVl0l2Ekex2mrjKFK6tbGBe6nSoWdVb4l1c5gt7A3yafLIwqBzA3wiG41EFZ.5viDe5pK6ztyP6f1grrm8JeSdpevXvsr3c6VCoqzMs6Lx580Glklq+3mUgzyiQm5Pr0P06daGcDFezY9nmkIM2UyUbWONaae0RhMrYt46pvtowo7YkSm1CRV1oKa.HnCWtocedvcP9vNjOGbdhsuWtwT.oV4FzjN9b5dn4TTquFt79tu6S.HLXvfnxJqTnWudgRkJEexm7IWPFN1zVWgTBCU0pvsosF1PyE4Ozrc4OKG9xeHhFJuPojZ5Yn4fCeKDh5JQJwk7Cy5o4toRkRTXEkdNq2gC8G+rJG9LjPysVkX4z2gl82ttCMaWr0kK4e7OgC2AnQtU8ZpKBgPzhnvbQP1EJZQllfCMWmbn4BqbnL8jPQe1Qr81aWjbxIGnyHf3AevGbvKM60HVQ9KWTxtpRXxTMhhWctB.Qw0XUHDVEkHmU2pKoRgoFpSr0BVr7KrJ8y.QoKGAjsnvsVtXqAxJFwhKp6NhEElNh9ytFPr7MTpn5ZpVTZQEHJrTSBgvsn7Umsb1gEKp1jIQcUuKQQEVjTlk8odGNzO7SzfX0x5xJJpTQ4kTX2YjmeQxYm1a6v+bDycEEKptNShVsKDUVfjbVcwkKpr7hCjo6JJtJgU2Bgvd0Ar6UTT4h5ZnNwVWc9R9gsZRlw8HqY60H8kBPTPIUJLYxjnpcUhnvh10P1bD62rlW0u92D3kcLwDi3KNQiC9riZqVwilUnK0vi956u6rOayT2YOJesv0sytyP1gKQCGXSRYzABHKwyskMIVJHt0Wp6rlek6pGYVKecliWg3gxIT4uzWuV4meZwVV2x5wRjrLwG21.PuC6UevOGtDm5H6r6rYAwReo2T76uqvjYcH1QShMJmULf3Ip7zhScjcJkQt+UD302j3gj000T4oCrpBrfkEBcK7YeOwoB32OnXgf3VdwC1su5j6UrlEDpMyx1Tu7oWnt52+Vym9zmlwN1whRkJ4EewWjG3AdfAbX+HAGNrgc6dPsgjHbqWsCaVvtG.CFIovspwdrgEKNPswTBa66a3AaVrfGziAiF60hR6wgMrXyC50a.i8f48mdGVo0G7CbfEy1viZijRRCbCQR+UiwjLJunzNvhE6n1XRXTMfCKX1tZRR94db3.050GPdnOIRx3.6uLijr.0FLhwAxJ3OHw.ZSOrxUtRtm64d3JuxqbHSQhhucCEs4vU+1QLJhhgZDcGZGECKPzcncTLr.Q2g1QwvBDMzbTLr.QCMGECKPzPyQwvBDMzbTLr.WjCM6gubO6fs8t0NfOq+91.r7k6gsssOjyiiWvdCOlYOaaa7g02GGw+dZQllVt.J3AGtHuf1cPQWSx7qX8zvm7SIQbvGrtGjhcjGubA+6D2EOEYXE9rMNOx4wZkO3LUyLiCb74+ctu6YGrj2pHtkLFL0rCPG+KVvHuAZcc6kO5Qu1HPy941F4ro0mdO7wO10M3MfK.37p3oFDhizlHf0XHYcZvHNv1wdCdmllibAC02vik5orxOLSXd2NYMPqhs+G.RJ1DARjjzpgDzApTXgxO7aPddeYRPmlAmcKLPx.pkKZpvSSLxzDSjo4hD9FNqYirz+par+Ou+P6D5wFlMaFyVrDxY2rsitCx+d9Q7Op2RX4lEyMSyMaNLg88fGO9YsEZtYyXKbgA8XCyM2LMat2+bWDfA3.yM2b3aeHrxLM2byXIByAwgEyAzUMFC8qfwk0xvta27fYIEinuraO1rH6qBiNCDubGLI6xBCzn+Q1WNDggphgoOKpnb5t.g5YwPYpxWMPQNAHHq0KZvgKQi6a88pJ19866zR78L0Jdhf2xRYsLwGbxtBUlKX8hcr8mIn1mi308WfQNbIZXeAu8xPL6U9lhFBrMoj1JUydkqOvViZMUd5HXiVEk+Rgt8udh20TnO+YC94YIau4I18YjnowC7RB.w5OPqQ1t6nNwZ5wVZa1q7M6UQkM60tQwSbWAQ2BdFwm1V2zDRQS0u9xgtqKHmzCCbzyccbO27msHJLaDjaAhpanIgoZpTTZ40HbKDBg8lDkWzJBrwVqpxpDMX0sH3hkpjpLIZn5sJxGDjewgtSmkcrqt3xE6pjBj1M392vssTozFJcwaPTSSMIppjUKsQRKsAomGzlKk7KPT5t1kntVBeQTYpTYcrncIZpo5jKO1bE92ryMT5pk2vrkHL0TChJk0ktKPLgvVM+mB.wyWcqQ1tsWiX0Kd0hRqplfjChhqyZXz4UKJsxJEEKugXY0kGflPKiz9yWNzguw6HFxm6mpkKbUwV+VAdA0QbCU0hLE96vKsan62pVSd61S9EJZpOsuVkJkAJH.c9qG4MTs0fzkBBgO0TT9QtiXDr6d4at.T8d8uubnCWjWP6dVPP87yWJ+fW5t44+oOCYm7yvruqGmGecOFy7RjxbriN6B.ZqSqztSoeit5vba.xUL2iDrrxQpvf7WvU4rQtiomnbADol3FGfUuzkS2zQmREKz8b4F3dBlEa9Db5W0MIJW.R257lGw1WEOjiufOrB.VKiVwZC4QeT0eE+3LbS4kBrfQfHH9zg7uYKR5a2EIkC4BfJb1M.e8g+m72d82fG+Eei.2qmEM0XmTFAUvSpIlL.pvJszoaZWcOJZp90WFIC+7GWzyZNlPNVM54mg47vkPSW+8xq8x+AV6+mmfadKURks7gL6T.kx+ZCZPql.Y44+d4t5sxe5GN1.ULmKWZXZIngDT2aYzS8HN4eK7Jrzp3lSyeUq4BzLJFsNMnV9X6vpWQemcovf7Ophqfxq9dIotUFzLpQSBJOtTkZFht.IEa7.1HN+YMKaS5ksyvY2l2yefKeNqhrWbATd00QRGaSLqezyzMMx5LgnyNHFG.nPRVZB8nFo+8kCnWxCJLrbMPF0U88429xeet648yYR+nmm8cLKL6T59W1QcZLD3+aH8KgrApvtVtpqZvsVXZ7+i8cRSgq5550ufjCbne7L27gMW5YXzS8pHqdsDf14eKanzi2B1.4Nh13veVM.SneYe21sC18lWEP9r4+5ukr.7nYbxzD5qzP+MZzAM1DP1+aLQiPOSI9BgubvhK5glaqGglC4yNNLq4d+yLiGdoLyKMF93O4SAf3TCs6zMNDRN4RdkR3xW12gwMtLHwQLOV6Jyhe3eHeli1WkmZoeGLz9I3ipvD23C8.LA88TlR5g+oDzpS2bI+fGkkdealUMmjo8WuB9QWS5b1S7I7QlRiexxxE8xg45t1oiDzyrer0Ck9nj80pmsrwGgLSvM0u++en45VJ27kkBW8hxAVyZ469.Z3IxaB7wuxhX8uM.SKhglCmcGW5YATJ+Wux6v7R+Trt7jJr9ZppFZZhWKw4wC5.p3QtZdXU6jk9cGG08p+ZVUEvR2Rdn0ozzVBNzL8qu7BZmgPwP6TP6IjKYzfRVIjOauNwp6wgjzpKoZQPGERhhC5PZp65JtUQoEt7PZGrboIj2KYJcOoCpotyFzcKUE5ghDHX4kHqWRkhZ9g43JIbngcUTHGfQPPkdp6lB8flZwEJJtvEG1jUJtF+GtU81tsapboDnveYeVhXEx9tBpp0fRvZ4gP2h2PPkDprck2+Yv1Ue4KG5vvxM8P+Usb8tJ1jPeWwbCLbgqp0jprNBidFrbLlTRC34Gcwr58tP3KOWPzhmJJFVfgkiHFEe6CQ2g1QwvBDcGZGECKPzPyQwvBDMzbTLr.QCMGECKPzPyQwvBDMzbTLr.++A7s7u+hJ6tKL.....IUjSD4pPfIH" ],
					"embed" : 1,
					"id" : "obj-16",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 776.0, 120.0, 104.0, 40.0 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"background" : 1,
					"data" : [ 11538, "", "IBkSG0fBZn....PCIgDQRA...fO...P7HX....f4dq2v....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wY6clFkbUddf94V66Uu2MsT2f1UKAHPfXQHDfXIfRrfXAgIGOINLDSXBj3b.OIFT7vxjSvKXmiI3LAHGLLGBFarLXOXY.iksElCVCBABg1U2hVsTqV8R0cW6624GstWcqpqp2p8p9dNm9TUeqsuaI8zuueueKWoW5kdIYDHPPUI5J0M.ABDT3PH3BDTEiPvEHnJFgfKPPULFz9KQhDgcricvfCNXop8HPPYIaaaaC.13F2XItkL6HEAeG6XG7U+peUN0oNEgBEpT0lDHnrk6+9u+RcSXVQJonO3fCxoN0o3bNmyAqVsVpZSBDHHOggzOPnPgTkbQjbAUyXx07XEc0I10kDOm3.bf97VpaR4cxXQ1zJ4hH4BpNwEW75VEsaWGgRZ.WtsUpaPEDlTDbEDQxETUidm3FXfO88X2EfZJquwt3Fu7l3ieq2kARj+e+mojUAGDRtfpTzWOW3UrLL.zz4e0bkiM.e3dFlkeoKkP8dBrtntnMcCvNd2OkvlZjy+hWg5KcIsXgiLXX.nwkbQrPqiQe9cwxWXKPHObvOdOLP35X0qZAnCX4WwUw4F3T7QebODsDbpNkBNHjbAUgjHAQBECbaAhEkPQiAFcPKM1HlZrQh40Cu911Nuxq71b7S+Yz8gOHiM53.Pc06lELu1oiEtD9C+y9q4x9iuAZVJACMvnTeaswEc9d3M2oGhDKNXQGwhFhHQhQoJH9zJ3fPxETsgWNzd5gNZaU3omOhO9jQAScPhDI3Yd5mfG6a983T8O..T+Bkn9KQl1bCxxPHOiR2cOFe3quOd8W+04qu3Eve1e7miUe02.ctlagU0RCXgOi8cjAY9qtE9rcuaNY4ZJ5ZQH4BppPuNzAnWmQfnbh9NFW8UbW7g65CwcmRr9GEV5l.K0IibBHYLHoxsIkw+ofdda3fasWdzu0Swhe8swS97KjU0Ri.fQ8ST+ZcFgRV3alEBNHjbAUm7ge3Gx+7S7DjPNLW6+K3h9uMwJnNY7Ij5LgsFgUbmvRu0jbfeLrmmua9Su4KmW6UeNLhaPmdfjPxh24QlXVOWzECglfpI1968+iG6weLL2TD9u9qk4RtWPWZg8jSl1sIN6s5zCK+OFtomRF8NBym6V+y4C9vOjX98CXhlaudzqunc5LIlSK1DgjKnZfCbnCwc+f+Oww4Hye5aljFW5DRbx3Szea3rxb1hjqPcmKbs+yIvb8I4wdh+Y5cuuOCDFZaEWI+AW+pwRg8TIqLqRQWKhz0ETIShPeFa9y8.DIQDtiWQF6sblTxOibKm3rRs1H1IOSTb0a07G.r0Dr1sjje0+iH7sdxGgy4bbhcW1IY3vkjgHCxwkKpHRtfJUdm24c3HG4nrtsHSCKN0H2JxsbhImdNnQ3yP+qcMOXE+Wj4nG4n7Nuy6P3.gIZIrHa475AWH4BpzPVVlezq9ivcmRbgewLG4VQt0FIOdnTilqba5+QfEdSf8Vj3Ue0eDxxk1s7v7xF9fPxETIwd26dYfSM.q5tjQRuFAOCxc7gf8++FdqaG99+sPzDSN8bH09pqSOrfaTlScpAXu6cuE+SPMj21QWDRtfJE14N2I.rnMBIhdlw2NwD2O8H2I8AiJCMt3IdsISN4n1Ypu5seYo9YUpHutkMIjbAUBbvCcPp67jvVCmQVOyDXQN4YtUIRdRPW6vke2vBtTfIlsppR8TkltYWf8Vk3fG7fE8yOsj22S1DRtfxcN4IOA0uX4IlUZwNqTqDMOQLMQwOysIh.jg9nmLsH9vYu047j4jm7DkrySn.soKJjbAky32e.L69rolqUpUk8Xm4XJQ3UFW7joI2ZjZkGCl3VS1A+ABTZNIOCy4wAe5PLN4BJ1HIIgACFPud8XvfgTtud85UuuDRf7DQvSmjZlsZZG2a4j.xmoHZL4hwo7ZRpMccYPBIVvBV.wiGmDIRPhDITue73wS49EhJtWvDbPH4BJLHIIgISlvrYyXxjILZzHlLYBIIIhGONwhEiXwhQznQITnPDK1DlrACFvjIS3voCBMpWU4ENqjp89ISKZMx.IzDYOYFjZMuln9AGNcvPCMDQiFk3wiC.FMZTsMa2tcLZzHFLX.YYYhFMpZaORjHDMZzbR7KnBNHjbA4FFLXXRhrACFHb3vDNbX74ym58iGON5zoCqVshMa1vpUq3xkKrZ0ppDYvfAV5RVJGo6OjjQmr3nMBN.xwfv9fv9ANF3aXvpIPugTk6L027.mRhtV5xXwKdwpQqiEKFgBEhPgBQvfAIXvfDJTHRlLIFLX.KVrfEKVvpUqTe80iEKVHd73SR7U9iES62e4o+cXJQH4BlInSmN0+CtEKVvnQijLYRUYvqWuDJTHhFchI9oYylwlMa31saZqs1vpUqXwhkTRKWQpATSG9hu3Klc8b6h.CAVmX0clZDbMiycriAu5W5rO1u3K.seuvZugLH2ZdcgGE7eZYtnMcQnWud0+vjR6Pap4IRjfvgCqJ9gBEhgFZHhDIB.XxjIrZ0p54aCMz.5zoiXwho9G2BGNLI0lRxYnnH3fPxELYzFwRIBcnPgvue+LxHiPnPgHQhTmmms1ZqzXiMhUqVmT+rMXv.5zoKk92FLXPRjHQJ+m+q4ZtFdtm64nm2F551SsMk9XZqe9ve5uHyytsLceEIu+O3reV986W88WmNcpsWiFMhUqVQud8jLYxII9JB7HiLBm9zmNk1od85wpUqX2tcb3vAM2bypQ30lQSQSvAgjWqi1HQlMaF.BDH.974i.ABP3vgm19apWudZngFvgCGoHEQhDg.ABLo+fPl3xu7KmN6rCNzO4DrjMIOwR2lrGEW6bOe5jakJx22NjnyysCt7K+xS4yNYxjjLYR05Bn87R4OVY1rYra2N5zoC+98yXiM1jNGRjHA986G+98yoO8oQRRBKVrfc61wtc6Te80CTDifqfPxqsPIJiMa1HYxj3ymO73wCABDPMU6YCJQjiEKFiO93yo1jjjD2689emG9geXN3OFV9m+rOVll9ooON2SkbKmD58WCAGQl68AuWjjjlQsIkJrqE2tcShDIHXvfS6qWVVVM89gGdXfI9CpEcAGDRd0LRRRXylM0ehDIBiO93ze+8q1mxbA+98S73wwnQi4z6yl27l4k+AuLexKtOZ8hjw84l4H3Yabtylb66DvQeCIN+KXkr4Mu4bpMZvfAhGOdJo3OaHZznktqtnhICS0C5zoCmNcRas0Fc1YmXylMFarw3PG5PbjibDFbvAyKxMLQApBDH.whECSlLMmeezqWOemu82AyFrvu6wzg+ScFgU6jcISyrsz9csxcnggO5YzgYiV367s+NnOG1JWLYxjpbOSqXdlnjd4CVH4UtHIIgKWtn81amN5nCLZzHCMzPbfCb.5omdviGOSpel4KFarwHZzn4jfCvBW3B4oe5uGg8nie6VzwncOYoNcYWUzOyLcSaj6O36pind0wS+zeOV3BWXN01LYxDQiFcN2MDEJ4WevERdkElMallatY5ryNwnQize+8y9129n2d6kwFarYTQtxU73wCQhDQsPc4Bqe8qmm8YeVR52L+5GVhi7+EhGVyLYSSesSFaxQsiGdhcW0e+SJAgMyy9rOKqe8qOmaWlMalHQhfGOdxo2mRRevSGQexKuQmNc3vgCb5zIRRRLxHivwO9wyoTGyEBGNLd85EGNbfEKVHb3v4z625W+540e8eJOvC9.r2W5Som2Rhy6Fko8KCr3N0hsoTnsviNwPg02NjH3Hxb9WvJ467s+N4bja.rXwBwhECud8lymakEBNHj7xQLa1Ltb4B61siWud4Dm3Dy4B9juYngFh5qudb3vQNKAvDoqu0e7VYqacq7u+L+6ruW93ruWFbzlDNNGYLZehmWTef+SIQfAmX3757b6f68AuW17l2bN0masX0pU762OCMzP476UYifCBIub.k9V6vgC.J4QqyFJC0lc61USmMWQud87m7m7mvcbG2A6bm6jsu8syG8QeDG6X8vfd8A.tb4jktfExE+GcwrgMrAt7K+xmwCE1LAylMirrLABDHmSOGJyDbPH4kJzqWOtc6FmNchOe9nu95i.k3k53TgrrL82e+X2tcb5zYdqJ8vD+Qtq3JtBthq3JTO1hVzh.ft6t671mSlvtc63ymO5u+9yKqtrRdQ1xDhBuU7vjISzbyMy7m+7IVrXb3CeX5s2dKqkaEFd3gwqWuDOdbrYqx+56sMa1Hd7330qW0IqRtRYWDbEDQxKrX0pUb61MFMZjgGdX5s2dKJU.OeSu81q5TyLRjHUjmCvYma4iN5nzau8l2deKaEbPH4EBb3vAtc6FYYYFbvAYrwFqju09lK32ueFXfAvnQi3xkKFczQK0Mo4Dtb4R8bIeVHyxZAGDRd9B2tcia2tIb3vbxSdR74yWotIk23Dm3D3zoSUI2qWuk5lzrBWtbQrXwXrwFiSbh76d3VYufCBIOWvgCGTe80S3vgomd5op76tjISR2c2MlLYhFZnAra2dEQMDfIJpljjDiO93bzidzLtltyEJKKxVlPT3sYGVrXg4Mu4gCGNn2d6spUtUHb3vbzidTFe7wwjISUDEcylMaXxjIU4NeNR.JTwH3fPxmIXxjIZqs1nwFajAFX.NxQNRESzrbEe97Q2c2MiM1Xpqo5xUTF+9wFaL5t6tKXcYphRvAgjmMzqWOM2byzVaswniNJG5PGJiaT.U6L5nixQO5QYzQGECFLfKWtxqSDk7Atb4BCFLjRasPQEmfCBIWK5zoiFZnAl+7mOACFjCdvCxvCObEckwyUT9Cbd73gjISRc0UWdaZjlKnWudpu95IYxj3wiGNzgNTAup+UjBNHjbXhz75niNHYxjbnCcHFXfApXGG37M974i8u+8yPCMDACFj5pqtRZ+xsYyF0UWcDLXPFZngX+6e+EkQxnhnJ5YiZ0pqqjNtd85omd5YFsk9TKRjHQ3.G3.L+4OeZqs1vgCGzPCMPf.AJHEzJSnTKf3wiyniNJCLv.bhSbh7d0xyFUzBNT6I4tb4h5pqNFd3gYvAGrlNU7YBISljie7iiGOdnyN6D2tcqtGwEJTn7xJQKSnr2lKKKiOe9X7wGmie7iWzWMdU7BNTaH4FMZjlatYRjHQAaHUplwue+r+8ueZpoln81aG61sqtgPFIRDhDIRNuCzXxjI0q3JwhEC+98Sf.An+96OuM2xmsTUH3P0sjWWc0gKWt3zm9zkr+iR0BCO7vLxHiP80WOszRK3xkK0Kn.JaxgZ2exU1li0hNc5RY+MW4Gkq.IiN5n30qWFZngviGOkzrrpZDbn5SxU1djBGNLG9vGtfsGmUqgrrLd73AOd7fEKVngFZ.2tciCGNTufDXxjIzqWupLqPSM0jpzqrUGGLXP0MHwwGeb73wSAK0+YKUUBNT8H4JS4x96u+ZxwytXQ3vgo+96m96ueLXv.Nb3.a1rgEKVvrYypWSyb61M.L3fCpdUGIRjHDNbXBFLXNu6mVnnpSvgJaI2nQizRKsPjHQ3vG9vkk+mlpUhGONiM1XY7OntjkrD.X26d2E6lUNQUofCyLIezQGkeyu42TvpFsjjDczQGbsW60NiFCVmNcRCMz.m5TmhQFYj7d6QPsGUsBNL0R9d26d4wdrGC.0qiSxxx48eRjHAu7K+x70+5ecl+7meFam5zoiVZoEjjj3HG4HhJjKHuQUsfCYVxiDIBOwS7Dzd6sy0bMWiZEPiEKVJ2lsiMadNxxx30qWdhm3I36889dSp8Y0pUZt4lwiGOLv.CHFWaA4UpXmppyFReZs9we7GiWud4ptpqJixc5nLbIMzPCzZqshc61mwSITIIIRlLIe1m8YzWe8kxi0PCMPSM0D81aubpScJgbKHuSUeDbEzJ4ACFT8BxtV4VAsQfiFMJqZUqhq8Zu1TVvB6ae6isssswoN0olwsgwGeb0KyOs1ZqDJTHQgzDTPolQvgyJ40UWcHIIoFsVQvzlhMLwzb71u8amN5nCFXfA38e+2mHQhvxW9x4htnKhksrkw+5+5+JG4HGIqovmN1samlZpIQgzDTTnlRvgIjbkgAQqXq82gItNTu10tV5niNXaaaa7JuxqfWudUmJhqYMqg669tOtm64d3we7GeFsI0qrXG5t6tqnF5NAUtTSzG7zQo3WwhEKE4VazWSlLwkcYWFG7fGjW5kdIBDH.s1Zq3zoSRlLIe5m9o7i+w+Xra2NqZUqZF84ZxjINxQNhPtETznlTvUPqTmdZ5M2by.v69tuKABDfN5nCV9xWNM2bypSSwCe3CCLw00pLUU8zo+96WLcSETTolKEcsnUBSOMcCFl3qFkk2mx7WNPf.jLYRzqWuZkzqqt5lQU.uXsFfEHPgZVAWIEcERu+3Jhcqs1JlMalQFYD74ym5UOi5pqNZrwFAfSdxSptvCDHnbBQJ5ZRqVaZ1m3DmfXwhwsbK2Bs0VaXvfAhEKF50qmy67NOrYyFW5kdo.vgNzgT2X+xV54BDTJnlVvUj5zGdr3wiS3vg4m8y9YTe80ye2e2eGKcoKE2tcSyM2LMzPCrgMrAtxq7J4vG9vr6cu6rNIYDHnTRMaJ5.SJRqVAWRRhcu6cia2t4FtgafG9geXFYjQHXvfzd6sid854y9rOim8Ye1Tl63hn2BJmPH3Y320Vvs25sdKN7gOLW2ezcxFtl0x7ZqUZrI2D5SeQt+6+IY7wGeJqbt.AkRpYEbYY4IIjoWI8Itc0rk+OuBqq4Te8QjiHjaAk8H5CdF5GtxOQiFESlzgMa.Q7vnZ1ch0YzEKdwKtr6plg.AZolMBNL0ona2tcV+5WOabiajS9a2FmLQBzeNqlMdoyC.L1vJ3QdjGgidzixq9puJ6ZW6RLLYBJ6PH3ZHVrXjLYRVvBV.2y8bO3zoyIdfyHtF0M4DdV7hWLOzC8Pr8sucdwW7EY7wGuf2tEHXlRMaJ5ZmK5Z+YEqXE7.OvCnJ2m9zmlW3EdA1xV1BOy6c1kFZzg+H1912t5LXaCaXC7O7O7Ont6vHPP4.hH3ZX9ye97W8W8Wo1u5W60dMdi23MTWoXKTyrQUNQXdlm4Y38du2i69tuaZu81YEqXEb228cyS8TOEQiFsncdHPP1nlMBNjZQ1.3K9E+hpapCe+u+2mW5kdoobYfZxjI5s2d4odpmRc2ZYcqacbUW0UU3a7BDLCnlVv0Vw7Uu5UqtoH9Nuy6v1111l1EGhx9lcnPg3+7+7+TMifuvW3KfKWtJ3seABlNDBd73jLYRt9q+5AlHp9O+m+ySI8cYYYZqs1voQsecIodEvvrYy3ymO9k+xeIvDW8KloqQbABJjTyJ3JSzk3wiSqs1JKXAK..du2683XG6Xo77VxRVBW0UcU3zTpBtjjDlMaVUx095V5RWZw5TQffrRMcQ1T56cSM0j5wNxQNRJSdEGNtVd926sXstg3FN6WWNufuDG6XeIL3+C3O+F+qoaSlvqWu3ymOb5zIc0UWHKKKlHLBJoTyFAGNaJ51saW8XoON1NbTO50a.LXXR+0PCF.LXDyNbfCGNvjISLv.C..szRKXznwB7Yf.ASM0zQvUP6twhtzlLKwi+t7.+gWE974ScSfvmOe30qWjkk4RtjKAWtbodsgV40K18VDTNfPvA750q58qqt5R4w762Om3Dm.850iOe9R446vgCU41rYyDIRDNmy4b.lXWdIVrXhTzETRolNEcEzdwKXEqXEpQzMYxDFMZT8B5txOvD6YaKZQKRUtMYxDtc6V8hLn1c4EABJUHDblHZ69129.fK6xtL5pqtR4wc5zIxxxX1rYrXwB0We8zUWcQas0lpbKIIw4dtmq5q4PG5PE0yAABxDBAmI5u7a8VuEvD8A+y+4+7XylM0nyRRRzRKsvBVvBXoKcozUWcotgKpz2aIIIt7K+xAfd6sW9jO4SJYmOBDnfPvOC+9e+umCbfC..qYMqg67NuSLYxD.obq16a1rY.HRjHby27MqlR9K+xur3haffxBDB9YHZzn7bO2yQvfSrqNroMsItq65t3bNmyAYYYUwFNqvqb7MsoMga2tAfe1O6mwG7AePw+DPffLfnJ5Zn6t6lu427axC8PODlMal0st0wZVyZX6ae6bxSdRBDH.50qGa1rQKszBKe4KmEu3Eq95+c+teGuxq7JYcHxrXwRw5TQf..gfmBRRR7QezGwV1xV3u8u8ukN6rSLa1L2xsbKS4qSVVle3O7Gxq8ZuVJ6vpoSqs1J1rYSMKAABJzTSJ3J8cNSHIIwQO5Q4ge3Gl0st0wse62dJSkUsHKKy69tuKuwa7FyngEavAGjUu5UywO9wERtfhB0bBtQiFYwKdwjHQBjjjxpT52uedy27MYG6XGrjkrDl+7mOtc6FCFLPvfAYvAGjd5oG5qu9lx2GsXylMN9wONc1YmBIWPQgZNAu0VakjISRyM2Ld73gjISNkxYvfAYO6YOrm8rmL93SmXKKKiNc5nqt5hVZoEBFLnPxETznlpJ5MzPCDJTHFarw3q9U+ppWUP0oSWA6GYYY5niN3u4u4uQscnUxUl4aBDTHnlIBtUqVwtc6pWSum27lGe2u62k8rm8vPCMTA4yTRRh4Mu4wJW4JmTjdQjbAECpIDbc5zQyM2L81auorSsXvfAtjK4RJYsKgjKnPSMQJ5szRK3wiG0k6Y4Dhz0ETHopWvc5zIRRRpaDCkiHjbAEJppEbiFMRCMz.81auoroNTNhPxETHnpVvaokV3Tm5TS4rKqbBgjKHeSUqf2PCMPjHQXjQFoT2TlUHjbA4SpJEbylMic61UuZiTogPxEjunpTvat4lo+96eRW6wpjPH4BxGT0I30UWcDNbXFarwJ0MkbFgjKHWopRvMZzHtb4hSbhSTpaJ4MDRtfbgpJAu4lalSe5SqdEKoZAgjKXtRUif6xkKRjHACO7vk5lRAAgjKXtPUgfqWudpqt5pXqZ9LEgjKX1RUgf2byMyvCObEyDZIWPH4BlMTwK31saGc5zwfCNXotoTzPH4BloTQK35zoilZpI5qu9J6mq44aDRtfYBUzBd80WOiO930rqiZgjKX5nhUv0qWONb3nrdYfVLPH4BlJpXE7FZnAFd3gq5Fy64BBIWP1nhTvMYxDVsZsfsWpUIhPxEjIpHE7FZnAN8oOMIRjnT2TJqPH4BRmJNA2hEKnWu9Jt04cwBgjKPKUbBdiM1HCLv.0bCK1rAgjKPgJJA2gCGjHQhphkBZgFgjK.pvD75qud5u+9K0MiJFDRtfJFA2sa2DNbXBDHPotoTQgPxqsohRvO0oNUotYTQhPxqcohPvc3vAgCGlPgBUpaJUrHj7ZSpHDb2tcWSsZwJTHj7ZOJ6EbqVshrrL974qT2TpJPH40VT1K3hn24eDRdsCk0BtISlvnQihw8t.fPxqMnrVvc61MCO7vhYsVABgjW8SYqfqWudrYylXNmWfQH4U2T1J3tc6FOd7HVwXEADRd0KkkBtjjDNc5rpcONubDgjWcRYof6xkK74yGQiFsT2TpoPH4UeTVJ3Nb3PD8tDgPxqtnrSvMa1L.hEURIDgjW8PYmf6xkKQkyKCPH4UGTVI35zoCa1rwniNZotoH.gjWMPYkf6vgC74yGwiGuT2TDbFDRdkMkUBtSmNEomWFhPxqborQvMa1LRRR32u+RcSQPFPH4UlT1H3hhqU9iPxq7nrPvkjjvlMa3wimRcSQvzfPxqrnrPvc5zI986WTbsJDDRdkCkEBtCGNDomWggPxqLnjK35zoCiFMJJtVEHBIu7mRtfa2tc74ymXScnBEgjWdSYgfO93iWpaFBxADRd4KkTAWRRBKVrH1wTqBPH4kmTREba1rQf.AD6ZKUIHj7xOJ4BtXGSs5BgjWdQIWvEomW8gPxKenjI3VsZkHQhPrXwJUMAAEPDRd4AkLAWT87peDRdomRlfaylMgfWCfPxKsTRDbSlLQxjIIRjHkhOdAEYDRdoiRhfa0pUQw0pwPH4kFJIBtYylE6Zp0fHj7hK1rYSH3BJtHj7hC1rYiN6ryhufavfA.DW0RpgQH4EVTj6ie7iW7EbKVrHhdKPH4EHzJ2ACFTH3BJcHj77KoK2PInHalLYRH3BTQH44GxjbCEYAWY2aIb3vEyOVAk4Hj7birI2PQVvsXwBACFTr6sHXRHj74FSkbCk.AWjdtfrgPxmcLcxMHDbAkYHj7YFyD4FJxBtQiFITnPEyORAUfHj7olYpbCEQA2fACjHQBw1yjfYDBIOyLajanHJ3lMaVD8VvrBgjmJyV4FJhBtISlDBtfYMBIeBlKxMTDEbw3eKXtRstjOWkanHGAWH3BlqTqJ44hbCEIAWRRBCFLHVAYBxIp0j7bUtghjfqD8VLC1DjqTqH44C4FJRBtYylEomKHuQ0tjmuja.LjmZSSIlLYRrGrIHuhVIOeHBYCYYY9jO4SXm6bmHKKiEKVXUqZUHIIUP97xmxMTjDbQEzETHnPK4QiFk+o+o+IN4IOo5Tr1tc6zd6sy+3+3+HlMaNu94kukaPjhtfJbJjoq+zO8SS2c2cJqeh.ABPO8zCe2u62Mu9YUHjanHH3JoxDOd7B8GkfZTJDRtWud4PG5PYbpUmHQB5t6tyaW3LKTxMTDRQ2fAChq+XBJ3jORWWVVlidzixANvAXe6aenSmNZokVvlMaXxjIjjjHRjHDLXPhFMJO4S9jzUWcQWc0EqbkqbNkxdgTtghffqWudgfKnnvbUxO4IOIae6amCbfCPWc0EWzEcQbK2xsPiM1HwiGmDIRPznQUuuxOd85kd5oG93O9i4Ue0WkFarQtwa7F4BuvKbFUDtBsbCEoH3hzyETrX1H4c2c27Zu1qgISlXyady72+2+2SxjIUE3HQhLkq.Ra1rwRW5RYQKZQbi23MR+82O6XG6fevO3GvF23FYcqacnSWl6EbwPtghjfKlAaBJlLcRtWud4kdoWBYYYdvG7A4bO2ykDIRLoHzYCsOmB9R3s...C8lDQAQUszZqsxF23FYrwFi2+8eedy27M4tu66lEsnEkxyqXI2PQJEcwpHSPwlrI46ZW6he5O8mxW9K+k4JuxqLihZ5jt7OUOuDIRfEKVX8qe8LxHivy+7OOqbkqj67NuSzqWeQUtghPUzEEYSPoBsRtEKV3G7C9Ar6cuad9m+4SQtyTz6YhPCYOZN.0UWcba21sw3iONO9i+3DKVrhpbCEAAWud8h9fKnjQvfAomd5g+s+s+MZu814a8s9VXylsLJ2SEyV4W6y+BuvKjku7kyi7HOB6ZW6pnI2PQRvEQvETpHQhD7M9FeCtzK8R4u7u7uj3wiOI4V6yMaQymIeNIRjfXwhkQwedyadr5UuZdjG4QX3gGNudNNUTTRQWDAWPohm64dNt5q9pYyadyYUbyVz4YyumMz9bc61Mqd0qluw23afe+9KJm+ETAWmNcHKKSxjIKjeLBDjQ94+7eNs1ZqbG2wcLqj64JJQumpH+M1XirrksLdxm7IKJdQAUvEEXSPohd5oG9jO4S39u+6WchpnHcoK6ylT0mpee5P440RKsfUqVYqacqExuB.JvBtjjjH5sfhNwiGmW3EdA1xV1B.SoXmIQMW6G9L40u3EuX98+9eO80WeEjuCTPH3Bp53W7K9ErgMrAZt4lmRwN8zxmtT0mJAdllopxqMYxjrrksLdlm4Yxqm6oSAWvEaSSBJlDHP.14N2I21scapxmR54oKnSUjaXlmpdl568LYX0b4xE5zoiO7C+vB12GhH3Bpp3se62lMtwMhjjTViXOcopCYWtyGn88q81amezO5GkWdeyDBAWPUA974iW3EdA9k+xeIW+0e8pBTjHQTk5oRrmt9gqPtD8NS8E2pUqDHP.9ZesuF82e+48uWJnyEcQJ5BJF79u+6yK9huHd85kq65tNjjjxX+n096J2OSQs0dbsO1TI2yFRW96ryNYO6YO7XO1iwsdq2JabiabN+cQ5HDbAUzr0stUdy27MwmOe3vgCt4a9lypzkdjXkioUhUdroStyz68bcRwTWc0gNc5X7wGmexO4mvPCMDewu3WL2+xgrJ31Ygm+BH1INL8M1zsTOsyBO+kQqtMS7wOA69S6CkSAQJ5BJj7Vu0aw1111T2yzzqWOc1YmYc4IqUpUH8n1ZOV1DdHySpkb42c3vAiN5n32ue1wN1A0We8roMsob96nL2GbKMyR6rSZuNiS6aPaWzUxx6rIRFJNlb5.suBQDbAEJ5qu9XqacqorgH1ZqslR+s09i1MugYZevgLGMOaxctPiM1n5t.Sf.A3Mdi2fd6s2b58DxlfmLAwm3loEmNMQxg9D14t+.du2+.Xuqqla9ZOeziPvET33YdlmAud8lxwV1xVVVk3L8iRA3lMR+TMcTmKQuUtuc61Qud8puWd85k+i+i+ib96or1G7TMeSbdW3EyhayMDZTNvt2MmLPBZbIqgk3Dv7x4ptx14X83ikdtNQmNqr1qxIi2+Q4nmVratHH+Ru81Km9zmNkiYvfAVvBVvz12aXxopms9fqbLk6mtbq88elV07rgEKVlzwFXfAnu95iN5niYz6QlHEAeaaaao7fMeAWCa7BR6U3rYV007GvpzdLS1vsIabQWRapusNcWONcuFleWy41l.AYj64dtmIckxwrYyTWc0kUYVgLUI8r0Gbk6qLK0lpz2mpT0mtn2IRj.YY4IsQM5ymO5omd3q7U9JytufzPQ4Jah.A4S90+5ecFOtYylyXgzzRlD7LEQO8n1oe7oJR8rI07oKR+u5W8qlYeojERQv23F2H2+8e+foN3ltgKfQ26ukOXnFXCa3BH8DHFZu+V9f9RvpuoMPSitWd6OXhIM+7V8MwpZYX19atarVe8DIRjIkNk.A4BYZCSPmNcjLYxobC9b1lpdlj6rsLSyzwxT57yVFbvA4G9C+gy4W+LJBtA.eGam7tGXDzqGNa6cx8aPKxxxYcaiUff4JYZnWkkkm1pYmdT1zuuVwV4209XyV4N8O6oKx9L8bc1vzaegOM8GDbtfKfkzhc.KzXasgK8Y6EjDzYk5rnOi8qPffbkL8+oRlLIACFbNMLYZqPd5ERKaR4LQtmsolmoQbJW8mrDAehgIaBhxm9t6DSW8ZXIW50vRNyw161G.uIfz+CLCzaerx1VDqdC+AL1Q9P9zSK1xjEjewfgI+eaSjHAiM1XzXiMlwGKaGS6x7La80N8iMUhr12qLI2YBsEZKczNzYyExrfGse191zLw2SLB6927lXxhEzQRhEN5YlsZg4iemTq7dhQNDu8acLrXDLazL5xwFn.Aoy7l27XjQFIkiEIRDFXfA37NuyKkimI4NSRs16mdzYkaSu.aZO9z0ObseFY5wiDIRFOWat4lm9uPlBlUcPNZ3vDVUtmBRDchmmHEcAE.t9q+5y33FericrTDnvgCql1s1exTJ3JqY74xjeY5VW4P1GRMkmS3vgmT+sMZzHWy0bM4z2UEzgISzGbAEBtzK8Rwtc6S5ZNee802zdcnO8H5YJU7Lc6zspyzd7zO1zMjXIRLwExvzSQ2tc6r10t1o6qiojBZItEUQWPg.850yse629jtVfGMZT74y2zVLMsoEOS1RmlIKozYpbqP5uFsyodXhwz+5ttqKiYpLaPH3BpH45ttqiEsnEgQimc4MEIRD1+92+LRnmJodpRGOSOFj4BukI4NSQyiDIxjROugFZfMu4MmyeOUvEbQJ5BJT7U9JeEl27lmpjmLYR18t28LZXxztOso89yjH5yjiOSkaXhEVhVAu95qmG5gdnLNZAyVDQvETwhYyl4we7Gmt5pKra2N.DJTHFXfAxpLOUQjmoopOUGOSOOH6xc73wUmW85zoilZpI9ZesuFszRK4kuiDBtfJZLYxDO7C+vbW20cQSM0DxxxricrCUwNSxc1RUe1r6qlsn1vzunTTHQhDL93iShDSbYM5BtfKfm3IdBZu81yae+TPqhd73wSoORBDTnXcqacr10tV16d2KO6y9rbxSdRZpolx3yMcIKS2epppdll7KyzwFW6yMd7330qWN+y+74K7E9BrfErfb+KhznfJ3ISlDIII0EBf.AERzoSGqZUqhsrksv+x+x+BaXCaXR0.RqDm9umoUhV1D6Lcro53Y5XABDf63NtCt0a8VyOeAjAJ3KWz3wiiACFlxU4i.A4SZu814xtrKi8su8QWcc1MjfzkaHyK.kLI3oGgNaGSap6J+dlj6PgBgCGN3y849b4mS5rPAuCxIRjPjltfhNadyalwGeb0Btkd+tSux5SUe0SuB7YarwyVU1SWtiEKFwiGmG7AevBdMpJJBd9nb+BDLaPmNc7.OvCv92+9wiGOSYUzmqULepdLH6yhsvgCyccW2UdqR4S42CE5O.Qg1DTpXdyad7s+1ea1yd1C986OEQK8H1S0PkkqiMNbVYOTnP7W7W7Wjyyw7YJhTzETUhMa1niN5.a1rw8ce2G6YO6QcZrld55YKU870XiqUtuoa5l3Zu1qUssUnonDA2jISE5OFABTQQt6qu9HXvfr3EuX0z0GZnglQomOWD6rEMOZznDLXP97e9OO21scaDLXP0cK0BsjWTDbQevETrHc4VgN6rSdzG8QYzQGkicriotrQymS5kL8XACFjXwhwW9K+k4FtgaPs8Trj7hRJ5hH3BJFjM4VA2tcyi9nOJqYMqg8u+8yvCO7T1u7boO3QhDgPgBwhVzh3q+0+5r7ku7I0dJFRdQabvEHnPxzI2JHIIwl1zlXsqcs7hu3KR2c2M0We831sa0BhAS+jdI8mixwTVYXlLYhuzW5KwJW4Jmx1sVIe5Z6yEJ3lmrrLxxxXvfgIsP2EHHevLUt0RSM0DO3C9fze+8yq8ZuFG7fGD61siUqVwpUqSRl0d+LMK0BEJDRRRTe80yse62NqZUqZxenYgBojWTBsFMZTrXwB986uX7wInFh4hbqk1auctu669Hb3vrqcsKdu268nu95CCFLfQiFId73nWud0caEYYYhEKF50qmjISR73wwsa2r90udV6ZWaVm+6SGEJIunH3whESH3Bx6jqxsVrXwBqacqi0st0A.iLxHb7iebN8oOMiLxHzd6sShDIn+96G2tcSqs1Jye9ym1au871rQqPH4EsH3VsZsX7QInFg7obmIZrwFSYKX9Nuy6Dfb5pLxLg7sjWTVr1QhDIm2aoDHPgBsbWpIeVc8hhfqzGbw12jfbkpc4Vg7kjWTDbkqaThwCWPtPshbqP9PxKZ6mRJQwEHXtPslbqPtJ4EMAWoR5BDLaoVUtUHWj7hZD7hwpmQP0E05xsByUIunI3QhDQLTYBlUHj6TYtH4EMAOd73nSmtb9xgpfZCDxclY1J4++AzHfdbD7H2xR.....jTQNQjqBAlf" ],
					"embed" : 1,
					"id" : "obj-14",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 728.0, 88.0, 104.0, 104.0 ]
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-31", 2 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-31", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 3 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.568627, 0.619608, 0.662745, 1.0 ],
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-57", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-57", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"color" : [ 0.666667, 0.698039, 0.717647, 1.0 ],
					"destination" : [ "obj-31", 1 ],
					"disabled" : 0,
					"hidden" : 1,
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-66" : [ "OpenOper", "OpenOper", 0 ],
			"obj-167" : [ "AudioRecord", "Record", 0 ],
			"obj-23::obj-50" : [ "Play", "Play", 0 ],
			"obj-165" : [ "AudioSetup", "Setup", 0 ],
			"obj-23::obj-65" : [ "Transp", "Transp", 0 ],
			"obj-29" : [ "OutputGain", "Master", 0 ],
			"obj-23::obj-82" : [ "Gain", "Gain", 0 ],
			"obj-23::obj-39" : [ "Loop", "Loop", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "FMA.SoundPlayer~.maxpat",
				"bootpath" : "/Users/Manuel/Music/ManuelMax/MaxMSPLib/IRCAM/ForumMaxApps-All/Utilities",
				"patcherrelativepath" : "../Utilities",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "spat.times~.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat.oper.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "spat.spat~.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
