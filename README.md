## Principal Functions ##

From a navigation patch, users can browse various applications grouped in simple to advanced examples showcasing underlying technologies with tutorials on their functionalities, or their complete musical applications within a programming and musical workflow.

The navigator gives access to several application themes, each treating specific technologies: live sound transformations, synthesis, analysis or spatialization. User is invited to study example patches and test their effects on their own, and reuse the patches of their interest in their own work involving Forum technologies in [Max](https://forum.ircam.fr/projects/detail/max-8/).

Special effort is given to provide reusable patches in Max as well as devices for [Max For Live](https://www.ableton.com/en/live/max-for-live/).

## How to install Forum Max Apps ##

1-Make sure that the [MaxSoundBox](https://forum.ircam.fr/projects/detail/max-sound-box/), [OMax](https://forum.ircam.fr/projects/detail/omax/), [Spat](https://forum.ircam.fr/projects/detail/spat/) and [SuperVP Max](https://forum.ircam.fr/projects/detail/supervp-for-max/) objects are properly copied and authorized on your machine. Also make sure that the [Modalys](https://forum.ircam.fr/projects/detail/modalys/) software is properly installed and authorized, as [Modalys](https://forum.ircam.fr/projects/detail/modalys/) comes with its own installer. 

All software can be found [there](https://forum.ircam.fr/collections/detail/technologies-ircam/). 

2-Where to copy the Forum Max Apps patches and IRCAM Max objects:

Each collection of IRCAM Max objects comes now as Max "packages": a Max package is a folder that is organized in such a way that Max can scan its content and add it to its search path. Locate the "packages" folder inside your Max folder. You may also access it from the ‘Max/Packages’ in your Documents folder. To install a package, simply copy it to your "packages" folder. To uninstall a package, simply remove it from your "packages" folder.

> - **Note:** 
> The Modalys Max package is located in the "HardDrive/Applications/Modalys/Components/Max/" folder. You'll need to copy the content of that folder into the "packages" folder as well.

3-Launch Max 6.

4-From the Extras menu, select the Forum Max Apps overview item (or Mlys.Overview, or Spat.Overview) according to the package that you've downloaded , which should launch the corresponding navigation patch:

![Image of Forum Max Apps](https://git.forum.ircam.fr/haddad/forum-max-apps/raw/master/misc/Media/Screen%20Shot%202013-12-19%20at%2016.13.57.png)

> - Forum Max Apps are developed as a collaboration between IRCAM and  [Manuel Poletti](http://www.musicunit.fr/music-unit-en/manuel-poletti) at studio [Music Unit]( http://www.musicunit.fr/musicunit-en).
> - Video: [Ircam Forum Max App: Modalys](https://www.youtube.com/watch?v=8m-KlgF48mU)